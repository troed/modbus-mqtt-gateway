### Version 0.1.0

- Introduced Web Admin interface

### Version 0.0.14

- Refactor 'get_modbus_handler' to improve maintainability
- Add handling of custom ModBus errors 
- Add CDR handling 


### Version 0.0.13

- Add handling of user auth and cert based auth in MQTT client
- Add full stack trace in exception feedback on command error
- Float conversion in write_holding register


### Version 0.0.12

- Fix scaling validation ( > 0 instead of >1.0)

### Version 0.0.11

- Fix values conversion in state table (for refresh outputs)
- Add missing documentation from 'gtwwrite' command


### Version 0.0.10

- Fix registers range handling (ie 40001-40003 is managed as 3 addresses starting from 40001)
- Refactor to manage multiple registers reading as a single message
- Refactor to manage send strategies (on value change, absolute/perc) handling list of values
- Add reverse scaling for values received on MQTT

### Version 0.0.9

- Fix handling in state table of ALL register (it should handle only the row marked with 'W(rite)')

### Version 0.0.8

- Add output registers read state on start
- Add log (gateway) for initialization of output register table


### Version 0.0.7

- Add handling of ModBus read by register value range (ie. 40001 for holing, 30001 input, ....)
- Add strategy to manage variation for COILS and DIGITAL INPUT (based on value change from previous read)
- Add synamic state table to store last values written in registers marked as W(rite)
- Change: discrete input and coils returns 0/1 (based on bit[0] of the returned byte), 
          holding and input register return the value converted following the table rules

NOTE: Registers are read following this table:
    ModBusRespType.HOLDING_REG: mbus.read_holding_registers,
    ModBusRespType.INPUT_REG: mbus.read_input_registers,
    ModBusRespType.COILS: mbus.read_coils,
    ModBusRespType.DISCRETE_INPUT: mbus.read_discrete_inputs

### Version 0.0.6

- Feedback message on MQTT for unknown commands
- Add 'cnxmdbstatus' command
- Add startup log in gateway logger
- Add register write logging in MQTT
- Add ModBus logging in gateway logger 

### Version 0.0.5

- Add MQTT connection error handling
- Add QOS handling MQTT client

### Version 0.0.4

- Add MQTT clientid matching the gatewayId

### Version 0.0.3

- Add MQTT message on ModBus communication problem


### Version 0.1.2

- Add web administration

### Version 0.1.3

- Fix to web admin

### Version 0.1.4

- Error handling for ModBus and MQTT producers and change Dashboard status management
- Fix serial config save bug (wrong bitlen handling)
- Add gateway name and id in dashboard
- Add parameter --web-port to manage web admin listening port
- Add parameter --debug to activate debug mode
- Move register table link in config menu
- Handled Dashboard run even when suubsystems aren't started

### Version 0.1.5

- Remove link to company logo
- Remove internal MQTT event from web dashboard
- Add MQTT messages published by the device in dashboard
- Add auto refresh in Dashboard
- Add timestamp e topic in the ModBus table


### Version 0.1.6

- Add `--mbus-poll-sec` to set ModBus polling interval
- Add `--mbus-refresh-sec` to set ModBus regs refresh frequency
- Version on bottom left corner
- Change title color
- Add handler to manage MQTT connection lost
- Add feedback on reboot commands
- Update logo
- Add image for gateway title
- Add topic logging in exception when registryWrite is called

### Version 0.1.7

- Update layout with changes provided by the client
- Change MQTT connection strategy to handle reconnect
- ModBus polling frequency read from config (serial.ini)
- Handling of certificare and key file upload
- Update dashboard tables width to handle large text logs

### Version 0.1.8

- WebAdmin still running if MQTT cert file and/or key are wrong (user can change settings)
- Add handling of CA cert file
- Fix WebAdmin error if MQTT in wrong connection status
- Retry handle after disconnect callback


### Version 0.1.9

- Certificate path are now hidden
- If a certificate is loaded, last upload datetime is shown otherwise is shown a message saying file is missing
- Remove 'cert_file', 'key_file', 'ca_file' from "mqtt" command
- Add upload certs trough MQTT implementation
- Add file validation on WebAdmin upload. NOTE: Files are validated one by one during upload, there is no way to check consistency across them (can be loaded a correct "ca.crt" that doesn't match with currently uploaded "server.crt" and "server.key")
- Add system date time in the upper left header

### Version 0.1.10

- Add Gevent to handle HTTP request because Flask server is bugged

### Version 0.1.11

- Update online help

### Version 0.1.12

- Change SimpleThread to support delayed start 
- Add parameter to manage delay for refresh state thread

### Version 0.1.13

- Rmoved --mbus-refresh-sec cli params
- Use mbus_poll_sec as register refresh delay
- Rename 'comandi' in 'commands'

### Version 0.1.14

- Fix bug in CSV validation when writted trought webadmin
- Fix state initialization for MODBUS Faulted, it should start faulted and reset state on first message