"""
.. module:: broker
   :platform: Unix
   :synopsis: MQTT broker interface modukle

.. moduleauthor:: Fabrizio Guglielmino <guglielmino@gmail.com>

"""

import threading
import random
import string
import datetime
import paho.mqtt.client as mqtt
from paho.mqtt.client import MQTTv311
import ssl

class MQTTBroker():
    _connect_timestamp = None

    """
    MQTT Brocker implementation.
    """
    def __init__(self, qos=0, username=None, password=None, ca_cert_file=None, cert_file=None, key_file=None, clientid=None, logger=None):

        if clientid is None:
            self._myid = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))
        else:
            self._myid = clientid
        
        self._qos = qos
        self._client = mqtt.Client(client_id=self._myid, clean_session=True, userdata=None, protocol=MQTTv311, transport="tcp")

        if username is not None:
            self._client.username_pw_set(username, password=password)

        if cert_file is not None and key_file is not None:
            try:
                # cert_reqs=ssl.CERT_NONE,
                self._client.tls_set( ca_certs=ca_cert_file, certfile=cert_file, keyfile=key_file)     
                self._client.tls_insecure_set(True)
            except Exception as ex:
                logger.error(ex)

        self._client.on_connect = self._on_connect
        self._client.on_disconnect = self._on_disconnect
        if logger is not None:
            self._client.enable_logger(logger)
        self._logger = logger 

        self._thread_started = False
        self._is_connected = False
        self._loop_thread = LoopThread(self._client)

    # internal managed callbacks

    def _on_connect(self, client, userdata, flags, rc):
        self._is_connected = True
        if self.connect_cb is not None:
            self.connect_cb(userdata)

        self._logger.info(f"Connected with result code {rc} - {userdata}")

    def _on_disconnect(self, client, userdata, rc):
        self._is_connected = False
        if self.disconnect_cb is not None:
            self.disconnect_cb(userdata, rc)
        self._logger.error(f"Disconnected with result code {rc} - {userdata}")

    def set_connect_cb(self, cb):
        self.connect_cb = cb

    def set_disconnect_cb(self, cb):
        self.disconnect_cb = cb

    @property
    def is_connected(self):
        return self._is_connected
        
    def connect(self, broker_address, port):
        self._broker_address = broker_address;
        self._port = port

        self._client.connect(broker_address, port=port, keepalive=60)
        self._client.user_data_set(f"{broker_address}:{self._port}")
        self._connect_timestamp = datetime.datetime.now()
        
    def disconnect(self):
        self._client.disconnect()

    def broker_info(self):
        return {
            "ip": self._broker_address or "",
            "port": self._port or "",
            "timestamp": self._connect_timestamp
        }

    def reinitialise(self):
        """
        Reinitialise the connection "forgetting" all subscriptions and so on...
        """
        self._client.reinitialise(client_id=self._myid, clean_session=True, userdata=None)

    def set_subscriber_callback(self, on_message):
        """
        set the callback to be called when a message comes from
        a subscribed topic. Method signature is: def on_message(client, userdata, message)
        """
        self._client.on_message = on_message

    def subscribe(self, topic):
        return self._client.subscribe(topic, qos=self._qos)
    
    def publish(self, topic, payload):
        self._client.publish(topic, payload=payload, qos=self._qos)

    @property
    def is_started(self):
        return self._thread_started
        
    def start(self):
        if self._thread_started == False:
            self._thread_started = True
            self._loop_thread.start()
        #self._client.loop_start()

    def stop(self):
        if self._thread_started == True:
            self._loop_thread.join()
            self._thread_started = False
        #self._client.loop_stop()


class LoopThread(threading.Thread):
    def __init__(self, client):
        super(LoopThread,self).__init__()
        self._client = client
    
        return

    def run(self):
        while True:
            self._client.loop(.1)

        return

if __name__ == "__main__": 
    import time

    b = MQTTBroker("127.0.0.1")
    b.connect()
    b.set_subscriber_callback(lambda client, userdata, message : print(message))
    b.subscribe("/test")    
    b.start()
    while True:
        time.sleep(1)


