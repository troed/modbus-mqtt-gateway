import json
import sys
import time
import queue
import copy
import traceback

from consts import MQTT_RESP_TOPIC
from datetime import datetime, timezone
from core.models import BusEvent, EventTypes, SensorValue, ModbusValue, MQTTMessage 
from core.cmdmap import get_commands_map, get_settings_command_map
from core.strategies import add_to_map, HistoryItem, get_registers_strategies, get_bits_strategies, evaluate
from modbus.responseTypes import ModBusRespType, modbustype_from_register
from modbus.valueUtils import scale_modbus_value

from pymodbus.pdu import ExceptionResponse

def get_modbus_producer(table, mbus):
    modbus_funcs = {
        ModBusRespType.HOLDING_REG: mbus.read_holding_registers,
        ModBusRespType.INPUT_REG: mbus.read_input_registers,
        ModBusRespType.COILS: mbus.read_coils,
        ModBusRespType.DISCRETE_INPUT: mbus.read_discrete_inputs
    }
    def modbus_producer(queue):
        try:
            for tab_item in table:
                # Note: modbus type is deduced from first element in range string, ie. 40001-40004 type is deduced using 40001
                modbus_resp_type = modbustype_from_register(tab_item.registries[0]['address'])
                resp = modbus_funcs[modbus_resp_type](tab_item.registries[0]['index'], len(tab_item.registries), int(tab_item.slave_id))
                queue.put(BusEvent(event_type=EventTypes.MODBUS, event_payload=dict(tab_item=tab_item, mbus_value=ModbusValue(reg_str=tab_item.registry_range, mbus_resp=resp))))
        except Exception as ex:
                queue.put(BusEvent(event_type=EventTypes.ERROR, event_payload=dict(system=EventTypes.MODBUS, exception=ex)))

    return modbus_producer

def get_mqtt_message_producer(queue):
    """
    Return the function, to be connected to a MQTT subcription, that handle payload 
    deserialization and put it in queue for processing
    """
    def on_mqtt_message(client, userdata, message):
        try:
            mg_object = json.loads(message.payload)    
            event = BusEvent(event_type=EventTypes.MQTT, event_payload=mg_object)
            queue.put(event)
        except Exception as ex:
            queue.put(BusEvent(event_type=EventTypes.ERROR, event_payload=dict(system=EventTypes.MQTT, exception=ex)))
            print("Excpt receiving {0}".format(ex))

    return on_mqtt_message


def interpolate_topic(topic, **kwargs):
    ret = topic
    try:
        ret = topic.format(**kwargs)
    except KeyError:
        pass
    return ret




def bit_modbus_value(bits):
    """
    Manage values made of array of boolean flag representing bit statuses in the response (COILS and DISCRETE INPUT)
    """
    # BitMap : ''.join([str(int(x)) for x in bits])
    return int(bits[0])

def make_sensor_value(tab_item, read_value, mbus_value):
    # Decoding value read from ModBuse device by read type
    value = 0
    if mbus_value.resp_type == ModBusRespType.HOLDING_REG or mbus_value.resp_type == ModBusRespType.INPUT_REG:
        value = [scale_modbus_value(int(val), float(tab_item.cscaling_coeff), float(tab_item.offset)) for val in read_value]
    elif mbus_value.resp_type == ModBusRespType.COILS or mbus_value.resp_type == ModBusRespType.DISCRETE_INPUT: 
        value = [bit_modbus_value(read_value)]

    return SensorValue(tag_name=tab_item.topic, 
                                ui=tab_item.ui if tab_item.add_ui else '', 
                                value=value,
                                descr=tab_item.descr if tab_item.add_descr else '' , 
                                opt1=tab_item.opt1, 
                                opt2=tab_item.opt2, 
                                opt3=tab_item.opt3, 
                                utc_timestamp = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S%z"))



# TODO: Too big, split the code
def get_modbus_handler(mqtt, history_map: dict, slave_status_map: dict, add_to_state, cdr_table):
    """
    Factory for the main ModBus event manager 
    """
    def handle_modbus_event(event_payload):
        """
        Handle a ModBus event, produced by the modbus_producer, it decodes the payload and read data from the ModBus resp
        """
        local_slave_status_map = {}
        tab_item = event_payload['tab_item']
        mbus_value = event_payload['mbus_value']
        
        if isinstance(mbus_value.mbus_resp, Exception) or isinstance(mbus_value.mbus_resp, ExceptionResponse):
            slave_status_map[str(tab_item.slave_id)] = False
            mqtt.publish(topic=tab_item.topic, payload=json.dumps({ 'error': str(mbus_value.mbus_resp) }))
        else:
            slave_status_map[str(tab_item.slave_id)] = True
            check_and_send_value(mqtt, tab_item, history_map, mbus_value, add_to_state, cdr_table)
            
    return  handle_modbus_event

def mbus_resp_type(mbus_resp):
    """
    Define the message type based on the property present in the ModBus response.
    At this time only 'registers' and 'bits' are allowed
    
    Arguments:
        mbus_resp {object} -- ModBus response coming from ModBus library
    
    Returns:
        str -- 'registers' or 'bits'
    """
    if hasattr(mbus_resp, 'registers'):
            return 'registers'
    elif hasattr(mbus_resp, 'bits'):
        return 'bits'  
    return  'registers'

def get_decoders(mbus_resp_type):
    """
    Define a map to abstracts function to decode data from ModBus read based on type 
    
    Arguments:
        mbus_resp_type {str} -- type of ModBus reading, should be 'registers' or 'bits'
    
    Returns:
        dict -- Dictionary with functions to decode read_value, get strategies and value to be store in state table
    """
    # Note: assuming that default is a 'registers' read
    ret =  { 
            'read_value': lambda mbus: mbus.registers,
            'strategies': get_registers_strategies,
            'state_value': lambda read_value, sens_val: read_value[0]
    }

    if mbus_resp_type == 'bits':
        ret = {
            'read_value': lambda mbus: mbus.bits,
            'strategies': get_bits_strategies,
            'state_value': lambda read_value, sens_val: sens_val.value
        }   

    return ret
    
def normalize_modbus_value(mbus_value, tab_item):
    decoders = get_decoders(mbus_resp_type(mbus_value.mbus_resp))

    read_value = decoders['read_value'](mbus_value.mbus_resp)
    sens_val = make_sensor_value(tab_item, read_value, mbus_value)
    return (sens_val, read_value, decoders)

def check_and_send_value(mqtt, tab_item, history_map, mbus_value, add_to_state, cdr_table):
    """
    Main function in charge of managing data validation, transformation and send to MQTT
    
    Arguments:
        mqtt {object} -- Mqtt client
        tab_item {TableItem} -- Item from the registries to topic mapping table
        history_map {dic} -- Map to store history 
        mbus_value {object} -- Object wrapper for ModBus library (coming from pymodbus lib)
        add_to_state {function} -- Function to add value to the state table, used to refresh output registers
    """
    (sens_val, read_value, decoders) = normalize_modbus_value(mbus_value, tab_item)
    strategies = decoders['strategies'](tab_item, history_map)

    if send_value_to_mqtt(mqtt, tab_item, sens_val, strategies, cdr_table):
        add_to_map(history_map, tab_item.slave_id, tab_item.registry_range, HistoryItem(time.time(), sens_val.value))
        if add_to_state and 'w' in tab_item.access.lower():
            # Assuming that W(rite) registers aren't range but single value
            # we get read_value[0] 
            add_to_state(tab_item.topic, decoders['state_value'](read_value, sens_val))

def send_value_to_mqtt(mqtt, tab_item, sens_val, strategies, cdr_table):
    # Check with strategy before pushing 
    if not isinstance(sens_val.value, (list,)):
        values = [sens_val.value]
    else:
        values = sens_val.value
    evaluate_res = evaluate(strategies, register_string=tab_item.registry_range, current_values=values, current_timestamp=time.time())
    if evaluate_res:  
        sens_val_tosend = copy.copy(sens_val)  
        sens_val_tosend.value = sens_val.value if len(sens_val.value) > 1 else sens_val.value[0]  
        dict_payload = sens_val_tosend.__dict__
        if cdr_table and tab_item.topic in cdr_table:
            dict_payload = {**sens_val_tosend.__dict__, **{'CDR': cdr_table[tab_item.topic] }}
            cdr_table.pop(tab_item.topic, None)
        mqtt.publish(topic=tab_item.topic, payload=json.dumps(dict_payload))
    return evaluate_res

def get_mqtt_handler(gatewayId, mqtt, commands):
    """
    Handle mapping between MQTT commands (ie. Gtw, MqttWrite, ...) and specific command implementation
    """
    def handle_mqtt_event(event_payload):
        command_type = 'UNKNOWN'
        try:
            command_type = event_payload['type'].lower()
            if command_type in commands:
                commands[command_type].execute(event_payload['payload'])
            else:
                raise Exception("Missing command {0}".format(command_type))
        except Exception as e:
            payload = { 'error': str(e), 'details': traceback.format_exc() } 
            msg = MQTTMessage(type=command_type, payload=payload)
            mqtt.publish(MQTT_RESP_TOPIC.format(gatewayId=gatewayId), json.dumps(msg.__dict__))
        


    return handle_mqtt_event


def get_state_refresher(queue, out_state_table):
    def state_refresh():
        """
        Refresh status of output registers creating "internal" MQTT messages like they were received from outside (note: idsender is "internal" to identify them)
        """
        for topic, value in out_state_table.items():
            message = '{{"type": "regwrite", "payload": {{"topic": "{topic}", "value": "{value}", "timestamp": "{timestamp}", "idsender": "internal"}}}}'.format(
                topic=topic,
                value=value,
                timestamp=datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S%z")
            )
            mg_object = json.loads(message)    
            event = BusEvent(event_type=EventTypes.MQTT, event_payload=mg_object)
            queue.put(event)

    return state_refresh


def get_state_table_initializator(out_state_table, logger = None):
    """
    Fill the out_state_table keeping the state of input to periodically refresh them)
    add_to_state_table manage it like a cache, adding item if it doesn't exist
    Table is a dict in the form { "topic": value }, for example
    { 
        "12334/DO1": 1,
        "12334/DO2": 0,
        "12334/AO1": 532.2,
    }

    """
    def add_to_state_table(table_item_topic, value):
        if table_item_topic not in out_state_table:
            out_state_table[table_item_topic] = value
            if logger:
                logger.info(f"add {value} to {table_item_topic} in output regs refresh table")

    return add_to_state_table




        
