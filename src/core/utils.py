import os
import datetime
import tailer

def tail(fname, lines, blkeywords = []):
    rows = tailer.tail(open(fname), lines)
    rows =list(filter(lambda item: next(filter(lambda kword: kword in item, blkeywords), None) == None, rows))
    return '\n'.join(map(str, rows))


def get_file_datetime_str(file_path):
    if(os.path.exists(file_path)):
        return datetime.datetime.fromtimestamp(os.path.getmtime(file_path)).strftime('%d/%m/%Y %H:%M:%S')
    else:
       return None

def validate_file(file_content ,start_line, end_line):
    if file_content is None or len(file_content) == 0:
        return False
    lines = file_content.strip().split('\n')
    return (start_line in lines[0].strip()) and (end_line in lines[-1].strip())