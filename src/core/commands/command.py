import json

class Command:
    """Base class for commands classes
    """
    def __init__(self, **kwargs):
        self._mqtt = kwargs['mqtt']
        self._topic = kwargs['topic']

    def execute(self, command_payload=None): 
        pass

    def send_response(self, msg):
        self._mqtt.publish(self._topic, json.dumps(msg.__dict__))
        