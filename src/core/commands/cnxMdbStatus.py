from core.commands.command import Command
from core.models import MQTTMessage
from consts import CMD_NAME_CNXMDBSTATUS

class CnxMdbStatusCommand(Command):
    """
    Gwt command, handle table request from MQTT Broker, serialize it and publish n the requested topic
    """
    def __init__(self, **kwargs):
        super(CnxMdbStatusCommand, self).__init__(**kwargs)
        self._table = kwargs['table']
        self._slave_status_map = kwargs['slave_status_map']

    def execute(self, command_payload=None): 

        slave_ids = list(set([ti.slave_id for ti in self._table]))
        status = { str(key): False for key in slave_ids }
        for slave_id, value in self._slave_status_map.items():
            status[str(slave_id)] = value

        msg = MQTTMessage(type=CMD_NAME_CNXMDBSTATUS, payload={'status': status})

        self.send_response(msg)
        
