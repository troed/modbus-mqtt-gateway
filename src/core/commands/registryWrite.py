from core.models import MQTTMessage
from core.commands.command import Command
from modbus.responseTypes import modbustype_from_register, ModBusRespType
from modbus.valueUtils import unscale_modbus_value
    

class RegistryWriteCommand(Command):
    """
    Command received on custom topic ({gateway_id}/{table_item_topic}) to manage MQTT to device communication
    """

    def __init__(self, **kwargs):
        super(RegistryWriteCommand,self).__init__(**kwargs)
        self._modbus = kwargs['modbus']
        self._table = kwargs['table']
        self._gatewayId = kwargs['gatewayId']
        self._out_state_table = kwargs['out_state_table']
        self._cdr_table = kwargs['cdr_table']
      

    def execute(self, command_payload=None):
        payload_topic = command_payload['topic'].format(gatewayId=self._gatewayId)
        tab_item = next(filter(lambda x: x.topic.format(gatewayId=self._gatewayId) == payload_topic, self._table), None)
        try:
            if tab_item is not None:
                for reg in tab_item.registries:
                    mbus_resp_type = modbustype_from_register(reg['address'])
                    if(mbus_resp_type == ModBusRespType.COILS):
                        self._modbus.write_coil(address=reg['index'], value=int(float(command_payload['value'])), unit=tab_item.slave_id)
                    else:
                        value_to_send = unscale_modbus_value(float(command_payload['value']), tab_item.cscaling_coeff, tab_item.offset)
                        self._modbus.write_registers(address=reg['index'], values=[int(value_to_send)], unit=tab_item.slave_id)
                    self._out_state_table[payload_topic] = command_payload['value']
                    if 'CDR' in command_payload:
                        self._cdr_table[payload_topic] = command_payload['CDR']
            else: 
                raise Exception("Topic '{0}' not found in table, check 'topic' field in payload".format(command_payload['topic']))
        except Exception as ex:
            raise Exception(f"Error '{ex}' [topic = {command_payload['topic']}] ")
