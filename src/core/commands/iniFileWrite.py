from core.commands.command import Command
from core.commands.writeSettingValue import WriteSettingValueCommand
from core.models import MQTTMessage

class IniFileWriteCommand(Command):

    def __init__(self, **kwargs):
        super(IniFileWriteCommand, self).__init__(**kwargs)
        self._setting = kwargs['setting_provider']
        self._ini_filename = kwargs['ini_filename']
        self._cmd_name = kwargs['cmd_name']
        self._sett_strategy = kwargs.get('sett_strategy', lambda sett: sett)

    def execute(self, command_payload=None):
        command_payload = self._sett_strategy(command_payload)
        with open(self._ini_filename, 'w') as output:
            writeCommand = WriteSettingValueCommand(mqtt=self._mqtt, topic=self._topic, 
                                        setting_provider=self._setting, output_stream=output)
            writeCommand.execute(command_payload=command_payload)
            msg = MQTTMessage(type=self._cmd_name, payload={})  
            self.send_response(msg)
                    