from core.models import MQTTMessage
from core.commands.command import Command

class WriteSettingValueCommand(Command):
    """
    Take a setting provider and given a payload with { "key": "value" } 
    try to set "value" in the setting provider "key"
    """
    def __init__(self, **kwargs):
        super(WriteSettingValueCommand, self).__init__(**kwargs)
        self._setting = kwargs['setting_provider']
        self._outputStream = kwargs['output_stream']

    def execute(self, command_payload=None):
        for item in command_payload['settings']:
            for k, v in item.items():
                setattr(self._setting, k, v)

        self._setting.write(self._outputStream)