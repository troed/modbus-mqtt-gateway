import os
import json
from core.commands.gtwWrite import GtwWriteCommand
from table.table_item import TableItem
from unittest.mock import MagicMock

def test_gtwwriter(mocker):
    mqtt = MagicMock()

    mocker.patch.object(mqtt, 'publish', autospec=False)

    file_name = './test_gtwwriter.csv.log'
    command = GtwWriteCommand(mqtt=mqtt, topic='/sample/topic', file_path=file_name)
    command.execute('{"csv": "topic;descr;cscaling_coeff;offset;ui;registry_range;slave_id;access;send_on_variation;variation;variation_type;recurring_send;recurring_span;add_ui;add_descr;opt1;opt2;opt3\\n/sample1;descr1;1;0;V;40001-40003;1;R;S;10;%;S;1;S;S;opt1_1;opt2_1;opt3_1\\n/sample1;descr1;1;0;V;40005;1;R;S;10;%;S;1;S;S;opt1_1;opt2_1;opt3_1"}')
    exists = os.path.isfile(file_name)
    assert exists
    os.remove(file_name)
    mqtt.publish.assert_called_with('/sample/topic', '{"type": "gtwwrite", "payload": {}}')

def test_gtwwriter_validation_error(mocker):
    mqtt = MagicMock()

    mocker.patch.object(mqtt, 'publish', autospec=False)

    file_name = './test_gtwwriter.csv.log'
    command = GtwWriteCommand(mqtt=mqtt, topic='/sample/topic', file_path=file_name)
    command.execute('{"csv": "topic;descr;cscaling_coeff;offset;ui;registry_range;slave_id;access;send_on_variation;variation;variation_type;recurring_send;recurring_span;add_ui;add_descr;opt1;opt2;opt3\\n/sample1;descr1;1;0;V;40001-40003;1;R;S;10;U;S;1;S;S;opt1_1;opt2_1;opt3_1\\n/sample1;descr1;1;0;V;40005;1;R;S;10;%;S;1;S;S;opt1_1;opt2_1;opt3_1"}')
    exists = os.path.isfile(file_name)
    assert not exists
    mqtt.publish.assert_called_with('/sample/topic', '{"type": "gtwwrite", "payload": {"error": "write failed", "write_errors": [["invalid \'variation_type\' value (U)"]]}}')