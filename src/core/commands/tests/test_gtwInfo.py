import json
from core.commands.gtwInfo import GtwInfoCommand
from table.table_item import TableItem
from unittest.mock import MagicMock


def  test_gtwinfo(mocker):
    mqtt = MagicMock()

    mocker.patch.object(mqtt, 'publish', autospec=False)
    command = GtwInfoCommand(version='1.0.1', mqtt=mqtt, topic='/sample/topic')

    command.execute()
    json_resp = '{"type": "gtwinfo", "payload": {"version": "1.0.1"}}'
    
    mqtt.publish.assert_called_with('/sample/topic', json_resp)
    