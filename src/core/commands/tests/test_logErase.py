import os
import logging
from core.commands.logErase import LogEraseCommand
from unittest.mock import MagicMock


def test_logerase_command(mocker):
    test_log_file = '/tmp/test_logger.log'
    test_logger = logging.getLogger('test.logger')
    handler = logging.handlers.RotatingFileHandler(filename=test_log_file, backupCount=5, maxBytes=3000000)
    handler.setFormatter(logging.Formatter('%(levelname)s %(message)s'))
    test_logger.addHandler(handler)
    test_logger.setLevel(logging.DEBUG)

    for i in range(1, 10):
        test_logger.info('Test logging {0}'.format(i))

    assert os.path.exists(test_log_file) == 1
    mqtt = MagicMock()

    command = LogEraseCommand(mqtt=mqtt, topic='/sample/topic', logger=test_logger, cmd_name='testcmd')
    command.execute()

    statinfo = os.stat(test_log_file)
    assert statinfo.st_size == 0
    os.remove(test_log_file)