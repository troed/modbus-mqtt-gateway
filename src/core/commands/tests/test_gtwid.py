import json
from core.commands.gtwId import GtwIDCommand
from table.table_item import TableItem
from unittest.mock import MagicMock

def test_gtw(mocker):

    mqtt = MagicMock()

    mocker.patch.object(mqtt, 'publish', autospec=True)
    command = GtwIDCommand(gtwid="1234", mqtt=mqtt, topic='/sample/topic')

    command.execute()
    table_json = '{"type": "gtwid", "payload": {"id": "1234"}}'
    
    mqtt.publish.assert_called_with('/sample/topic', table_json)
    