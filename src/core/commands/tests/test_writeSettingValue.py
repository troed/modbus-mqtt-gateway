import json
import configparser
from io import StringIO

from mock import MagicMock

from core.commands.writeSettingValue import WriteSettingValueCommand
from settings import GeneralSettings, CellularModuleSettings, SerialMDBSettings, MQTTSettings

def test_gtwidwrite_settings_write(mocker):
    raw_payload = """
    {  
        "settings":[  
                { "gtwid":"TEST_ID" }
        ]
    }
    """
    payload = json.loads(raw_payload)
    mqtt = MagicMock()
    config = configparser.ConfigParser()

    setting = GeneralSettings(config, 
                                gtwid='12334', 
                                name='test', 
                                adminusr='admin', 
                                adminpwd='admin', 
                                sysusr='root', 
                                syspwd='root')

    output = StringIO()
    command = WriteSettingValueCommand(mqtt=mqtt, topic='/sample/topic', 
                                        setting_provider=setting, output_stream=output)
    command.execute(payload)
    assert setting.gtwid == "TEST_ID" 

def test_mqtt_settings_write():
    raw_payload = """
    {  
        "settings":[  
                { "broker_pri":"mqtt.test.com" },
                { "broker_sec":"mqtt2.test.com" },
                { "qos":"2" },
                { "user_auth":"test_auth" },
                { "cert_auth":"test_cert" }
        ]
    }
 
    """
    payload = json.loads(raw_payload)
    mqtt = MagicMock()
    config = configparser.ConfigParser()
    
    setting = MQTTSettings(config, 
                            broker_pri='mqtt1.nowhere.org', 
                            broker_sec='mqtt2.nowhere.org', 
                            qos='0', 
                            user_auth='S',
                            cert_auth='N')


    output = StringIO()
    command = WriteSettingValueCommand(mqtt=mqtt, topic='/sample/topic', 
                                        setting_provider=setting, output_stream=output)
    command.execute(payload)

    assert setting.broker_pri == "mqtt.test.com" 
    assert setting.broker_sec == "mqtt2.test.com" 
    assert setting.qos == "2"
    assert setting.user_auth == "test_auth"
    assert setting.cert_auth == "test_cert"

