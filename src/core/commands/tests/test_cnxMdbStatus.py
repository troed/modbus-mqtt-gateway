import json
from core.commands.cnxMdbStatus import CnxMdbStatusCommand
from table.table_item import TableItem
from unittest.mock import MagicMock
from core.strategies import HistoryItem
from datetime import datetime


def test_cnxmdbstatus(mocker):
    slave_status_map = {}
     
    slave_status_map['1'] = True
    slave_status_map['2'] = True

    table = [TableItem(topic='/test',
                    descr='decription',
                    cscaling_coeff=1,
                    offset=10,
                    ui='V',
                    registry_range='41000',
                    slave_id=1,
                    access='W',
                    send_on_variation='S',
                    variation=12,
                    variation_type='%',
                    recurring_send='N',
                    recurring_span=0,
                    add_ui='N',
                    add_descr='N',
                    opt1='o1-1',
                    opt2='o2-1',
                    opt3='o3-1'),
            TableItem(topic='/test',
                    descr='decription',
                    cscaling_coeff=1,
                    offset=10,
                    ui='V',
                    registry_range='41000',
                    slave_id=2,
                    access='W',
                    send_on_variation='S',
                    variation=12,
                    variation_type='%',
                    recurring_send='N',
                    recurring_span=0,
                    add_ui='N',
                    add_descr='N',
                    opt1='o1-1',
                    opt2='o2-1',
                    opt3='o3-1'),
            TableItem(topic='/test',
                    descr='decription',
                    cscaling_coeff=1,
                    offset=10,
                    ui='V',
                    registry_range='41000',
                    slave_id=3,
                    access='W',
                    send_on_variation='S',
                    variation=12,
                    variation_type='%',
                    recurring_send='N',
                    recurring_span=0,
                    add_ui='N',
                    add_descr='N',
                    opt1='o1-1',
                    opt2='o2-1',
                    opt3='o3-1')]
    
    mqtt = MagicMock()

    mocker.patch.object(mqtt, 'publish', autospec=False)
    command = CnxMdbStatusCommand(table=table, slave_status_map=slave_status_map, mqtt=mqtt, topic='/sample/topic')

    command.execute()

    resp_json = '{"type": "cnxmdbstatus", "payload": {"status": {"1": true, "2": true, "3": false}}}'

    mqtt.publish.assert_called_with('/sample/topic', resp_json)

    slave_status_map['2'] = False
    command.execute()

    resp_json = '{"type": "cnxmdbstatus", "payload": {"status": {"1": true, "2": false, "3": false}}}'
    mqtt.publish.assert_called_with('/sample/topic', resp_json)