import sys, os
from unittest.mock import MagicMock
from core.commands.readFile import ReadFileCommand



def test_readFile(mocker):
    mqtt = MagicMock()
    mocker.patch.object(mqtt, 'publish', autospec=False)

    app_dir = os.path.dirname(os.path.realpath(__file__))
    command = ReadFileCommand(mqtt=mqtt, topic='/sample/topic', file_path=os.path.join(app_dir, './fixtures/modbus.samplelog'), cmd_name='test-log-cmd', lines=2, resp_key='log')
    command.execute()

    mqtt.publish.assert_called_with('/sample/topic', '{"type": "test-log-cmd", "payload": {"log": "2019-03-21 18:16:33,934 - app.modbus - INFO - reading from address 4 on slave_id 1\\n2019-03-21 18:16:33,967 - app.modbus - INFO - reading from address 1 on slave_id 1"}}')


def test_readFile_with_block_kwords(mocker):
    mqtt = MagicMock()
    mocker.patch.object(mqtt, 'publish', autospec=False)

    app_dir = os.path.dirname(os.path.realpath(__file__))
    command = ReadFileCommand(mqtt=mqtt, topic='/sample/topic-ini', file_path=os.path.join(app_dir, './fixtures/serial.ini'), cmd_name='test-ini-cmd', lines=300, resp_key='data', block_kwords=['polling_time'])
    command.execute()

    mqtt.publish.assert_called_with('/sample/topic-ini', '{"type": "test-ini-cmd", "payload": {"data": "[SERIAL MODBUS]\\nport = /dev/serial0\\nspeed = 19200\\nparity = N\\nstopbit = 8\\nbitlen = 8\\nmaster_id = 1234\\nconnect_timeout = 6\\nretry_count = 8"}}')

def content_enricher(content):
    return "A " + content

def test_readFile_with_enricher(mocker):
    mqtt = MagicMock()
    mocker.patch.object(mqtt, 'publish', autospec=False)

    app_dir = os.path.dirname(os.path.realpath(__file__))
    command = ReadFileCommand(mqtt=mqtt, topic='/sample/topic-ini', file_path=os.path.join(app_dir, './fixtures/serial.ini'), cmd_name='test-ini-cmd', lines=300, resp_key='data', block_kwords=['polling_time'], content_enricher=content_enricher)
    command.execute()

    mqtt.publish.assert_called_with('/sample/topic-ini', '{"type": "test-ini-cmd", "payload": {"data": "A [SERIAL MODBUS]\\nport = /dev/serial0\\nspeed = 19200\\nparity = N\\nstopbit = 8\\nbitlen = 8\\nmaster_id = 1234\\nconnect_timeout = 6\\nretry_count = 8"}}')
