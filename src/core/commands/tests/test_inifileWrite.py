import os
import configparser
from settings import MQTTSettings
from core.commands.iniFileWrite import IniFileWriteCommand
from unittest.mock import MagicMock, Mock



def test_mqttwrite_command(mocker):
    ini_file = '/tmp/test_mqtt_file'
    mqtt = MagicMock()

    config = configparser.ConfigParser()
    
    setting = MQTTSettings(config, 
                            broker_pri='mqtt1.nowhere.org', 
                            broker_sec='mqtt2.nowhere.org', 
                            qos='0', 
                            user_auth='S',
                            cert_auth='N')

    mqttWrite = IniFileWriteCommand(mqtt=mqtt, topic='/sample/topic', 
                                setting_provider=setting, ini_filename=ini_file, cmd_name='mqttwrite')

    payload = {  
        "settings":[  
                { "broker_pri":"mqtt.test.com" },
                { "broker_sec":"mqtt2.test.com" },
                { "qos":"2" },
                { "user_auth":"test_auth" },
                { "cert_auth":"test_cert" }
        ]
    }

    mqttWrite.execute(command_payload=payload)
    assert os.path.exists(ini_file) == 1
    os.remove(ini_file)

def test_mqttwrite_with_setting_stategy():
    ini_file = '/tmp/test_mqtt_strat_file'
    mqtt = MagicMock()

    config = configparser.ConfigParser()
    
    setting = MQTTSettings(config, 
                            broker_pri='mqtt1.nowhere.org', 
                            broker_sec='mqtt2.nowhere.org', 
                            qos='0', 
                            user_auth='S',
                            cert_auth='N',
                            cert_file='''
-----BEGIN CERTIFICATE-----
MIIEZDCCA0ygAwIBAgIUHvbWkHKpY7rjDVBcsIP0C8kXqj0wDQYJKoZIhvcNAQEN
BQAwajEXMBUGA1UEAwwOQW4gTVFUVCBicm9rZXIxFjAUBgNVBAoMDU93blRyYWNr
cy5vcmcxFDASBgNVBAsMC2dlbmVyYXRlLUNBMSEwHwYJKoZIhvcNAQkBFhJub2Jv
ZHlAZXhhbXBsZS5uZXQwHhcNMTkxMjEyMDgyMjEzWhcNMzIxMjA4MDgyMjEzWjAb
MRkwFwYDVQQDDBBpbmZvQGFjbW90ZWMuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOC
AQ8AMIIBCgKCAQEAnhqq8O9PExWGmG4UtXYF3FHJluTGFdY5HkYTXf6vF5Pbvxfh
3leKqSXcmV1p30SabB1x1561R6UxKbvlR88yVMsUs6falR1gClSYpeZZnMb5fIQl
Or4gfDtPGkIcxg7NkQ5QUHDxIsHslN4mKyJahVdwIqj/N7ZIt4P8evybBZA4cIpD
k7ksfIajeCE/ZCNSxXLDYUGGqxy4IEpUMmsDm5NA3IC3MG1ECgTydiqanyJA+b4A
U8OGD3iHWh0LFM57Um6PaO7Mxv6PA+I3lPna+paL6X2CsZIbqmNjqHRhzW+LsnOg
WjLCMOc/4lGuf+hgPZReea45s0Gq+r8kDFsIpQIDAQABo4IBTzCCAUswDAYDVR0T
AQH/BAIwADAJBgNVHREEAjAAMBEGCWCGSAGG+EIBAQQEAwIFoDAdBgNVHSUEFjAU
BggrBgEFBQcDAgYIKwYBBQUHAwQwCwYDVR0PBAQDAgOoMCgGCWCGSAGG+EIBDQQb
FhlDbGllbnQgQnJva2VyIENlcnRpZmljYXRlMB0GA1UdDgQWBBRWzofYh+v/wfi1
BOeia5waBnv/ZTCBpwYDVR0jBIGfMIGcgBSTMpM9ifplznPi5iy5kCgw/0JX36Fu
pGwwajEXMBUGA1UEAwwOQW4gTVFUVCBicm9rZXIxFjAUBgNVBAoMDU93blRyYWNr
cy5vcmcxFDASBgNVBAsMC2dlbmVyYXRlLUNBMSEwHwYJKoZIhvcNAQkBFhJub2Jv
ZHlAZXhhbXBsZS5uZXSCFGyOZBEjnLtZ7mbHdAyCrQWFT4UxMA0GCSqGSIb3DQEB
DQUAA4IBAQA7PcsTjLZNwRGXB+AnoK4hIh6abJlabk8kF8DOTJ7ToqkI8cpVG2ks
P6J3JhJRpYSi+OMk6x1M/Lr3HuvcvxePNfYI/pvyCRcNHoi3E3KaVq09KW6ekFj+
15q4i259kIP2ql2jsDo9Hm0fRPSSxdPXBq8GOSr1kRo7tgowBVxw5QFF4uqCCqL5
3CVl4FlquhClEVhnFA04zJuNETX1vOYfxErrd7MIyvCgrAUr8B9GQIto+iWTp8pR
/xdJssu7bW6wlCHy1Bz+CrQ0xiPmDqhLRCrLduGLqwKw9xS+kvzSQOTMDs3DzQI3
aZcROP67AGdyU5wkyGAC9cGys3j08IJX
-----END CERTIFICATE-----
''')

    def change_settings(payload):
        return payload
        
    mock = Mock()
    mock.side_effect = change_settings

    mqttWrite = IniFileWriteCommand(mqtt=mqtt, topic='/sample/topic', 
                                setting_provider=setting, ini_filename=ini_file, cmd_name='mqttwrite', sett_strategy=mock)

    payload = {  
        "settings":[  
                { "broker_pri":"mqtt.test.com" },
                { "broker_sec":"mqtt2.test.com" },
                { "qos":"2" },
                { "user_auth":"test_auth" },
                { "cert_auth":"test_cert" },
                { "cert_file":"/tmp/changed.crt" }
        ]
    }

    mqttWrite.execute(command_payload=payload)
    mock.assert_called_once()
    assert os.path.exists(ini_file) == 1
    ini_cont = open(ini_file).read()
    assert "cert_file = /tmp/changed.crt" in ini_cont
    os.remove(ini_file)