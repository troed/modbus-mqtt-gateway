import sys, os
from unittest.mock import MagicMock
from core.commands.gtwReboot import GtwRebootCommand


def test_gtwreboot(mocker):
    mqtt = MagicMock()

    mocker.patch.object(mqtt, 'publish', autospec=False)

    command = GtwRebootCommand(mqtt=mqtt, topic='/sample/topic')
    mocker.patch.object(command, 'restart_program', autospec=False)
    command.execute()

    command.restart_program.assert_called_once()
    mqtt.publish.assert_called_with('/sample/topic', '{"type": "gtwreboot", "payload": {}}')

