import json
import pytest
from mock import MagicMock
from table.table_item import TableItem
from core.commands.registryWrite import RegistryWriteCommand


def test_registrywrite(mocker):
    tableArray = [
        TableItem(topic='test/topic',
                    descr='test descr',
                    cscaling_coeff=0,
                    offset=0,
                    ui='A',
                    registry_range='40001',
                    slave_id=1,
                    access='R',
                    send_on_variation='S',
                    variation=5,
                    variation_type='%',
                    recurring_send='S',
                    recurring_span=1,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1',
                    opt2='o2',
                    opt3='o3'),
        TableItem(topic='{gatewayId}/samplesub',
                    descr='decription',
                    cscaling_coeff=1,
                    offset=0,
                    ui='V',
                    registry_range='40002',
                    slave_id=123,
                    access='R/W',
                    send_on_variation='S',
                    variation=12,
                    variation_type='%',
                    recurring_send='N',
                    recurring_span=0,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1-1',
                    opt2='o2-1',
                    opt3='o3-1')
    ]

    mqtt = MagicMock()
    
    mbus = MagicMock()
    mocker.patch.object(mbus, 'write_registers')

    command = RegistryWriteCommand(table=tableArray, out_state_table={}, cdr_table={}, mqtt=mqtt, modbus=mbus, topic='', gatewayId='12334')
    command.execute(json.loads('{"topic":"12334/samplesub","value":"4","CDR":"","timestamp":"2019-03-17T07:39:28","idsender":"8888"}'))
 
    mbus.write_registers.assert_called_with(address=1,values=[4], unit=123)


def test_registrywrite_update_out_status(mocker):
    tableArray = [
        TableItem(topic='test/topic',
                    descr='test descr',
                    cscaling_coeff=0,
                    offset=0,
                    ui='A',
                    registry_range='40001',
                    slave_id=1,
                    access='R',
                    send_on_variation='S',
                    variation=5,
                    variation_type='%',
                    recurring_send='S',
                    recurring_span=1,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1',
                    opt2='o2',
                    opt3='o3'),
        TableItem(topic='{gatewayId}/samplesub',
                    descr='decription',
                    cscaling_coeff=1,
                    offset=10,
                    ui='V',
                    registry_range='40002',
                    slave_id=123,
                    access='R/W',
                    send_on_variation='S',
                    variation=12,
                    variation_type='%',
                    recurring_send='N',
                    recurring_span=0,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1-1',
                    opt2='o2-1',
                    opt3='o3-1')
    ]

    mqtt = MagicMock()
    
    mbus = MagicMock()
    mocker.patch.object(mbus, 'write_registers')

    out_status = {}
    command = RegistryWriteCommand(table=tableArray, out_state_table=out_status, cdr_table={}, mqtt=mqtt, modbus=mbus, topic='', gatewayId='12334')
    command.execute(json.loads('{"topic":"12334/samplesub","value":"4","CDR":"","timestamp":"2019-03-17T07:39:28","idsender":"8888"}'))

    # Address 40002 is 1 when device is addressed
    assert "12334/samplesub" in  out_status
    assert out_status["12334/samplesub"] == '4'
 
def test_registrywrite_right_mbus_write(mocker):
    tb1 = TableItem(topic='{gatewayId}/samplesub',
                    descr='test descr',
                    cscaling_coeff=0,
                    offset=0,
                    ui='A',
                    registry_range='00001',
                    slave_id=1,
                    access='R/W',
                    send_on_variation='S',
                    variation=5,
                    variation_type='%',
                    recurring_send='S',
                    recurring_span=1,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1',
                    opt2='o2',
                    opt3='o3')
        
    mqtt = MagicMock()
    
    mbus = MagicMock()
    mocker.patch.object(mbus, 'write_registers')
    mocker.patch.object(mbus, 'write_coil')

    out_status = {}
    command = RegistryWriteCommand(table=[tb1], out_state_table=out_status, cdr_table={}, mqtt=mqtt, modbus=mbus, topic='', gatewayId='12334')
    command.execute(json.loads('{"topic":"12334/samplesub","value":"4","CDR":"","timestamp":"2019-03-17T07:39:28","idsender":"8888"}'))

    assert mbus.write_coil.called
    assert not mbus.write_registers.called

    tb2 = TableItem(topic='{gatewayId}/samplesub',
                    descr='test descr',
                    cscaling_coeff=0,
                    offset=0,
                    ui='A',
                    registry_range='40001',
                    slave_id=1,
                    access='R/W',
                    send_on_variation='S',
                    variation=5,
                    variation_type='%',
                    recurring_send='S',
                    recurring_span=1,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1',
                    opt2='o2',
                    opt3='o3')

    mbus = MagicMock()

    command = RegistryWriteCommand(table=[tb2], out_state_table=out_status, cdr_table={}, mqtt=mqtt, modbus=mbus, topic='', gatewayId='12334')
    command.execute(json.loads('{"topic":"12334/samplesub","value":"4","CDR":"","timestamp":"2019-03-17T07:39:28","idsender":"8888"}'))

    assert not mbus.write_coil.called
    assert mbus.write_registers.called

def test_registrywrite_float_value(mocker):
    tb1 = TableItem(topic='{gatewayId}/samplesub',
                    descr='test descr',
                    cscaling_coeff=0,
                    offset=0,
                    ui='A',
                    registry_range='00001',
                    slave_id=1,
                    access='R/W',
                    send_on_variation='S',
                    variation=5,
                    variation_type='%',
                    recurring_send='S',
                    recurring_span=1,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1',
                    opt2='o2',
                    opt3='o3')
        
    
    tb2 = TableItem(topic='{gatewayId}/samplesub2',
                    descr='test descr',
                    cscaling_coeff=0,
                    offset=0,
                    ui='A',
                    registry_range='40001',
                    slave_id=1,
                    access='R/W',
                    send_on_variation='S',
                    variation=5,
                    variation_type='%',
                    recurring_send='S',
                    recurring_span=1,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1',
                    opt2='o2',
                    opt3='o3')

    mqtt = MagicMock()   
    mbus = MagicMock()

    out_status = {}

    command = RegistryWriteCommand(table=[tb1, tb2], out_state_table=out_status, cdr_table={}, mqtt=mqtt, modbus=mbus, topic='', gatewayId='12334')
    try:
        command.execute(json.loads('{"topic":"12334/samplesub","value":"4.4","CDR":"","timestamp":"2019-03-17T07:39:28","idsender":"8888"}'))
    except:
        pytest.fail("Should not raise Exception ..")
    

    command = RegistryWriteCommand(table=[tb1, tb2], out_state_table=out_status, cdr_table={}, mqtt=mqtt, modbus=mbus, topic='', gatewayId='12334')
    try:
        command.execute(json.loads('{"topic":"12334/samplesub2","value":"3.4","CDR":"","timestamp":"2019-03-17T07:39:28","idsender":"8888"}'))
    except:
        pytest.fail("Should not raise Exception ..")
    
    

def test_registrywrite_cdr(mocker):
    tb1 = TableItem(topic='{gatewayId}/samplesub',
                    descr='test descr',
                    cscaling_coeff=0,
                    offset=0,
                    ui='A',
                    registry_range='00001',
                    slave_id=1,
                    access='R/W',
                    send_on_variation='S',
                    variation=5,
                    variation_type='%',
                    recurring_send='S',
                    recurring_span=1,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1',
                    opt2='o2',
                    opt3='o3')
        
    
    tb2 = TableItem(topic='{gatewayId}/samplesub2',
                    descr='test descr',
                    cscaling_coeff=0,
                    offset=0,
                    ui='A',
                    registry_range='40001',
                    slave_id=1,
                    access='R/W',
                    send_on_variation='S',
                    variation=5,
                    variation_type='%',
                    recurring_send='S',
                    recurring_span=1,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1',
                    opt2='o2',
                    opt3='o3')

    mqtt = MagicMock()   
    mbus = MagicMock()


    cdr_table = {}

    command = RegistryWriteCommand(table=[tb1, tb2], out_state_table={}, cdr_table=cdr_table, mqtt=mqtt, modbus=mbus, topic='', gatewayId='12334')
    command.execute(json.loads('{"topic":"12334/samplesub","value":"4.4","CDR":"568","timestamp":"2019-03-17T07:39:28","idsender":"8888"}'))
    assert "12334/samplesub" in cdr_table
    assert cdr_table["12334/samplesub"] == "568"

    
