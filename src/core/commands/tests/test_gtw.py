import json
from core.commands.gtw import GtwCommand
from table.table_item import TableItem
from unittest.mock import MagicMock

def test_gtw(mocker):
    tableArray = [
        TableItem(topic='test/topic',
                    descr='test descr',
                    cscaling_coeff=0,
                    offset=0,
                    ui='A',
                    registry_range='40000',
                    slave_id=1,
                    access='R',
                    send_on_variation='S',
                    variation=5,
                    variation_type='%',
                    recurring_send='S',
                    recurring_span=1,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1',
                    opt2='o2',
                    opt3='o3'),
        TableItem(topic='prova/topic',
                    descr='decription',
                    cscaling_coeff=1,
                    offset=10,
                    ui='V',
                    registry_range='41000',
                    slave_id=123,
                    access='W',
                    send_on_variation='S',
                    variation=12,
                    variation_type='%',
                    recurring_send='N',
                    recurring_span=0,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1-1',
                    opt2='o2-1',
                    opt3='o3-1')
    ]

    mqtt = MagicMock()

    mocker.patch.object(mqtt, 'publish', autospec=False)
    command = GtwCommand(table=tableArray, mqtt=mqtt, topic='/sample/topic')

    command.execute()
    table_json = '{"type": "gtw", "payload": {"csv": "topic;descr;cscaling_coeff;offset;ui;registry_range;slave_id;access;send_on_variation;variation;variation_type;recurring_send;recurring_span;add_ui;add_descr;opt1;opt2;opt3\\r\\ntest/topic;test descr;0;0;A;40000;1;R;S;5;%;S;1;S;S;o1;o2;o3\\r\\nprova/topic;decription;1;10;V;41000;123;W;S;12;%;N;0;S;S;o1-1;o2-1;o3-1\\r\\n"}}'
    
    mqtt.publish.assert_called_with('/sample/topic', table_json)
    