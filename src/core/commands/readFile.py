import tailer
import os, sys
from core.models import MQTTMessage
from core.commands.command import Command
from core.utils import tail

class ReadFileCommand(Command):
    def __init__(self, **kwargs):
        super(ReadFileCommand,self).__init__(**kwargs)
        self._file_path = kwargs['file_path']
        self._cmd_name = kwargs['cmd_name']
        self._resp_key = kwargs['resp_key'] if 'resp_key' in kwargs else 'log'
        self._lines = kwargs['lines'] if 'lines' in kwargs else 200
        self._block_kwords =  kwargs.get('block_kwords', [])
        self._content_enricher = kwargs.get('content_enricher', lambda c: c)

    def execute(self, command_payload=None): 
        content = tail(self._file_path, self._lines, self._block_kwords)
        content = self._content_enricher(content)
        msg = MQTTMessage(type=self._cmd_name, payload={self._resp_key: content })
        self.send_response(msg)