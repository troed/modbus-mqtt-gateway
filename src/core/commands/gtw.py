import json
from io import StringIO
from table.csvtable import CsvTable
from consts import CMD_NAME_GTW
from core.commands.command import Command
from core.models import MQTTMessage


class GtwCommand(Command):
    """
    Gwt command, handle table request from MQTT Broker, serialize it and publish n the requested topic
    """
    def __init__(self, **kwargs):
        super(GtwCommand, self).__init__(**kwargs)
        self._table = kwargs['table']

    def execute(self, command_payload=None): 
        """
        Execute the command, in the Gwt context it means publishing the JSON serialized table to MQTT
        """   
        output = StringIO() 
        c = CsvTable(output)
        c.tableWriter(self._table )
        msg = MQTTMessage(type=CMD_NAME_GTW, payload={'csv': output.getvalue() })

        self.send_response(msg)
        
