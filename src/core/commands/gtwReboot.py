import json
import os, sys
import psutil
from consts import CMD_NAME_GTWREBOOT
from core.models import MQTTMessage
from core.commands.command import Command

class GtwRebootCommand(Command):
    """
    Reboot command, script is restarted.
    """
    def __init__(self, **kwargs):
        super(GtwRebootCommand, self).__init__(**kwargs)

    def execute(self, command_payload=None):
        try:
            msg = MQTTMessage(type=CMD_NAME_GTWREBOOT, payload={})  
            self.send_response(msg)
        except:
            pass

        self.restart_program()

    def restart_program(self):
        """
        Restarts the current program, with file objects and descriptors
        cleanup
        """
        p = psutil.Process(os.getpid())
        for handler in p.open_files() + p.connections():
            os.close(handler.fd)
        python = sys.executable
        os.execl(python, python , *sys.argv)
