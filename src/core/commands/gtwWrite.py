import json
from io import StringIO
from core.commands.command import Command
from table.csvtable import CsvTable
from table.register_topic_handler import writeRegisterTopicTable
from consts import CMD_NAME_GTWWRITE
from core.models import MQTTMessage

class GtwWriteCommand(Command):
    """
    Gwt command, writes a new table replacing the current one
    """
    def __init__(self, **kwargs):
        """
        Args:
            file_path: Where local CSV table is stored
  
        """
        super(GtwWriteCommand,self).__init__(**kwargs)
        self._file_path = kwargs['file_path']

    def execute(self, command_payload=None): 
        pyaload = json.loads(command_payload)
        f = StringIO(pyaload['csv'])
        c = CsvTable(f) 
        (errors, table) = c.tableListValidated()
        payload = {}
        if len(errors) > 0:
            payload = { 'error': 'write failed', 'write_errors': errors } 
        else:
            writeRegisterTopicTable(self._file_path, table)
        
        msg = MQTTMessage(type=CMD_NAME_GTWWRITE, payload=payload)
    
        self.send_response(msg)

        
