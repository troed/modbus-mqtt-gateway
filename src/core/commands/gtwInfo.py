from consts import CMD_NAME_GTWINFO
from core.commands.command import Command
from core.models import MQTTMessage


class GtwInfoCommand(Command):
    
    def __init__(self, **kwargs):
        super(GtwInfoCommand, self).__init__(**kwargs)
        self._gtw_version = kwargs['version']

    def execute(self, command_payload=None): 
        """
        Execute the command returning the GatewayId as read from the configuration
        """  
        msg = MQTTMessage(type=CMD_NAME_GTWINFO, payload={'version': self._gtw_version })
        self.send_response(msg)