from loggers import clearLog
from core.models import MQTTMessage
from core.commands.command import Command


class LogEraseCommand(Command):
    """
    Generic log erase command, passed a log object it erase attached file (if logger if bound to handler managing file logging)
    """
    def __init__(self, **kwargs):
        super(LogEraseCommand, self).__init__(**kwargs)
        self._logger = kwargs['logger']
        self._cmd_name = kwargs['cmd_name']

    def execute(self, command_payload=None): 
        clearLog(self._logger)
        msg = MQTTMessage(type=self._cmd_name, payload={})  
        self.send_response(msg)