import json
from consts import CMD_NAME_GTWID
from core.commands.command import Command
from core.models import MQTTMessage

class GtwIDCommand(Command):
    def __init__(self, **kwargs):
        super(GtwIDCommand, self).__init__(**kwargs)
        self._gtwid = kwargs['gtwid']

    def execute(self, command_payload=None): 
        """
        Execute the command returning the GatewayId as read from the configuration
        """  
        msg = MQTTMessage(type=CMD_NAME_GTWID, payload={'id': self._gtwid })
        self.send_response(msg)