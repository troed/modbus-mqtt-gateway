import json
from core.utils import validate_file
from consts import (
    TLS_CERT_CLIENT_PATH,
    TLS_KEY_CLIENT_PATH,
    TLS_CA_PATH
)

class WrongCertException(Exception):
    pass

""" 
Receive a list of disctionaries like 
[
    {'broker_pri': 'mqtt.test.com'}, 
    {'broker_sec': 'mqtt2.test.org'}, 
    {'qos': '0'}, {'user_auth': 'N'},
    {'cert_auth': 'N'}, 
    { 'cert_file': '-----BEGIN CERTIFIC...CATE-----'}, 
    {'ca_file': '-----BEGIN CERTIFIC...CATE-----'}, 
    {'key_file': '-----BEGIN RSA PRIV...EY-----\n'}
]

and transform it in a dictionary like
{
    'broker_pri': 'mqtt.test.com', 
    'broker_sec': 'mqtt2.test.org', 
    'qos': '0',
    'user_auth': 'N',
    'cert_auth': 'N', 
    'cert_file': '-----BEGIN CERTIFIC...CATE-----', 
    'ca_file': '-----BEGIN CERTIFIC...CATE-----', 
    'key_file': '-----BEGIN RSA PRIV...EY-----\n'
}
"""
def MakeSettingsDict(settingsPayload):
    ret = dict()

    for item in settingsPayload:
        for k, v in item.items():
            ret[k] = v
    return ret

def MakeSettingsList(settings):
    ret = []

    for k, v in settings.items():
        ret.append({k: v})

    return { 'settings': ret}

"""[summary]

settings is an array of dictionaries like:

[
    {'broker_pri': 'mqtt.test.com'}, 
    {'broker_sec': 'mqtt2.test.org'}, 
    {'qos': '0'}, {'user_auth': 'N'},
    {'cert_auth': 'N'}, 
    { 'cert_file': '-----BEGIN CERTIFIC...CATE-----'}, 
    {'ca_file': '-----BEGIN CERTIFIC...CATE-----'}, 
    {'key_file': '-----BEGIN RSA PRIV...EY-----\n'}
]

"""
def validateCerts(settings):
    if settings['cert_auth'] == 'S':
        if not 'cert_file' in settings:
            raise WrongCertException("Missing server certificate")
        if not validate_file(settings['cert_file'], '-----BEGIN CERTIFICATE-----', '-----END CERTIFICATE-----'):
            raise WrongCertException("Server certificate not valid")
        
        if not 'ca_file' in settings:
            raise WrongCertException("Missing CA certificate ")
        if not validate_file(settings['ca_file'], '-----BEGIN CERTIFICATE-----', '-----END CERTIFICATE-----'):
            raise WrongCertException("CA certificate not valid")
        
        if not 'key_file' in settings:
            raise WrongCertException("Missing Key certificate")
        if not validate_file(settings['key_file'], '-----BEGIN RSA PRIVATE KEY-----', '-----END RSA PRIVATE KEY-----'):
            raise WrongCertException("Key certificate not valid")


def rewriteCertsWithPath(payload: dict):
    settings = MakeSettingsDict(payload['settings'])
    validateCerts(settings)
    if 'cert_file' in settings and settings['cert_file'] is not None:
        with open(TLS_CERT_CLIENT_PATH, "w") as f:
            f.write(settings['cert_file'])
            settings['cert_file'] = TLS_CERT_CLIENT_PATH

    if 'key_file' in settings and settings['key_file'] is not None:
        with open(TLS_KEY_CLIENT_PATH, "w") as f:
            f.write(settings['key_file'])
            settings['key_file'] = TLS_KEY_CLIENT_PATH
    
    if 'ca_file' in settings and settings['ca_file'] is not None:
        with open(TLS_CA_PATH, "w") as f:
            f.write(settings['ca_file'])
            settings['ca_file'] = TLS_CA_PATH

    return MakeSettingsList(settings)