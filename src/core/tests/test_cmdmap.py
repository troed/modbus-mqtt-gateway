import json
from unittest.mock import Mock, MagicMock, patch, ANY

from consts import (CMD_NAME_GTWINFO,
                    CMD_NAME_GTW,
                    CMD_NAME_GTWIDRITE,
                    CMD_NAME_GTWREBOOT,
                    CMD_NAME_GTWID,
                    CMD_NAME_REGWRITE,
                    CMD_NAME_GTWLOG,
                    CMD_NAME_MQTTLOG,
                    CMD_NAME_MQTTREBOOT,
                    CMD_NAME_CNXMDBLOG,
                    CMD_NAME_CNXMDBREBOOT,
                    CMD_NAME_CNXMDBSTATUS,
                    CMD_NAME_MQTT,
                    CMD_NAME_CNSMDB,
                    CMD_NAME_4G,
                    CMD_NAME_4GWRITE,
                    CMD_NAME_MQTTWRITE,
                    CMD_NAME_GTWLOGERASE,
                    CMD_NAME_MQTTLOGERASE,
                    CMD_NAME_CNXMDBERASE,
                    CMD_NAME_4GLOGERASE)

from table.table_item import TableItem
from core.cmdmap import get_commands_map, get_settings_command_map, get_logging_command_map, get_gtw_command_map, get_mdb_command_map
from core.commands.gtw import GtwCommand
from core.commands.gtwInfo import GtwInfoCommand
from core.commands.gtwReboot import GtwRebootCommand
from core.commands.gtwId import GtwIDCommand
from core.commands.registryWrite import RegistryWriteCommand
from core.commands.readFile import ReadFileCommand
from core.commands.iniFileWrite import IniFileWriteCommand
from core.commands.logErase import LogEraseCommand
from core.commands.cnxMdbStatus import CnxMdbStatusCommand

# Constext variables
tableArray = [
        TableItem(topic='test/topic',
                    descr='test descr',
                    cscaling_coeff=0,
                    offset=0,
                    ui='A',
                    registry_range='40000',
                    slave_id=1,
                    access='R',
                    send_on_variation='S',
                    variation=5,
                    variation_type='%',
                    recurring_send='S',
                    recurring_span=1,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1',
                    opt2='o2',
                    opt3='o3'),
        TableItem(topic='prova/topic',
                    descr='decription',
                    cscaling_coeff=1,
                    offset=10,
                    ui='V',
                    registry_range='41000',
                    slave_id=123,
                    access='R/W',
                    send_on_variation='S',
                    variation=12,
                    variation_type='%',
                    recurring_send='N',
                    recurring_span=0,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1-1',
                    opt2='o2-1',
                    opt3='o3-1')
    ]

sets = MagicMock()

def test_get_cmd_gtw():
    cmds = get_commands_map(topics_table=tableArray, sets=sets, mqtt=None, mbus=None)
    assert CMD_NAME_GTW in cmds
    assert type(cmds[CMD_NAME_GTW]) is GtwCommand

def test_get_cmd_gtwreboot():
    cmds = get_commands_map(topics_table=tableArray, sets=sets, mqtt=None, mbus=None)
    assert CMD_NAME_GTWREBOOT in cmds
    assert type(cmds[CMD_NAME_GTWREBOOT]) is GtwRebootCommand

def test_get_cmd_gtwid():
    cmds = get_commands_map(topics_table=tableArray, sets=sets, mqtt=None, mbus=None)
    assert CMD_NAME_GTWID in cmds
    assert type(cmds[CMD_NAME_GTWID]) is GtwIDCommand

def test_get_cmd_gtwlog():
    cmds = get_commands_map(topics_table=tableArray, sets=sets, mqtt=None, mbus=None)
    assert CMD_NAME_GTWLOG in cmds
    assert type(cmds[CMD_NAME_GTWLOG]) is ReadFileCommand

def test_get_cmd_mqttlog():
    cmds = get_commands_map(topics_table=tableArray, sets=sets, mqtt=None, mbus=None)
    assert CMD_NAME_MQTTLOG in cmds
    assert type(cmds[CMD_NAME_MQTTLOG]) is ReadFileCommand

def test_get_cmd_mqttreboot():
    cmds = get_commands_map(topics_table=tableArray, sets=sets, mqtt=None, mbus=None)
    assert CMD_NAME_MQTTREBOOT in cmds
    assert type(cmds[CMD_NAME_MQTTREBOOT]) is GtwRebootCommand


def test_get_cmd_cnxmdblog():
    cmds = get_commands_map(topics_table=tableArray, sets=sets, mqtt=None, mbus=None)
    assert CMD_NAME_CNXMDBLOG in cmds
    assert type(cmds[CMD_NAME_CNXMDBLOG]) is ReadFileCommand

def test_get_cmd_mqtt():
    cmds = get_commands_map(topics_table=tableArray, sets=sets, mqtt=None, mbus=None)
    assert CMD_NAME_MQTT in cmds
    assert type(cmds[CMD_NAME_MQTT]) is ReadFileCommand

def test_get_cmd_cnxmdb():
    cmds = get_commands_map(topics_table=tableArray, sets=sets, mqtt=None, mbus=None)
    assert CMD_NAME_CNSMDB in cmds
    assert type(cmds[CMD_NAME_CNSMDB]) is ReadFileCommand


def test_get_cmd_cnxmdbreboot():
    cmds = get_commands_map(topics_table=tableArray, sets=sets, mqtt=None, mbus=None)
    assert CMD_NAME_CNXMDBREBOOT in cmds
    assert type(cmds[CMD_NAME_CNXMDBREBOOT]) is GtwRebootCommand

def test_get_cmd_4g():
    cmds = get_commands_map(topics_table=tableArray, sets=sets, mqtt=None, mbus=None)
    assert CMD_NAME_4G in cmds
    assert type(cmds[CMD_NAME_4G]) is ReadFileCommand

## Settings related command map

def test_get_cmd_mqttwrite():
    cmds = get_settings_command_map(sets=sets, mqtt=None)
    assert CMD_NAME_MQTTWRITE in cmds
    assert type(cmds[CMD_NAME_MQTTWRITE]) is IniFileWriteCommand

def test_get_cmd_mqttwrite():
    cmds = get_settings_command_map(sets=sets, mqtt=None)
    assert CMD_NAME_4GWRITE in cmds
    assert type(cmds[CMD_NAME_4GWRITE]) is IniFileWriteCommand

def test_get_cmd_gtwidwrite():
    cmds = get_settings_command_map(sets=sets, mqtt=None)
    assert CMD_NAME_GTWIDRITE in cmds
    assert type(cmds[CMD_NAME_GTWIDRITE]) is IniFileWriteCommand

## Log related command maps

def test_get_cmd_gtwlogerase():
    cmds = get_logging_command_map(sets=sets, mqtt=None, gateway_logger=None, mqtt_logger=None, cellular_logger=None, modbus_logger=None)
    assert CMD_NAME_GTWLOGERASE in cmds
    assert type(cmds[CMD_NAME_GTWLOGERASE]) is LogEraseCommand

def test_get_cmd_mqttlogerase():
    cmds = get_logging_command_map(sets=sets, mqtt=None, gateway_logger=None, mqtt_logger=None, cellular_logger=None, modbus_logger=None)
    assert CMD_NAME_MQTTLOGERASE in cmds
    assert type(cmds[CMD_NAME_MQTTLOGERASE]) is LogEraseCommand

def test_get_cmd_cnxmdblogerase():
    cmds = get_logging_command_map(sets=sets, mqtt=None, gateway_logger=None, mqtt_logger=None, cellular_logger=None, modbus_logger=None)
    assert CMD_NAME_CNXMDBERASE in cmds
    assert type(cmds[CMD_NAME_CNXMDBERASE]) is LogEraseCommand

def test_get_cmd_4glogerase():
    cmds = get_logging_command_map(sets=sets, mqtt=None, gateway_logger=None, mqtt_logger=None, cellular_logger=None, modbus_logger=None)
    assert CMD_NAME_4GLOGERASE in cmds
    assert type(cmds[CMD_NAME_4GLOGERASE]) is LogEraseCommand

def test_get_gtw_command_map():
    cmds = get_gtw_command_map(sets=sets, mqtt=None, gtw_version='1.2.3')
    assert CMD_NAME_GTWINFO in cmds
    assert type(cmds[CMD_NAME_GTWINFO]) is GtwInfoCommand

def test_get_cnxstatus_command_map():
    cmds = get_mdb_command_map(topics_table=tableArray, slave_status_map={}, out_state_table={}, cdr_table={}, sets=sets, mqtt=None, mbus=None)
    assert CMD_NAME_CNXMDBSTATUS in cmds
    assert type(cmds[CMD_NAME_CNXMDBSTATUS]) is CnxMdbStatusCommand

def test_get_cmd_regostrywrite():
    cmds = get_mdb_command_map(topics_table=tableArray, slave_status_map={}, out_state_table={}, cdr_table={}, sets=sets, mqtt=None, mbus=None)
    assert CMD_NAME_REGWRITE in cmds
    assert type(cmds[CMD_NAME_REGWRITE]) is RegistryWriteCommand