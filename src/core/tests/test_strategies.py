from core.strategies import get_absolute_value_strategy, get_perc_value_strategy, get_elasped_time_strategy, get_value_change_stategy
from core.strategies import HistoryItem, add_to_map, get_registers_strategies, get_bits_strategies, evaluate
from table.table_item import TableItem

def test_elapsed():
    history = {}
    add_to_map(history, '1', '400001', HistoryItem(1552330441, [513]))
    add_to_map(history, '1', '400002', HistoryItem(1552330496, [232]))

    need_to_push = get_elasped_time_strategy(history, '1', value_change=5)(register_string='400001', current_value=None, val_idx=0, current_timestamp=1552330497)
    assert need_to_push == True

    need_to_push = get_elasped_time_strategy(history, '1', value_change=5)(register_string='400002', current_value=None,  val_idx=0, current_timestamp=1552330497)
    assert need_to_push == False

    need_to_push = get_elasped_time_strategy({}, '1', value_change=5)(register_string='400002', current_value=None,  val_idx=0, current_timestamp=1552330497)
    assert need_to_push == True

def test_perc_value():
    history = {}
    add_to_map(history, '1', '400001', HistoryItem(1552330441, [513]))
    add_to_map(history, '1', '400002', HistoryItem(1552330496, [232]))
    
    need_to_push = get_perc_value_strategy(history, '1', 10)(register_string='400001', current_value=500, val_idx=0, current_timestamp=None)
    assert need_to_push == False

    need_to_push = get_perc_value_strategy(history, '1', 10)(register_string='400001', current_value=460, val_idx=0, current_timestamp=None)
    assert need_to_push == True

    need_to_push = get_perc_value_strategy(history, '1', 10)(register_string='400001', current_value=565, val_idx=0, current_timestamp=None)
    assert need_to_push == True
 
    need_to_push = get_perc_value_strategy(history, '1', 10)(register_string='400001', current_value=560, val_idx=0, current_timestamp=None)
    assert need_to_push == False

    need_to_push = get_perc_value_strategy({}, '1', 10)(register_string='400001', current_value=560, val_idx=0, current_timestamp=None)
    assert need_to_push == True

def test_value_change_stategy():
    history = {}
    add_to_map(history, '1', '00001', HistoryItem(1552330441, ['00000010']))
    add_to_map(history, '1', '00002', HistoryItem(1552330496, ['00000011']))

    need_to_push = get_value_change_stategy(history, '1', None)(register_string='00001', current_value='00000001', val_idx=0, current_timestamp=None)
    assert need_to_push == True

    need_to_push = get_value_change_stategy(history, '1', None)(register_string='00002', current_value='00000011', val_idx=0, current_timestamp=None)
    assert need_to_push == False


# TODO: More test cases for evaluation
def test_evaluate():
    
    history = {}
    add_to_map(history, '1', '400001', HistoryItem(1552330441, [513]))
    strategies = [get_perc_value_strategy(history, '1', 10)]
    
    current_value=460
   
    res = evaluate(strategies, register_string='400001', current_values=[current_value], current_timestamp=None)
    assert res == True
    
    current_value=500
   
    res = evaluate(strategies, register_string='400001', current_values=[current_value], current_timestamp=None)
    
    assert res == False

def test_evaluate_empty_history():
    
    history = {}
    strategies = [get_perc_value_strategy(history, '1', 10)]
    
    current_value=460
   
    res = evaluate(strategies, register_string='400001', current_values=[current_value], current_timestamp=None)
    assert res == True



def test_evaluate_base():
    strategies = [lambda register_string, current_value, val_idx, current_timestamp: True]
    res = evaluate(strategies, register_string='400001', current_values=[130], current_timestamp=None)
    assert res == True

    strategies = [lambda register_string, current_value, val_idx, current_timestamp: True, lambda register_string, current_value, val_idx, current_timestamp: False]
    res = evaluate(strategies, register_string='400001', current_values=[130], current_timestamp=None)
    assert res == True

    strategies = [lambda register_string, current_value, val_idx, current_timestamp: False, lambda register_string, current_value, val_idx, current_timestamp: False]
    res = evaluate(strategies, register_string='400001', current_values=[130], current_timestamp=None)
    assert res == False

    strategies = [lambda register_string, current_value, val_idx, current_timestamp: True, lambda register_string, current_value, val_idx, current_timestamp: True]
    res = evaluate(strategies, register_string='400001', current_values=[130], current_timestamp=None)
    assert res == True

    strategies = []
    res = evaluate(strategies, register_string='400001', current_values=[130], current_timestamp=None)
    assert res == False

    strategies = [lambda register_string, current_value, val_idx, current_timestamp: register_string=='400001' and current_value==130 and current_timestamp==1552330441]
    res = evaluate(strategies, register_string='400001', current_values=[130], current_timestamp=1552330441)
    assert res == True
    

def test_absolute_value():
    history = {}
    add_to_map(history, '1', '400001', HistoryItem(1552330441, [513]))
    add_to_map(history, '1', '400002', HistoryItem(1552330496, [232]))

    need_to_push = get_absolute_value_strategy(history, '1', value_change=10)(register_string='400001', current_value=524, val_idx=0, current_timestamp=None)
    assert need_to_push == True

    need_to_push = get_absolute_value_strategy(history, '1', value_change=10)(register_string='400001', current_value=502, val_idx=0, current_timestamp=None)
    assert need_to_push == True

    need_to_push = get_absolute_value_strategy(history, '1', value_change=10)(register_string='400001', current_value=510, val_idx=0, current_timestamp=None) 
    assert need_to_push == False
    
    need_to_push = get_absolute_value_strategy(history, '1', value_change=10)(register_string='400001', current_value=522, val_idx=0, current_timestamp=None) 
    assert need_to_push == False

    need_to_push = get_absolute_value_strategy({}, '1', value_change=10)(register_string='400001', current_value=522, val_idx=0, current_timestamp=None) 
    assert need_to_push == True

def test_get_registers_strategies():
    ti = TableItem(topic='test/topic',
                    descr='test descr',
                    cscaling_coeff=0,
                    offset=0,
                    ui='A',
                    registry_range='40000',
                    slave_id=1,
                    access='R',
                    send_on_variation='S',
                    variation=5,
                    variation_type='%',
                    recurring_send='S',
                    recurring_span=1,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1',
                    opt2='o2',
                    opt3='o3')

    stats = get_registers_strategies(ti, {})
    
    assert next((True for item in stats if item.__name__ == 'perc_value_strategy'), False) == True 
    assert next((True for item in stats if item.__name__ == 'elasped_time_strategy'), False) == True 
    assert next((True for item in stats if item.__name__ == 'absolute_value_strategy'), False) == False 

    ti = TableItem(topic='test/topic',
                    descr='test descr',
                    cscaling_coeff=0,
                    offset=0,
                    ui='A',
                    registry_range='40000',
                    slave_id=1,
                    access='R',
                    send_on_variation='N',
                    variation=5,
                    variation_type='%',
                    recurring_send='S',
                    recurring_span=1,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1',
                    opt2='o2',
                    opt3='o3')

    stats = get_registers_strategies(ti, {})

    assert next((True for item in stats if item.__name__ == 'perc_value_strategy'), False) == False 
    assert next((True for item in stats if item.__name__ == 'elasped_time_strategy'), False) == True 
    assert next((True for item in stats if item.__name__ == 'absolute_value_strategy'), False) == False 
    
    ti = TableItem(topic='test/topic',
                    descr='test descr',
                    cscaling_coeff=0,
                    offset=0,
                    ui='A',
                    registry_range='40000',
                    slave_id=1,
                    access='R',
                    send_on_variation='S',
                    variation=5,
                    variation_type='A',
                    recurring_send='F',
                    recurring_span=1,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1',
                    opt2='o2',
                    opt3='o3')

    stats = get_registers_strategies(ti, {})

    assert next((True for item in stats if item.__name__ == 'perc_value_strategy'), False) == False 
    assert next((True for item in stats if item.__name__ == 'elasped_time_strategy'), False) == False 
    assert next((True for item in stats if item.__name__ == 'absolute_value_strategy'), False) == True 

def test_get_bits_strategies():
    ti = TableItem(topic='test/topic',
                    descr='test descr',
                    cscaling_coeff=0,
                    offset=0,
                    ui='A',
                    registry_range='00001',
                    slave_id=1,
                    access='R',
                    send_on_variation='S',
                    variation=5,
                    variation_type='%',
                    recurring_send='S',
                    recurring_span=1,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1',
                    opt2='o2',
                    opt3='o3')

    stats = get_bits_strategies(ti, {})
    
    assert next((True for item in stats if item.__name__ == 'value_change_strategy'), False) == True 
    assert next((True for item in stats if item.__name__ == 'elasped_time_strategy'), False) == True

    ti = TableItem(topic='test/topic',
                    descr='test descr',
                    cscaling_coeff=0,
                    offset=0,
                    ui='A',
                    registry_range='30001',
                    slave_id=1,
                    access='R',
                    send_on_variation='S',
                    variation=5,
                    variation_type='%',
                    recurring_send='N',
                    recurring_span=1,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1',
                    opt2='o2',
                    opt3='o3')

    stats = get_bits_strategies(ti, {})

    assert next((True for item in stats if item.__name__ == 'value_change_strategy'), False) == True 
    assert next((True for item in stats if item.__name__ == 'elasped_time_strategy'), False) == False
        