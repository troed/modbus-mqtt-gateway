import pytest
import json
from unittest.mock import Mock, MagicMock, patch, ANY
from core.cert_validation import validateCerts
from settings import MQTTSettings
from core.cert_validation import validateCerts, MakeSettingsDict, WrongCertException

def test_makeSettingsDict():
    data = json.load(open('./core/tests/fixtures/mqtt_payload.json'))
    settings = MakeSettingsDict(data['settings'])

    assert settings['broker_pri'] == "mqtt.test.com"
    assert settings['broker_sec'] == "mqtt2.test.org"
    assert settings['qos'] == "0"


def test_validate_success():
    try:
        data = json.load(open('./core/tests/fixtures/mqtt_payload.json'))
        settings = MakeSettingsDict(data['settings'])

        validateCerts(settings)
    except Exception as ex:
        pytest.fail(f"Unexpected exception {ex}")

def test_validate_server_cert_fail():
    with pytest.raises(WrongCertException) as excinfo:
        data = json.load(open('./core/tests/fixtures/mqtt_payload.json'))
        settings = MakeSettingsDict(data['settings'])

        settings['cert_auth'] = 'S'
        settings['cert_file'] = open('./core/tests/fixtures/server_bad.crt').read()
        settings['ca_file'] = open('./core/tests/fixtures/ca.crt').read()
        settings['key_file'] = open('./core/tests/fixtures/server.key').read()
        validateCerts(settings)

    assert "Server certificate not valid" in str(excinfo.value)



def test_validate_ca_cert_fail():
    with pytest.raises(WrongCertException) as excinfo:
        data = json.load(open('./core/tests/fixtures/mqtt_payload.json'))
        settings = MakeSettingsDict(data['settings'])

        settings['cert_auth'] = 'S'
        settings['cert_file'] = open('./core/tests/fixtures/server.crt').read()
        settings['ca_file'] = open('./core/tests/fixtures/ca_bad.crt').read()
        settings['key_file']= open('./core/tests/fixtures/server.key').read()
        validateCerts(settings)

    assert "CA certificate not valid" in str(excinfo.value)

def test_validate_ca_cert_missing():
    with pytest.raises(WrongCertException) as excinfo:
        data = json.load(open('./core/tests/fixtures/mqtt_payload.json'))
        settings = MakeSettingsDict(data['settings'])

        settings['cert_auth'] = 'S'
        settings['cert_file'] = open('./core/tests/fixtures/server.crt').read()
        del(settings['ca_file'])
        settings['key_file']= open('./core/tests/fixtures/server.key').read()
        validateCerts(settings)

    assert "Missing CA certificate" in str(excinfo.value)


def test_validate_key_cert_fail():
    with pytest.raises(WrongCertException) as excinfo:
        data = json.load(open('./core/tests/fixtures/mqtt_payload.json'))
        settings = MakeSettingsDict(data['settings'])

        settings['cert_auth'] = 'S'
        settings['cert_file'] = open('./core/tests/fixtures/server.crt').read()
        settings['ca_file'] = open('./core/tests/fixtures/ca.crt').read()
        settings['key_file'] = open('./core/tests/fixtures/server_bad.key').read()
        validateCerts(settings)

    assert "Key certificate not valid" in str(excinfo.value)

def test_validate_key_cert_fail1():
    with pytest.raises(WrongCertException) as excinfo:
        data = json.load(open('./core/tests/fixtures/mqtt_payload.json'))
        settings = MakeSettingsDict(data['settings'])

        settings['cert_auth'] = 'S'
        settings['cert_file'] = open('./core/tests/fixtures/server.crt').read()
        settings['ca_file'] = open('./core/tests/fixtures/ca.crt').read()
        settings['key_file'] = None
        validateCerts(settings)

    assert "Key certificate not valid" in str(excinfo.value)

