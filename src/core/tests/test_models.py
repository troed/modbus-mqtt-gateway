
import pytest
import json
import datetime
from core.models import (BusEvent, 
                        EventTypes, 
                        SensorValue, 
                        ModbusValue,
                        MQTTMessage)

from modbus.responseTypes import ModBusRespType

def test_valid_busevent():
    be = BusEvent(event_type=EventTypes.MODBUS, event_payload=dict(tab_item="item", mobus_read="resp"))
    assert be.event_type == EventTypes.MODBUS
    assert be.event_payload['tab_item'] == "item"
    assert be.event_payload['mobus_read'] == "resp"


def test_invalid_type_busevent():
    with pytest.raises(Exception):
        BusEvent(event_type="MyEvent", event_payload=dict(tab_item="item", mobus_read="resp"))
 
def test_sensor_value_json():
    value = SensorValue(tag_name='mytag', ui='V', value=12.3, descr='a description', opt1='opt1', opt2='opt2', opt3='opt3')
    jsonValue = json.dumps(value.__dict__)
    reversedValue = json.loads(jsonValue)
    assert reversedValue['tag_name'] == 'mytag'
    assert reversedValue['ui'] == 'V'
    assert reversedValue['value'] == 12.3
    assert reversedValue['descr'] == 'a description'
    assert reversedValue['opt1'] == 'opt1'
    assert reversedValue['opt2'] == 'opt2'
    assert reversedValue['opt3'] == 'opt3'

def test_sensor_value_w_timestamp_json():
    now = datetime.datetime.utcnow().isoformat()
    value = SensorValue(tag_name='mytag', ui='A', value=756.23, descr='a description', opt1='opt1', opt2='opt2', opt3='opt3', utc_timestamp=now)
    jsonValue = json.dumps(value.__dict__)
    reversedValue = json.loads(jsonValue)
    assert reversedValue['tag_name'] == 'mytag'
    assert reversedValue['ui'] == 'A'
    assert reversedValue['value'] == 756.23
    assert reversedValue['timestamp'] == now
    assert reversedValue['descr'] == 'a description'
    assert reversedValue['opt1'] == 'opt1'
    assert reversedValue['opt2'] == 'opt2'
    assert reversedValue['opt3'] == 'opt3'

def test_modbus_value():
    mb_value = ModbusValue(reg_str='00002', mbus_resp={'bits': [1,0,2]})
    assert mb_value.reg_str == '00002'
    assert mb_value.mbus_resp['bits'] == [1,0,2]
    assert mb_value.resp_type == ModBusRespType.COILS

    mb_value = ModbusValue(reg_str='10003', mbus_resp={'bits': [1,0,2]})
    assert mb_value.reg_str == '10003'
    assert mb_value.mbus_resp['bits'] == [1,0,2]
    assert mb_value.resp_type == ModBusRespType.DISCRETE_INPUT

    mb_value = ModbusValue(reg_str='30002', mbus_resp={'registers': [333]})
    assert mb_value.reg_str == '30002'
    assert mb_value.mbus_resp['registers'] == [333]
    assert mb_value.resp_type == ModBusRespType.INPUT_REG

    mb_value = ModbusValue(reg_str='40001', mbus_resp={'registers': [513]})
    assert mb_value.reg_str == '40001'
    assert mb_value.mbus_resp['registers'] == [513]
    assert mb_value.resp_type == ModBusRespType.HOLDING_REG

    # Defaut case for unrecognized reg 
    mb_value = ModbusValue(reg_str='2', mbus_resp={'registers': [513]})
    assert mb_value.reg_str == '2'
    assert mb_value.mbus_resp['registers'] == [513]
    assert mb_value.resp_type == ModBusRespType.COILS

def test_mqtt_message():
    msg = MQTTMessage(type='test_type', payload={'name': 'dictionary' })
    assert msg.type == 'test_type'
    assert msg.payload['name'] == 'dictionary'

    json_repr = json.dumps(msg.__dict__)
    assert json_repr == '{"type": "test_type", "payload": {"name": "dictionary"}}'