import pytest
import queue
import time

from callee import Regex

from core.models import EventTypes, BusEvent, ModbusValue, ModBusRespType
from table.table_item import TableItem
from core.functions import (get_modbus_producer, 
                            scale_modbus_value, 
                            make_sensor_value, 
                            get_decoders,
                            mbus_resp_type,
                            get_modbus_handler, 
                            get_mqtt_message_producer,
                            get_commands_map,
                            get_settings_command_map,
                            get_mqtt_handler,
                            interpolate_topic,
                            get_state_refresher,
                            get_state_table_initializator,
                            send_value_to_mqtt)

from core.strategies import (get_perc_value_strategy, 
                             get_value_change_stategy, 
                             add_to_map, 
                             HistoryItem,
                             get_registers_strategies,
                             get_bits_strategies)


from modbus.modbusinterface import ModbusRTU
from mqtt.broker import MQTTBroker

from unittest.mock import Mock, MagicMock, patch, ANY
from callee import Regex, String


def test_interpolate_topic():
    interpolated = interpolate_topic('/aa/{gatewayId}', gatewayId=10)
    assert interpolated == '/aa/10'

    interpolated = interpolate_topic('/aa')
    assert interpolated == '/aa'

    interpolated = interpolate_topic('/aa/{no_passed_key}', gatewayId=10)
    assert interpolated == '/aa/{no_passed_key}'
    
def test_modbus_producer():
    tabItem1 = TableItem(topic='test/topic',
                    descr='test descr',
                    cscaling_coeff=0,
                    offset=0,
                    ui='A',
                    registry_range='40000',
                    slave_id=1,
                    access='R',
                    send_on_variation='S',
                    variation=5,
                    variation_type='%',
                    recurring_send='S',
                    recurring_span=1,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1',
                    opt2='o2',
                    opt3='o3')

    tabItem2 = TableItem(topic='prova/topic',
                    descr='decription',
                    cscaling_coeff=1,
                    offset=10,
                    ui='V',
                    registry_range='41000',
                    slave_id=123,
                    access='W',
                    send_on_variation='S',
                    variation=12,
                    variation_type='%',
                    recurring_send='N',
                    recurring_span=0,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1-1',
                    opt2='o2-1',
                    opt3='o3-1')

    table = [
        tabItem1,
        tabItem2
    ]

    q = queue.Queue(10)
    mbus = ModbusRTU(port='')
    mbus.read_holding_registers = MagicMock(return_value="FAKE RESP")
    proucer_loop = get_modbus_producer(table, mbus)
    proucer_loop(q)
    qitem = q.get()
    assert qitem.event_type == EventTypes.MODBUS
    assert qitem.event_payload['mbus_value'].reg_str == "40000"
    assert qitem.event_payload['tab_item'].topic == tabItem1.topic

    qitem = q.get()
    assert qitem.event_type == EventTypes.MODBUS
    assert qitem.event_payload['mbus_value'].reg_str == "41000"
    assert qitem.event_payload['tab_item'].topic == tabItem2.topic

    assert q.empty()

def test_modbus_producer_mbus_fn_call():
    q = queue.Queue(10)
    mbus = MagicMock()
    table = [TableItem(topic='test/topic',
                    descr='test descr',
                    cscaling_coeff=0,
                    offset=0,
                    ui='A',
                    registry_range='00001',
                    slave_id=1,
                    access='R',
                    send_on_variation='S',
                    variation=5,
                    variation_type='%',
                    recurring_send='S',
                    recurring_span=1,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1',
                    opt2='o2',
                    opt3='o3')]

    proucer_loop = get_modbus_producer(table, mbus)
    proucer_loop(q)
    assert mbus.read_coils.called

    table = [TableItem(topic='test/topic',
                    descr='test descr',
                    cscaling_coeff=0,
                    offset=0,
                    ui='A',
                    registry_range='10001',
                    slave_id=1,
                    access='R',
                    send_on_variation='S',
                    variation=5,
                    variation_type='%',
                    recurring_send='S',
                    recurring_span=1,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1',
                    opt2='o2',
                    opt3='o3')]

    proucer_loop = get_modbus_producer(table, mbus)
    proucer_loop(q)
    assert mbus.read_discrete_inputs.called

    table = [TableItem(topic='test/topic',
                    descr='test descr',
                    cscaling_coeff=0,
                    offset=0,
                    ui='A',
                    registry_range='30001',
                    slave_id=1,
                    access='R',
                    send_on_variation='S',
                    variation=5,
                    variation_type='%',
                    recurring_send='S',
                    recurring_span=1,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1',
                    opt2='o2',
                    opt3='o3')]

    proucer_loop = get_modbus_producer(table, mbus)
    proucer_loop(q)
    assert mbus.read_input_registers.called

    table = [TableItem(topic='test/topic',
                    descr='test descr',
                    cscaling_coeff=0,
                    offset=0,
                    ui='A',
                    registry_range='40000',
                    slave_id=1,
                    access='R',
                    send_on_variation='S',
                    variation=5,
                    variation_type='%',
                    recurring_send='S',
                    recurring_span=1,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1',
                    opt2='o2',
                    opt3='o3')]

    proucer_loop = get_modbus_producer(table, mbus)
    proucer_loop(q)
    assert mbus.read_holding_registers.called

def test_modbus_producer_mbus_fn_call_with_reg_range():
    q = queue.Queue(10)
    mbus = MagicMock()

    table = [TableItem(topic='test/topic',
                    descr='test descr',
                    cscaling_coeff=0,
                    offset=0,
                    ui='A',
                    registry_range='40010-40012',
                    slave_id=1,
                    access='R',
                    send_on_variation='S',
                    variation=5,
                    variation_type='%',
                    recurring_send='S',
                    recurring_span=1,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1',
                    opt2='o2',
                    opt3='o3')]

    proucer_loop = get_modbus_producer(table, mbus)
    proucer_loop(q)
    assert mbus.read_holding_registers.call_count  == 1
    mbus.read_holding_registers.assert_called_with(9, 3, 1)





def test_make_sensor_value():
    ti = TableItem(topic='prova/topic',
                    descr='decription',
                    cscaling_coeff=1.5,
                    offset=3,
                    ui='V',
                    registry_range='40000',
                    slave_id=123,
                    access='W',
                    send_on_variation='S',
                    variation=12,
                    variation_type='%',
                    recurring_send='N',
                    recurring_span=0,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1-1',
                    opt2='o2-1',
                    opt3='o3-1')
    
    v = make_sensor_value(ti, [513], ModbusValue('40000', MagicMock()))

    assert v.tag_name == 'prova/topic'
    assert v.value == [772.5]
    assert v.ui == 'V'
    assert v.descr == 'decription'
    assert v.opt1 == 'o1-1'
    assert v.opt2 == 'o2-1'
    assert v.opt3 == 'o3-1'

    ti = TableItem(topic='prova/topic',
                    descr='decription',
                    cscaling_coeff=1,
                    offset=10,
                    ui='V',
                    registry_range='41000',
                    slave_id=123,
                    access='W',
                    send_on_variation='S',
                    variation=12,
                    variation_type='%',
                    recurring_send='N',
                    recurring_span=0,
                    add_ui='N',
                    add_descr='N',
                    opt1='o1-1',
                    opt2='o2-1',
                    opt3='o3-1')
    
    v = make_sensor_value(ti, [333], ModbusValue('41000', MagicMock()))

    assert v.tag_name == 'prova/topic'
    assert v.ui == ''
    assert v.descr == ''


def test_make_sensor_value_with_range():
    from modbus.responseTypes import ModBusRespType

    ti = TableItem(topic='test/range',
                    descr='decription',
                    cscaling_coeff=1.0,
                    offset=0,
                    ui='V',
                    registry_range='40001-40004',
                    slave_id=123,
                    access='W',
                    send_on_variation='S',
                    variation=12,
                    variation_type='%',
                    recurring_send='N',
                    recurring_span=0,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1-1',
                    opt2='o2-1',
                    opt3='o3-1')
    
    mbus_resp = MagicMock()
    mbus_resp.resp_type = ModBusRespType.HOLDING_REG 
    v = make_sensor_value(ti, [513], ModbusValue('40001-40004', mbus_resp))
    assert v.tag_name == 'test/range'
    assert v.value == [513]

def test_get_decoders_registers():
    mbus_resp = MagicMock() 
    del mbus_resp.bits
    mbus_resp.registers = [122]

    resp = get_decoders('registers')
    assert 'strategies' in resp
    assert 'read_value' in resp
    assert 'state_value' in resp
    assert resp['strategies'] == get_registers_strategies

def test_get_decoders_bits():
    mbus_resp = MagicMock(autospec=False) 
    del mbus_resp.registers
    mbus_resp.bits = [1,0,0,0,0,0,0,0]

    resp = get_decoders('bits')
    assert 'strategies' in resp
    assert 'read_value' in resp
    assert 'state_value' in resp
    assert resp['strategies'] == get_bits_strategies

def test_mbus_resp_type_registers():
    mbus_resp = MagicMock() 
    del mbus_resp.bits
    resp = mbus_resp_type(mbus_resp)
    assert resp == 'registers'

def test_mbus_resp_type_bits():
    mbus_resp = MagicMock() 
    del mbus_resp.registers
    resp = mbus_resp_type(mbus_resp)
    assert resp == 'bits'

def test_get_modbus_handler():
    mqtt = MagicMock()

    with patch.object(mqtt, 'publish',
                           wraps=mqtt.publish) as monkey:
        
        history_map = {}
        status_map = {}
        handle_modbus_event = get_modbus_handler(mqtt, history_map, status_map, None, {})
        # Mock read from Modbus
        mbus_resp = MagicMock() 
        mbus_resp.registers = [122]

        tb = TableItem(topic='/test',
                    descr='decription',
                    cscaling_coeff=1,
                    offset=10,
                    ui='V',
                    registry_range='41000',
                    slave_id=123,
                    access='W',
                    send_on_variation='S',
                    variation=12,
                    variation_type='%',
                    recurring_send='N',
                    recurring_span=0,
                    add_ui='N',
                    add_descr='N',
                    opt1='o1-1',
                    opt2='o2-1',
                    opt3='o3-1')

        bus_event_payload = dict(tab_item=tb, mbus_value=ModbusValue(reg_str='41000', mbus_resp=mbus_resp))
        handle_modbus_event(bus_event_payload)

        monkey.assert_called_with(topic='/test', payload=String())
        # payload=Regex('{"tag_name": "test", "ui": "", "value": 132.0, "timestamp": ".*", "descr": "", "opt1": "o1-1", "opt2": "o2-1", "opt3": "o3-1"}'))

def test_get_modbus_handler_statusmap(mocker):
    history_map = {}
    status_map = {}

    mqtt = MagicMock()
    mocker.patch.object(mqtt, 'publish', autospec=False)

    tb = TableItem(topic='/test',
                    descr='decription',
                    cscaling_coeff=1,
                    offset=10,
                    ui='V',
                    registry_range='41000',
                    slave_id='123',
                    access='W',
                    send_on_variation='S',
                    variation=12,
                    variation_type='%',
                    recurring_send='N',
                    recurring_span=0,
                    add_ui='N',
                    add_descr='N',
                    opt1='o1-1',
                    opt2='o2-1',
                    opt3='o3-1')

    mbus_resp = MagicMock() 
    mbus_resp.registers = [122]

    handle_modbus_event = get_modbus_handler(mqtt, history_map, status_map, None, {})
    bus_event_payload = dict(tab_item=tb, mbus_value=ModbusValue(reg_str='41000', mbus_resp=mbus_resp))
    assert bus_event_payload['mbus_value'].resp_type == ModBusRespType.HOLDING_REG
    handle_modbus_event(bus_event_payload)

    assert '123' in status_map 
    assert status_map['123']

def test_get_modbus_handler_state_table(mocker):
    history_map = {}
    status_map = {}

    mqtt = MagicMock()
    mocker.patch.object(mqtt, 'publish', autospec=False)

    tb = TableItem(topic='/test',
                    descr='decription',
                    cscaling_coeff=1,
                    offset=0,
                    ui='V',
                    registry_range='41000',
                    slave_id='123',
                    access='W',
                    send_on_variation='S',
                    variation=12,
                    variation_type='%',
                    recurring_send='N',
                    recurring_span=0,
                    add_ui='N',
                    add_descr='N',
                    opt1='o1-1',
                    opt2='o2-1',
                    opt3='o3-1')

    mbus_resp = MagicMock() 
    mbus_resp.registers = [122]

    state_table = {}
    handle_modbus_event = get_modbus_handler(mqtt, history_map, status_map, get_state_table_initializator(state_table), {})
    bus_event_payload = dict(tab_item=tb, mbus_value=ModbusValue(reg_str='41000', mbus_resp=mbus_resp))
    assert bus_event_payload['mbus_value'].resp_type == ModBusRespType.HOLDING_REG
    handle_modbus_event(bus_event_payload)

    assert len(state_table.keys()) == 1
    assert '/test' in state_table 
    assert state_table['/test'] == 122



def test_mqtt_message_producer():
    q = queue.Queue(10)
    mqtt_message_producer = get_mqtt_message_producer(q)
    
    msg = Mock()
    msg.payload = '{"type": "GtW","payload": {}}'

    mqtt_message_producer('/a/sampletopic', None, msg)
    event = q.get()

    assert event.event_payload['type'] == 'GtW'

def test_get_mqtt_handler(mocker):
    testTable = []

    sets = MagicMock()
    sets.general.gtwid='aabb'

    mqtt = MagicMock()
    mocker.patch.object(mqtt, 'publish', autospec=False)
    modbus = MagicMock()
    commands = {**get_commands_map(topics_table=testTable, sets=sets, mqtt=mqtt, mbus=modbus),
                **get_settings_command_map(sets=sets,mqtt=mqtt)}
   
    handler = get_mqtt_handler(sets.general.gtwid, mqtt, commands)

    msg = {"type": "gtwid", "payload__": {"id": "1234"} }
   
    handler(msg)
    mqtt.publish.assert_called_with('aabb/System/GatewayInfo', Regex(r'{"type": "gtwid", "payload": {\"error\".*\"details\".*}}'))

def test_get_mqtt_handler_unknow_command(mocker):
    sets = MagicMock()
    sets.general.gtwid='aabb'

    mqtt = MagicMock()
    mocker.patch.object(mqtt, 'publish', autospec=False)
    commands = {'KNOWN_CMD': lambda: True}
   
    handler = get_mqtt_handler(sets.general.gtwid, mqtt, commands)
    msg = {"type": "UNKNOWN", "payload": {} }
   
    handler(msg)
    mqtt.publish.assert_called_with('aabb/System/GatewayInfo', Regex(r'{"type": "unknown", "payload": {"error": "Missing command unknown".*\"details\".*}}'))

def test_state_refresher(mocker):
    q = queue.Queue(10)
    out_state_table = {
        '1234/DO1': '1',
        '1235/DO2': '0'
    }

    state_refresh = get_state_refresher(q, out_state_table)
    state_refresh()

    item = q.get()

    assert item.event_type == EventTypes.MQTT
    assert item.event_payload['type'] == 'regwrite'
    assert item.event_payload['payload']['topic'] == '1234/DO1'
    

def test_get_state_table_initializator(mocker):
    import random

    table = [TableItem(topic='test/topic',
                    descr='test descr',
                    cscaling_coeff=0,
                    offset=0,
                    ui='A',
                    registry_range='40000',
                    slave_id=1,
                    access='R/W',
                    send_on_variation='S',
                    variation=5,
                    variation_type='%',
                    recurring_send='S',
                    recurring_span=1,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1',
                    opt2='o2',
                    opt3='o3'),
            TableItem(topic='prova/topic',
                    descr='decription',
                    cscaling_coeff=1,
                    offset=10,
                    ui='V',
                    registry_range='41000',
                    slave_id=123,
                    access='W',
                    send_on_variation='S',
                    variation=12,
                    variation_type='%',
                    recurring_send='N',
                    recurring_span=0,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1-1',
                    opt2='o2-1',
                    opt3='o3-1')]
    out_state_table = {}
    
    add_to_state_table = get_state_table_initializator(out_state_table)
    v = 10
    for tab_item in table:
        add_to_state_table(tab_item.topic, v)
        v = v + 1

    assert 'test/topic' in out_state_table
    assert out_state_table['test/topic'] == 10
    assert 'prova/topic' in out_state_table
    assert out_state_table['prova/topic'] == 11

def test_send_value_to_mqtt(mocker):
    mqtt = MagicMock()
    mocker.patch.object(mqtt, 'publish', autospec=False)

    ti = TableItem(topic='prova/topic',
                    descr='decription',
                    cscaling_coeff=1,
                    offset=0,
                    ui='V',
                    registry_range='41000-41001',
                    slave_id=123,
                    access='W',
                    send_on_variation='S',
                    variation=12,
                    variation_type='%',
                    recurring_send='N',
                    recurring_span=0,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1-1',
                    opt2='o2-1',
                    opt3='o3-1')
    history = {}
    add_to_map(history, ti.slave_id, ti.registry_range, HistoryItem(time.time(), [208, 344]))
    perc_strategy = get_perc_value_strategy(history, '123', 10)

    mbus_value = MagicMock()
    mbus_value.resp_type = ModBusRespType.HOLDING_REG
    
    sval = make_sensor_value(ti, [208, 344], mbus_value)
    res = send_value_to_mqtt(mqtt, ti, sval, [perc_strategy])
    assert res == False

    sval = make_sensor_value(ti, [230, 344], mbus_value)
    res = send_value_to_mqtt(mqtt, ti, sval, [perc_strategy])
    assert res == True

    sval = make_sensor_value(ti, [208, 344], mbus_value)
    res = send_value_to_mqtt(mqtt, ti, sval, [perc_strategy])
    assert res == False

def test_send_value_to_mqtt_coils(mocker):
    mqtt = MagicMock()
    mocker.patch.object(mqtt, 'publish', autospec=False)

    ti = TableItem(topic='prova/topic',
                    descr='decription',
                    cscaling_coeff=1,
                    offset=0,
                    ui='V',
                    registry_range='10001-10002',
                    slave_id=123,
                    access='W',
                    send_on_variation='S',
                    variation=12,
                    variation_type='%',
                    recurring_send='N',
                    recurring_span=0,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1-1',
                    opt2='o2-1',
                    opt3='o3-1')
    history = {}
    add_to_map(history, ti.slave_id, ti.registry_range, HistoryItem(time.time(), [1]))
    value_change_strategy = get_value_change_stategy(history, '123', None)

    mbus_value = MagicMock()
    mbus_value.resp_type = ModBusRespType.COILS
    
    sval = make_sensor_value(ti, [1], mbus_value)
    res = send_value_to_mqtt(mqtt, ti, sval, [value_change_strategy], {})
    assert res == False
    

    sval = make_sensor_value(ti, [0], mbus_value)
    res = send_value_to_mqtt(mqtt, ti, sval, [value_change_strategy], {})
    assert res == True

def test_send_value_to_mqtt(mocker):
    mqtt = MagicMock()
    mocker.patch.object(mqtt, 'publish', autospec=False)

    ti = TableItem(topic='prova/topic',
                    descr='decription',
                    cscaling_coeff=1,
                    offset=0,
                    ui='V',
                    registry_range='41000-41001',
                    slave_id=123,
                    access='W',
                    send_on_variation='S',
                    variation=12,
                    variation_type='%',
                    recurring_send='N',
                    recurring_span=0,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1-1',
                    opt2='o2-1',
                    opt3='o3-1')
    history = {}
    add_to_map(history, ti.slave_id, ti.registry_range, HistoryItem(time.time(), [208, 344]))
    perc_strategy = get_perc_value_strategy(history, '123', 10)

    mbus_value = MagicMock()
    mbus_value.resp_type = ModBusRespType.HOLDING_REG
    
    sval = make_sensor_value(ti, [208, 344], mbus_value)
    res = send_value_to_mqtt(mqtt, ti, sval, [perc_strategy], {})
    assert res == False

    sval = make_sensor_value(ti, [230, 344], mbus_value)
    res = send_value_to_mqtt(mqtt, ti, sval, [perc_strategy], {})
    assert res == True

    sval = make_sensor_value(ti, [208, 344], mbus_value)
    res = send_value_to_mqtt(mqtt, ti, sval, [perc_strategy], {})
    assert res == False

def test_send_value_to_mqtt_CDR(mocker):
    mqtt = MagicMock()
    mocker.patch.object(mqtt, 'publish', autospec=False)

    ti = TableItem(topic='prova/topic',
                    descr='decription',
                    cscaling_coeff=1,
                    offset=0,
                    ui='V',
                    registry_range='41000-41001',
                    slave_id=123,
                    access='W',
                    send_on_variation='S',
                    variation=12,
                    variation_type='%',
                    recurring_send='N',
                    recurring_span=0,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1-1',
                    opt2='o2-1',
                    opt3='o3-1')
    history = {}
    cdr_table = {
        'prova/topic': '999'
    }
    perc_strategy = lambda register_string, current_value, val_idx, current_timestamp: True

    mbus_value = MagicMock()
    mbus_value.resp_type = ModBusRespType.HOLDING_REG

    sval = make_sensor_value(ti, [230, 344], mbus_value)
    res = send_value_to_mqtt(mqtt, ti, sval, [perc_strategy], cdr_table)
    assert res == True
    mqtt.publish.assert_called_with(payload=Regex(r'{\"tag_name\": \"prova/topic\", \"ui\": \"V\", \"value\": \[230\.0, 344\.0\], \"timestamp\": \".*\", \"descr\": \"decription\", \"opt1\": \"o1-1\", \"opt2\": \"o2-1\", \"opt3\": \"o3-1\", \"CDR\": \"999\"}'), topic='prova/topic')
    assert 'prova/topic' not in cdr_table

    res = send_value_to_mqtt(mqtt, ti, sval, [perc_strategy], cdr_table)
    assert res == True
    mqtt.publish.assert_called_with(payload=Regex(r'{\"tag_name\": \"prova/topic\", \"ui\": \"V\", \"value\": \[230\.0, 344\.0\], \"timestamp\": \".*\", \"descr\": \"decription\", \"opt1\": \"o1-1\", \"opt2\": \"o2-1\", \"opt3\": \"o3-1\"}'), topic='prova/topic')
    assert 'prova/topic' not in cdr_table

