from functools import reduce

class HistoryItem():
    """
    Item for the History Table to track what happened to a register for a specific slave_id
    in terms of last read and/or last value
    """
    def __init__(self, last_run_timestamp, last_value):
        self._last_run_timestamp = last_run_timestamp
        self._last_value = last_value

    @property
    def last_run_timestamp(self):
        return int(self._last_run_timestamp)

    @property
    def last_value(self):
        return self._last_value

        
def check_with(history_map, slave_id, register_string, fn):
    lookup_key = "{slave_id}_{register_string}".format(slave_id=slave_id, register_string=register_string)
    item = history_map.get(lookup_key)
    return fn(item) if item is not None else True

def add_to_map(history_map, slave_id, register_string, item):
    lookup_key = "{slave_id}_{register_string}".format(slave_id=slave_id, register_string=register_string)
    history_map[lookup_key] = item

#####
# Out of the loop: history_map, slave_id, elapsed, abs_|perc_change ||| in loop register_string, current_value
#####
def get_absolute_value_strategy(history_map, slave_id, value_change):
    """
    value_change in this contest is the absolute value
    """
    def absolute_value_strategy(register_string, current_value, val_idx, current_timestamp=None):
        """
        Strategy to check if a registry valued changed ± absolute value since last read 
        """
        fn = lambda current_value, value_change : lambda item : abs(current_value - item.last_value[val_idx]) > value_change
        return not history_map or check_with(history_map, slave_id, register_string, fn(current_value, value_change))

    return absolute_value_strategy

def get_perc_value_strategy(history_map, slave_id, value_change):
    """
    value_change in this contest is the percentage
    """
    def perc_value_strategy(register_string, current_value, val_idx, current_timestamp=None):
        """
        Strategy to check if a registry value changed ±  percentage since last read
        """
        fn = lambda current_value, value_change : lambda item : abs(current_value - item.last_value[val_idx]) > (item.last_value[val_idx] * (value_change / 100))
        return not history_map or check_with(history_map, slave_id, register_string, fn(current_value, value_change))

    return perc_value_strategy

def get_elasped_time_strategy(history_map, slave_id, value_change):
    """
    value_change in this contest is the time span
    """
    def elasped_time_strategy(register_string, current_timestamp, val_idx, current_value=None):
        """
        Strategy to check if elapsed time from last read is >= passed elapsed parameter
        """
        fn = lambda current_timestamp, elapsed_trigger : lambda item : (int(current_timestamp) - item.last_run_timestamp) >= elapsed_trigger
        return not history_map or check_with(history_map, slave_id, register_string, fn(current_timestamp, value_change))

    return elasped_time_strategy

def get_value_change_stategy(history_map, slave_id, value_change):
    """
    value_change in this context is ignored 
    """
    def value_change_strategy(register_string, current_timestamp, val_idx, current_value=None):
        """
        Simple strategy, checks current value against previous one 
        """
        fn = lambda current_value, value_change : lambda item : item.last_value[val_idx] != current_value
        return not history_map or check_with(history_map, slave_id, register_string, fn(current_value, value_change))

    return value_change_strategy

def get_registers_strategies(table_item, history_map):
    """
    get right send strategies, by looking at table_item configuration, for read that returns 'registers' (HOLDING and DISCRETE)
    """

    strategies = []
    if table_item.send_on_variation:
        if table_item.variation_type == 'A':
            strategies.append(get_absolute_value_strategy(history_map, table_item.slave_id, int(table_item.variation)))
        elif table_item.variation_type == '%':
            strategies.append(get_perc_value_strategy(history_map, table_item.slave_id, int(table_item.variation)))
    
    if table_item.recurring_send:
        strategies.append(get_elasped_time_strategy(history_map, table_item.slave_id, int(table_item.recurring_span)))

    return strategies

def get_bits_strategies(table_item, history_map):
    """
    get right send strategies, by looking at table_item configuration, for read that returns 'bits' (COILS and DIGITAL INPUT)
    """
    strategies = []
    if table_item.send_on_variation:
        strategies.append(get_value_change_stategy(history_map, table_item.slave_id, None))
   
    if table_item.recurring_send:
        strategies.append(get_elasped_time_strategy(history_map, table_item.slave_id, int(table_item.recurring_span)))

    return strategies

def evaluate(strategies, register_string, current_values, current_timestamp):
    results = []
    for i in range(0, len(current_values)):
        results.append(next(filter(lambda fn: fn(register_string=register_string, current_value=current_values[i], val_idx=i, current_timestamp=current_timestamp) == True, strategies), None) != None)
    
    return reduce(lambda a, b: a or b, results)
