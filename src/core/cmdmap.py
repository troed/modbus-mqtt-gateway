from consts import (MQTT_RESP_TOPIC,
                    TABLE_FILE_PATH)

from consts import (CMD_NAME_GTWINFO,
                    CMD_NAME_GTW,
                    CMD_NAME_GTWIDRITE,
                    CMD_NAME_GTWREBOOT,
                    CMD_NAME_GTWWRITE,
                    CMD_NAME_GTWID,
                    CMD_NAME_GTWLOG,
                    CMD_NAME_GTWLOGERASE)

from consts import (CMD_NAME_MQTT,
                    CMD_NAME_MQTTLOGERASE,
                    CMD_NAME_MQTTREBOOT,
                    CMD_NAME_MQTTLOG,
                    CMD_NAME_MQTTWRITE,
                    CMD_NAME_REGWRITE)

from consts import (CMD_NAME_CNXMDBLOG,
                    CMD_NAME_CNSMDB,
                    CMD_NAME_CNXMDBWRITE,
                    CMD_NAME_CNXMDBREBOOT,
                    CMD_NAME_CNXMDBERASE,
                    CMD_NAME_CNXMDBSTATUS)

from consts import (CMD_NAME_4G,
                    CMD_NAME_4GWRITE,
                    CMD_NAME_4GLOGERASE,
                    CMD_NAME_4GREBOOT)

from consts import (MODBUS_LOG_PATH,
                    CELLULAR_LOG_PATH,
                    GATEWAY_LOG_PATH,
                    MQTT_LOG_PATH,
                    MODBUS_LOG_PATH,
                    MODBUS_LOG_PATH,
                    GENERAL_INI_FILE_PATH,
                    MQTT_INI_FILE_PATH,
                    CELLULAR_INI_FILE_PATH,
                    SERIAL_INI_FILE_PATH)

from consts import (
    TLS_CERT_CLIENT_PATH,
    TLS_KEY_CLIENT_PATH,
    TLS_CA_PATH
)

from core.commands.gtw import GtwCommand
from core.commands.gtwInfo import GtwInfoCommand
from core.commands.gtwReboot import GtwRebootCommand
from core.commands.gtwWrite import GtwWriteCommand
from core.commands.gtwId import GtwIDCommand
from core.commands.registryWrite import RegistryWriteCommand
from core.commands.readFile import ReadFileCommand
from core.commands.iniFileWrite import IniFileWriteCommand
from core.commands.logErase import LogEraseCommand
from core.commands.cnxMdbStatus import CnxMdbStatusCommand

from core.cert_validation import rewriteCertsWithPath
from core.utils import get_file_datetime_str


def MQTTIniAddCertStatus(content):
    """Enricher passed to 'mqtt' command to enrich cknten

    Arguments:
        content {str} -- content buffer as read from ReadCommand
    """
    cert_file_lasttime = get_file_datetime_str(TLS_CERT_CLIENT_PATH) or "[NOT PRESENT]"
    key_file_lasttime = get_file_datetime_str(TLS_KEY_CLIENT_PATH) or "[NOT PRESENT]"
    ca_file_lasttime = get_file_datetime_str(TLS_CA_PATH) or "[NOT PRESENT]"
    return content + f"cert_file_lasttime = {cert_file_lasttime}\nkey_file_lasttime = {key_file_lasttime}\nca_file_lasttime = {ca_file_lasttime}\n"


def get_commands_map(topics_table, sets, mqtt, mbus):
    """
    Command factory, receive all the context objects (CSV table, settings, mqtt module and ModBus one)
    and create instances for all the commands passing needed information in the constructor
    """
    response_topic = MQTT_RESP_TOPIC.format(gatewayId=sets.general.gtwid)
    return {
        CMD_NAME_GTW: GtwCommand(mqtt=mqtt, topic=response_topic, table=topics_table),
        CMD_NAME_GTWREBOOT: GtwRebootCommand(mqtt=mqtt, topic=response_topic),
        CMD_NAME_MQTTREBOOT: GtwRebootCommand(mqtt=mqtt, topic=response_topic),
        CMD_NAME_4GREBOOT: GtwRebootCommand(mqtt=mqtt, topic=response_topic),
        CMD_NAME_CNXMDBREBOOT: GtwRebootCommand(mqtt=mqtt, topic=response_topic),
        CMD_NAME_GTWID: GtwIDCommand(mqtt=mqtt, topic=response_topic, gtwid=sets.general.gtwid),
        CMD_NAME_GTWWRITE: GtwWriteCommand(mqtt=mqtt, topic=response_topic, file_path=TABLE_FILE_PATH),
        CMD_NAME_GTWLOG: ReadFileCommand(mqtt=mqtt, topic=response_topic, file_path=GATEWAY_LOG_PATH, cmd_name=CMD_NAME_GTWLOG, resp_key='log'),
        CMD_NAME_MQTTLOG: ReadFileCommand(mqtt=mqtt, topic=response_topic, file_path=MQTT_LOG_PATH, cmd_name=CMD_NAME_MQTTLOG, resp_key='log'),
        CMD_NAME_CNXMDBLOG: ReadFileCommand(mqtt=mqtt, topic=response_topic, file_path=MODBUS_LOG_PATH, cmd_name=CMD_NAME_CNXMDBLOG, resp_key='log'),
        CMD_NAME_MQTT: ReadFileCommand(mqtt=mqtt, topic=response_topic, file_path=MQTT_INI_FILE_PATH, cmd_name=CMD_NAME_MQTT, resp_key='config', block_kwords=['cert_file', 'key_file', 'ca_file'], content_enricher=MQTTIniAddCertStatus),
        CMD_NAME_CNSMDB: ReadFileCommand(mqtt=mqtt, topic=response_topic, file_path=SERIAL_INI_FILE_PATH, cmd_name=CMD_NAME_CNSMDB, resp_key='config'),
        CMD_NAME_4G: ReadFileCommand(mqtt=mqtt, topic=response_topic,
                                     file_path=CELLULAR_INI_FILE_PATH, cmd_name=CMD_NAME_4G, resp_key='config')
    }

def get_settings_command_map(sets, mqtt):
    response_topic = MQTT_RESP_TOPIC.format(gatewayId=sets.general.gtwid)
    return{
        CMD_NAME_MQTTWRITE: IniFileWriteCommand(mqtt=mqtt, topic=response_topic, setting_provider=sets.mqtt, ini_filename=MQTT_INI_FILE_PATH, cmd_name=CMD_NAME_MQTTWRITE, sett_strategy=rewriteCertsWithPath),
        CMD_NAME_CNXMDBWRITE: IniFileWriteCommand(mqtt=mqtt, topic=response_topic, setting_provider=sets.serial, ini_filename=SERIAL_INI_FILE_PATH, cmd_name=CMD_NAME_CNXMDBWRITE),
        CMD_NAME_4GWRITE: IniFileWriteCommand(mqtt=mqtt, topic=response_topic, setting_provider=sets.cellular, ini_filename=CELLULAR_INI_FILE_PATH, cmd_name=CMD_NAME_4GWRITE),
        CMD_NAME_GTWIDRITE: IniFileWriteCommand(mqtt=mqtt, topic=response_topic, setting_provider=sets.general, ini_filename=GENERAL_INI_FILE_PATH, cmd_name=CMD_NAME_GTWIDRITE),
    }


def get_logging_command_map(sets, mqtt, gateway_logger, mqtt_logger, cellular_logger, modbus_logger):
    response_topic = MQTT_RESP_TOPIC.format(gatewayId=sets.general.gtwid)
    return{
        CMD_NAME_GTWLOGERASE: LogEraseCommand(mqtt=mqtt, topic=response_topic, logger=gateway_logger, cmd_name=CMD_NAME_GTWLOGERASE),
        CMD_NAME_MQTTLOGERASE: LogEraseCommand(mqtt=mqtt, topic=response_topic, logger=mqtt_logger, cmd_name=CMD_NAME_MQTTLOGERASE),
        CMD_NAME_4GLOGERASE: LogEraseCommand(mqtt=mqtt, topic=response_topic, logger=cellular_logger, cmd_name=CMD_NAME_4GLOGERASE),
        CMD_NAME_CNXMDBERASE: LogEraseCommand(
            mqtt=mqtt, topic=response_topic, logger=modbus_logger, cmd_name=CMD_NAME_CNXMDBERASE)
    }


def get_gtw_command_map(sets, mqtt, gtw_version):
    response_topic = MQTT_RESP_TOPIC.format(gatewayId=sets.general.gtwid)
    return{
        CMD_NAME_GTWINFO: GtwInfoCommand(
            mqtt=mqtt, topic=response_topic, version=gtw_version)
    }


def get_mdb_command_map(topics_table, slave_status_map, out_state_table, cdr_table, sets, mqtt, mbus):
    response_topic = MQTT_RESP_TOPIC.format(gatewayId=sets.general.gtwid)
    return {
        CMD_NAME_CNXMDBSTATUS: CnxMdbStatusCommand(table=topics_table, slave_status_map=slave_status_map, mqtt=mqtt, topic=response_topic),
        CMD_NAME_REGWRITE: RegistryWriteCommand(mqtt=mqtt, topic=response_topic, modbus=mbus, table=topics_table, out_state_table=out_state_table, cdr_table=cdr_table, gatewayId=sets.general.gtwid),
    }
