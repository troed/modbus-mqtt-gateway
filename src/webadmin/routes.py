
import os
import threading
import copy

from flask import Blueprint, render_template, flash, redirect, url_for, current_app
from flask import jsonify, make_response
from flask_nav.elements import Navbar, View, Subgroup, Link, Text, Separator
from flask import request
from flask_basicauth import BasicAuth

from table.csvtable import CsvTable
from io import StringIO, TextIOWrapper

import time
import datetime
from typing import NamedTuple

from .nav import nav

from consts import MQTT_RESP_TOPIC
from consts import (
    CMD_NAME_GTWLOGERASE,
    CMD_NAME_GTWWRITE,
    CMD_NAME_MQTTWRITE,
    CMD_NAME_CNXMDBWRITE,
    TABLE_FILE_PATH
)
from consts import (
    GENERAL_INI_FILE_PATH,
    SERIAL_INI_FILE_PATH,
    CELLULAR_INI_FILE_PATH,
    MQTT_INI_FILE_PATH,
    SERIAL_INI_FILE_PATH
)

from consts import (
    TLS_CERT_CLIENT_PATH,
    TLS_KEY_CLIENT_PATH,
    TLS_CA_PATH
)

from core.utils import tail, get_file_datetime_str
from core.commands.logErase import LogEraseCommand
from core.commands.iniFileWrite import IniFileWriteCommand
from core.commands.gtwReboot import GtwRebootCommand
from core.cert_validation import validate_file

from loggers import readLogFile

frontend = Blueprint('frontend', __name__)

AnonObject = lambda **kwargs: type("Object", (), kwargs)()


def get_blueprint(basic_auth):

    def process_post(setting_provider, ini_file_path, command_name, fn=lambda sett: sett):
        if request.get_json() is not None:
            response_topic = MQTT_RESP_TOPIC.format(
                gatewayId=current_app.sets.general.gtwid)
            mqttWrite = IniFileWriteCommand(mqtt=current_app.mqtt_obj, topic=response_topic,
                                            setting_provider=setting_provider, ini_filename=ini_file_path, cmd_name=command_name, sett_strategy=fn)

            payload = {
                "settings": request.get_json()
            }

            mqttWrite.execute(command_payload=payload)

            return make_response(jsonify(True), 200)
        else:
            return make_response(jsonify(False), 400)

    @frontend.route('/')
    def index():
        modbus_status = len([x for x in current_app.slave_status_map.values(
        ) if x == True]) == len(current_app.slave_status_map.values())

        mqtt_status = current_app.mqtt_obj.is_connected if current_app.mqtt_obj is not None else False
        model = AnonObject(mqtt_info=current_app.mqtt_obj.broker_info() if current_app.mqtt_obj is not None else None,
                           mqtt_connected=mqtt_status,
                           mqtt_commmands_logtable=current_app.mqtt_commmands_logtable,
                           slave_status_map=current_app.slave_status_map,
                           modbus_status=modbus_status,
                           now=datetime.datetime.now(),
                           modbus_settings=current_app.sets.serial,
                           mbus_events=current_app.mbus_events,
                           faulted=current_app.faulted)

        return render_template('index.html', model=model)

    @frontend.route('/logs', methods=['GET', 'DELETE'])
    def logs():
        op = request.args.get('op')
        if op and op == 'delete':
            logmap = {
                'gwlog': current_app.gw_logger,
                'mqttlog': current_app.mqtt_logger,
                'mbuslog': current_app.modbus_logger,
            }
            which = request.args.get('subject')
            _clearlog(current_app.mqtt_obj,
                      current_app.sets.general.gtwid, logmap[which])
            return make_response(jsonify(True), 200)
        else:
            log_data = readLogFile(current_app.gw_logger)
            return render_template('logs.html', log_buffer=log_data)

    @frontend.route('/getlog', methods=['GET'])
    def getlog():
        logmap = {
            'gwlog': current_app.gw_logger,
            'mqttlog': current_app.mqtt_logger,
            'mbuslog': current_app.modbus_logger,
        }
        which = request.args.get('subject')

        log_data = readLogFile(logmap[which])
        return log_data

    def _clearlog(mqtt_obj, gatewayId, logger):
        response_topic = MQTT_RESP_TOPIC.format(gatewayId=gatewayId)
        cmd = LogEraseCommand(mqtt=mqtt_obj, topic=response_topic,
                              logger=logger, cmd_name=CMD_NAME_GTWLOGERASE)

        cmd.execute()

    @frontend.route('/commands', methods=['GET', 'POST'])
    @basic_auth.required
    def commands():
        return render_template('commands.html')

    def _reboot_fun(mqtt_obj, response_topic):
        cmd = GtwRebootCommand(mqtt=mqtt_obj, topic=response_topic)
        cmd.execute()

    @frontend.route('/reboot', methods=['POST'])
    # @basic_auth.required
    def reboot():
        response_topic = MQTT_RESP_TOPIC.format(
            gatewayId=current_app.sets.general.gtwid)
        thread = threading.Thread(target=_reboot_fun, args=(
            current_app.mqtt_obj, response_topic))
        thread.start()

        # cmd = GtwRebootCommand(mqtt=current_app.mqtt_obj, topic=response_topic)
        # cmd.execute()
        return make_response(jsonify(True), 200)

    @frontend.route('/general', methods=['GET', 'POST'])
    @basic_auth.required
    def general():
        if request.method == 'GET':
            return render_template('general.html',
                                   general=current_app.sets.general
                                   )
        elif request.method == 'POST':
            json_post = {}
            [json_post.update(item) for item in request.get_json()]
            current_app.config['BASIC_AUTH_USERNAME'] = json_post['adminusr']
            current_app.config['BASIC_AUTH_PASSWORD'] = json_post['adminpwd']
            return process_post(current_app.sets.general, GENERAL_INI_FILE_PATH, CMD_NAME_GTWWRITE)

    @frontend.route('/mqtt', methods=['GET', 'POST'])
    @basic_auth.required
    def mqtt():
        if request.method == 'GET':
            # Certs data should be hidden in respons
            mqtt_copy = copy.copy(current_app.sets.mqtt)

            mqtt_copy.cert_file = "*****"
            mqtt_copy.cert_file_lasttime = get_file_datetime_str(
                TLS_CERT_CLIENT_PATH)

            mqtt_copy.key_file = "*****"
            mqtt_copy.key_file_lasttime = get_file_datetime_str(
                TLS_KEY_CLIENT_PATH)

            mqtt_copy.ca_file = "*****"
            mqtt_copy.ca_file_lasttime = get_file_datetime_str(
                TLS_KEY_CLIENT_PATH)

            return render_template('mqtt.html', mqtt=mqtt_copy)
        elif request.method == 'POST':
            return process_post(current_app.sets.mqtt, MQTT_INI_FILE_PATH, CMD_NAME_MQTTWRITE)

    @frontend.route('/serial', methods=['GET', 'POST'])
    @basic_auth.required
    def serial():
        if request.method == 'GET':
            return render_template('serial.html', serial=current_app.sets.serial)
        elif request.method == 'POST':

            return process_post(current_app.sets.serial, SERIAL_INI_FILE_PATH, CMD_NAME_CNXMDBWRITE)

    @frontend.route('/table', methods=['GET', 'POST'])
    @basic_auth.required
    def table():
        if request.method == 'GET':
            with open(TABLE_FILE_PATH, 'r') as csvfile:
                return render_template('table.html', table_buffer=csvfile.read())
        elif request.method == 'POST':
            tablebuffer = request.get_json()['tablebuffer']
            f = StringIO(tablebuffer)
            c = CsvTable(f)
            (errors, table) = c.tableListValidated()
            if len(errors) > 0:
                return make_response('<br />'.join([err[0] for err in errors ]), 400)
            else:
                with open(TABLE_FILE_PATH, 'w') as csvfile:
                    csvfile.write(tablebuffer)

            return make_response(jsonify(True), 200)

    @frontend.route('/cert_upload/<file_type>', methods=['POST'])
    def cert_upload(file_type):
        file_name = None
        if request.method == 'POST':
            file_val = request.files[file_type]
            file_content = TextIOWrapper(file_val)

            cert_settings = {
                'cert': {'validations': ('-----BEGIN CERTIFICATE-----', '-----END CERTIFICATE-----'), 'filename': TLS_CERT_CLIENT_PATH},
                'ca':  {'validations': ('-----BEGIN CERTIFICATE-----', '-----END CERTIFICATE-----'), 'filename': TLS_CA_PATH},
                'key':  {'validations': ('-----BEGIN RSA PRIVATE KEY-----', '-----END RSA PRIVATE KEY-----'), 'filename': TLS_KEY_CLIENT_PATH}
            }

            if not validate_file(file_content.read(), *cert_settings[file_type]['validations']):
                return make_response("Wrong file format", 400)

            file_val.seek(0)
            file_val.save(cert_settings[file_type]['filename'])

            if file_type == 'cert':
                print(f"Setting cert_file to {file_name}")
                current_app.sets.mqtt.cert_file = cert_settings[file_type]['filename']
            elif file_type == 'key':
                print(f"Setting key_file to {file_name}")
                current_app.sets.mqtt.key_file = cert_settings[file_type]['filename']
            elif file_type == 'ca':
                print(f"Setting ca_file to {file_name}")
                current_app.sets.mqtt.ca_file = cert_settings[file_type]['filename']

            process_post(current_app.sets.mqtt,
                         MQTT_INI_FILE_PATH, CMD_NAME_MQTTWRITE)

        return make_response(jsonify(file_name), 200)

    @frontend.route('/doc')
    def doc():
        return render_template('doc.html'), 200

    @frontend.route('/logout')
    def Logout():
        return render_template('logout.html'), 401

    return frontend
