from flask import Flask
from flask import request

from .nav import nav

def create_app(configfile=None):
  app = Flask(__name__)

  nav.init_app(app)

  @app.context_processor
  def inject_stage_and_region():
    auth = request.authorization
    username = None
    if auth is not None:
      username = auth.username
    return dict(user=username)

  return app