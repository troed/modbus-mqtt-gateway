

function handleAjaxForm(form, cb) {

  var _base_serializeArray = $.fn.serializeArray;
  $.fn.serializeArray = function () {
    var a = _base_serializeArray.apply(this);
    $.each(this.find("input"), function (i, e) {
      if (e.type == "checkbox") {
        e.checked
          ? a[i].value = "S"
          : a.splice(i, 0, { name: e.name, value: "N" })
      }
    });
    return a;
  };

  form.submit(function (event) {
    event.preventDefault();

    if (form.validationEngine('validate')) {
      var post_url = $(this).attr("action");
      var request_method = $(this).attr("method");
      var form_data = $(this).serializeArray();

      form_obj = form_data.map(function (item) {
        return {
          [item.name]: item.value
        }
      });
      $.ajax({
        url: post_url,
        type: request_method,
        data: JSON.stringify(form_obj),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
      }).done(function (response) { //
        cb(null, response);
      });
    }

    return false;

  });

}