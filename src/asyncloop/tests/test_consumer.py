import queue
import time

from asyncloop.consumer import ConsumerThread

def loop_strategy_factory():
    loc_cnt = 100
    def loop_strategy():
        nonlocal loc_cnt
        loc_cnt = loc_cnt - 1
        return loc_cnt > 0
    return loop_strategy


def test_consumer():
    q = queue.Queue(10)
    q.put("ITEM")
    q.put("ITEM")
    q.put("ITEM")

    calls = 0

    def consumer_fn(item):
        nonlocal calls
        calls = calls + 1

    c = ConsumerThread(name="consumer_test", q=q, loop_fn=consumer_fn, loop_strategy=loop_strategy_factory())
    c.start()
    c.join()

    assert calls == 3

