import queue
import time

from asyncloop.producer import ProducerThread

def loop_strategy_factory():
    loc_cnt = 100
    def loop_strategy():
        nonlocal loc_cnt
        loc_cnt = loc_cnt - 1
        return loc_cnt > 0
    return loop_strategy

def test_cproducer():
    q = queue.Queue(10)
    def producer_fn(queue):
        q.put("item")

    p = ProducerThread(name='test_producer', q=q, loop_sleep=0.2, loop_fn=producer_fn, loop_strategy=loop_strategy_factory())
    p.start()
    p.join()

    assert q.get() is not None 