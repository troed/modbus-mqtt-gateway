
import time

from asyncloop.simplethread import SimpleThread

def test_simplethread():

    cnt = 0
    def fn():
        nonlocal cnt
        cnt = cnt + 1

    t = SimpleThread(name='test_simple_thead', loop_fn=fn, loop_sleep=0.1, loop_strategy=lambda : cnt < 5)
    t.start()
    t.join()

    assert cnt == 5