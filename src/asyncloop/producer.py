import threading
import time

class ProducerThread(threading.Thread):
    def __init__(self, name=None, q=None, loop_sleep=0.5, loop_fn=None, loop_strategy=lambda : True):
        super(ProducerThread,self).__init__()
        self._q = q
        self.name = name
        self.loop_sleep = loop_sleep
        self.loop_fn = loop_fn
        self._loop_strategy = loop_strategy

    def run(self):
        while self._loop_strategy():
            if not self._q.full():
                self._q.put(self.loop_fn(self._q))
                time.sleep(self.loop_sleep)
        return
   