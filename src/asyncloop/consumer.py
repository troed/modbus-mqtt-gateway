import threading
import time

class ConsumerThread(threading.Thread):
    def __init__(self, name=None, q=None, loop_fn=None, loop_sleep=0.01, loop_strategy=lambda : True):
        super(ConsumerThread,self).__init__()
        self.name = name
        self._q = q
        self._loop_fn = loop_fn
        self._loop_strategy = loop_strategy
        self._loop_sleep = loop_sleep

        return

    def run(self):
        while self._loop_strategy():
            if not self._q.empty():
                item = self._q.get()
                if item is not None:
                    self._loop_fn(item)
                    self._q.task_done()
            if(self._loop_sleep > 0.0):
                time.sleep(self._loop_sleep)

        return