import threading
import time

class SimpleThread(threading.Thread):
    def __init__(self, name=None, loop_fn=None, loop_sleep=0.5, delayed_start=0, logger=None, loop_strategy=lambda : True):
        super(SimpleThread,self).__init__()
        self.name = name
        self._loop_fn = loop_fn
        self._loop_strategy = loop_strategy
        self.loop_sleep = loop_sleep
        self._delayed_start = delayed_start
        self._logger = logger

        return

    def run(self):
        if self._logger is not None:
            self._logger.info(f"Starting {self._name} with delay {self._delayed_start} secs")
        time.sleep(self._delayed_start)
        if self._logger is not None:
            self._logger.info(f"{self._name} starting")
        while self._loop_strategy():
            self._loop_fn()
            time.sleep(self.loop_sleep)

        return