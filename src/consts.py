import os

app_dir = os.path.dirname(os.path.realpath(__file__))
# Base path for application data (CSV file ...)
DATA_FILES_PATH=os.path.join(app_dir, '../data')
# File path for registries <-> topics mapping
TABLE_FILE_PATH=os.path.join(DATA_FILES_PATH,'regs-topics.csv')
# Configuration files paths
GENERAL_INI_FILE_PATH=os.path.join(DATA_FILES_PATH,'general.ini') 
SERIAL_INI_FILE_PATH=os.path.join(DATA_FILES_PATH,'serial.ini')
CELLULAR_INI_FILE_PATH=os.path.join(DATA_FILES_PATH,'cellular.ini')
MQTT_INI_FILE_PATH=os.path.join(DATA_FILES_PATH,'mqtt.ini')

logs_base_dir = os.path.join(app_dir, '../logs/')
GATEWAY_LOG_PATH=os.path.join(app_dir, '../logs/gateway.log')
MQTT_LOG_PATH=os.path.join(app_dir, '../logs/mqtt.log')
MODBUS_LOG_PATH=os.path.join(app_dir, '../logs/modbus.log')
CELLULAR_LOG_PATH=os.path.join(app_dir, '../logs/cellular.log')

QUEUE_MAX_BUF_SIZE=1000
# Delay for thread polling modbus
MODBUS_POLL_DELAY=1 
# Delay for refreshing output register state
MODBUS_REGS_REFRESH_DELAY=5

# System topic for receiving commands
MQTT_CMD_TOPIC ='{gatewayId}/System/GatewayCmd'
# System topic for sending command responses
MQTT_RESP_TOPIC ='{gatewayId}/System/GatewayInfo'


# System commands constants
CMD_NAME_GTW ='gtw'
CMD_NAME_GTWREBOOT = 'gtwreboot'
CMD_NAME_GTWWRITE = 'gtwwrite'
CMD_NAME_GTWLOG = 'gtwlog'
CMD_NAME_GTWINFO = 'gtwinfo'
CMD_NAME_MQTTLOG = 'mqttlog'
CMD_NAME_GTWLOGERASE = 'gtwlogerase'
CMD_NAME_MQTT = 'mqtt'
CMD_NAME_MQTTWRITE = 'mqttwrite'
CMD_NAME_MQTTLOGERASE = 'mqttlogerase'
CMD_NAME_MQTTREBOOT='mqttreboot'
CMD_NAME_GTWID='gtwid'
CMD_NAME_GTWIDRITE='gtwidwrite'

CMD_NAME_CNSMDB='cnxmdb'
CMD_NAME_CNXMDBSTATUS='cnxmdbstatus'
CMD_NAME_CNXMDBWRITE='cnxmdbwrite'
CMD_NAME_CNXMDBREBOOT='cnxmdbreboot'

CMD_NAME_CNXMDBLOG='cnxmdblog'
CMD_NAME_CNXMDBERASE='cnxmdberase'

CMD_NAME_4G='4g'
CMD_NAME_4GREBOOT='4greboot'
CMD_NAME_4GWRITE='4gwrite'
CMD_NAME_4GLOG='4glog'
CMD_NAME_4GLOGERASE='4glogerase' 
# Regisrty write command
CMD_NAME_REGWRITE='regwrite'

# Certificates paths
TLS_CERT_CLIENT_PATH=os.path.join(DATA_FILES_PATH,'client.crt')
TLS_KEY_CLIENT_PATH=os.path.join(DATA_FILES_PATH,'client.key')
TLS_CA_PATH=os.path.join(DATA_FILES_PATH,'ca.crt')