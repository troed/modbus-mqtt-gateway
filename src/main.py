# coding=utf-8

import json
import re
import os
import threading
import time
import datetime
import random
import queue
import collections
import click

from enum import Enum, auto

from bootstrap.boot_config import setup_config
from bootstrap.boot_mqtt import setup_mqtt, create_mqtt
from bootstrap.boot_modbus import setup_modbus
from bootstrap.boot_regs_table import setup_regs_table
from bootstrap.boot_webadmin import setup_webadmin

from asyncloop.producer import ProducerThread
from asyncloop.consumer import ConsumerThread
from asyncloop.simplethread import SimpleThread

from table.register_topic_handler import readRegisterTopicTable

from loggers import logger, gateway_logger, mqtt_logger, modbus_logger, cellular_logger

from consts import (DATA_FILES_PATH, 
                    TABLE_FILE_PATH,
                    QUEUE_MAX_BUF_SIZE, 
                    MODBUS_POLL_DELAY,
                    MODBUS_REGS_REFRESH_DELAY, 
                    MQTT_CMD_TOPIC, 
                    MQTT_RESP_TOPIC)

from core.models import EventTypes
from core.functions import (get_modbus_producer, 
                            get_modbus_handler, 
                            get_mqtt_message_producer,
                            get_mqtt_handler, 
                            interpolate_topic,
                            get_state_refresher,
                            get_state_table_initializator,
                            normalize_modbus_value)

from core.cmdmap import (get_commands_map, 
                        get_settings_command_map, 
                        get_logging_command_map, 
                        get_gtw_command_map, 
                        get_mdb_command_map)

from modbus.responseTypes import ModBusRespType, modbustype_from_register

__author__ = "Fabrizio Guglielmino"
__copyright__ = "Copyright 2019, Fabrizio Guglielmino"
__license__ = "COMMERCIAL"
__version__ = "0.1.14"
__maintainer__ = "Fabrizio Guglielmino"
__email__ = "guglielmino@gmail.com"
__status__ = "Development"

# Global queue for producer/consumer
q = queue.Queue(QUEUE_MAX_BUF_SIZE)

MAX_TRIES = 3

@click.command()
@click.option('--enable-modbus', default=True, help='Enable ModBus subsystem')
@click.option('--enable-webadmin', default=True, help='Enable WebAdmin interface')
@click.option('--web-port', default=5000, help='Web admin listening port')
@click.option('--debug', default=False, help='Enable debug')
@click.option('--mbus-refresh-delay', default=0, help=f'ModBus register refresh delayed start (secs)')
def main(enable_modbus, enable_webadmin, web_port, debug, mbus_refresh_delay):
    starting_message = 'Gateway version {0} starting up ...'.format(__version__)
    starting_message += f'\nMODBUS SubSystem: {enable_modbus}'
    logger.info(starting_message)
    gateway_logger.info(starting_message)

    faulted = { 'MQTT': False, 'MODBUS': True }

    sets = setup_config()
    mbus = setup_modbus(sets.serial, modbus_logger)

    mbus_poll_sec = sets.serial.polling_time

    username = sets.mqtt.username if sets.mqtt.user_auth.lower() == 's' else None
    password = sets.mqtt.password if sets.mqtt.user_auth.lower() == 's' else None
    cert_file = sets.mqtt.cert_file if sets.mqtt.cert_auth.lower() == 's' else None
    key_file = sets.mqtt.key_file if sets.mqtt.cert_auth.lower() == 's' else None
    ca_cert_file =  sets.mqtt.ca_file if sets.mqtt.cert_auth.lower() == 's' else None
    mqtt = create_mqtt(sets.mqtt.qos, username, password, ca_cert_file,
                cert_file, key_file, sets.general.gtwid, mqtt_logger)

    brokers = [sets.mqtt.broker_pri,  sets.mqtt.broker_sec]
    tries = 0

    def try_connect_mqtt():
        nonlocal mqtt
        nonlocal tries
        mqtt_logger.info("MQTT: connecting mqtt ...")

        fail = True
        while fail == True and tries < MAX_TRIES:
            logger.info(f"Trying to connect {brokers[tries % len(brokers)]} try {tries}")
            mqtt_logger.info(f"Trying to connect {brokers[tries % len(brokers)]} try {tries}")

            try:
                broker = brokers[tries % len(brokers)]
                broker_comps = broker.split(':')
                mqtt.connect(broker_comps[0], port=int(broker_comps[1]) if len(broker_comps) == 2 else 1883)
                fail = False
            except Exception as ex:
                fail = True
                mqtt_logger.error(str(ex))
            tries = tries + 1
        
        if tries == MAX_TRIES and  mqtt.is_connected == False:
            gateway_logger.error('Impossible to connect to a MQTT broker')

        mqtt.set_connect_cb(lambda userdata: print("MQTT: Connect"))
        mqtt.set_disconnect_cb(lambda userdata, rc: [print(f"MQTT: Disconnect ({rc})"), try_connect_mqtt()])
        mqtt.set_subscriber_callback(get_mqtt_message_producer(q))
        mqtt.subscribe(MQTT_CMD_TOPIC.format(gatewayId=sets.general.gtwid))
        mqtt.start()

                
    setup_mqtt(sets.general.gtwid, sets.mqtt, mqtt_logger)
    try_connect_mqtt()
    
    topics_table = setup_regs_table(sets, mqtt, mqtt_logger)

    # Context tables
    history_map = {} # Keep the history of ModBus events to manage publish to MQTT logic (time or value change based)
    slave_status_map = {} # Keeps the slave status based on the last read (true/flase for seen/not seen)
    out_state_table = {} # Keeps ModBus written values in registries 
    cdr_table = {} # Keeps CDR associatins
    
    mqtt_commmands_logtable = collections.deque(maxlen=10)  # Commands for MQTT to be used in Web dashboard
    mbus_events = collections.deque(maxlen=10) # Events from ModBus to be used in Web Dashboard
   
    # Setup wrapper for intercept publish messages and add them to the mqtt_commmands_logtable
    publish = mqtt.publish
    def pub_wrapper(topic, payload):
        mqtt_commmands_logtable.append({'payload': {'topic': topic, 'value': json.loads(payload)}, 'timestamp': datetime.datetime.now()})
        publish(topic, payload)

    mqtt.publish = pub_wrapper


    # Setup handlers for events produced by various sources (MQTT and ModBus)
    handle_modbus_event = get_modbus_handler(mqtt, history_map, slave_status_map, get_state_table_initializator(out_state_table, gateway_logger), cdr_table)

    commands = {
        **get_commands_map(topics_table=topics_table, sets=sets, mqtt=mqtt, mbus=mbus),
        **get_settings_command_map(sets=sets, mqtt=mqtt),
        **get_logging_command_map(sets=sets, mqtt=mqtt, gateway_logger=gateway_logger, mqtt_logger=mqtt_logger, cellular_logger=cellular_logger, modbus_logger=modbus_logger),
        **get_mdb_command_map(topics_table=topics_table, slave_status_map=slave_status_map, out_state_table=out_state_table, cdr_table=cdr_table, sets=sets, mqtt=mqtt, mbus=mbus),
        **get_gtw_command_map(sets=sets, mqtt=mqtt, gtw_version=__version__)
    }

    handle_mqtt_event = get_mqtt_handler(commands=commands, mqtt=mqtt, gatewayId=sets.general.gtwid)

    def handle_error(error_payload):
        if error_payload['system'] == EventTypes.MQTT:
            faulted['MQTT'] = True
        elif error_payload['system'] ==  EventTypes.MODBUS:
            faulted['MODBUS'] = True
        

    events_handlers = { 
        EventTypes.MODBUS: handle_modbus_event, 
        EventTypes.MQTT : handle_mqtt_event, 
        EventTypes.ERROR: handle_error
    }

    def main_consumer(item):
        """
        Process and dispatch events coming from various inputs (ModBus, Mqtt, ...)
        """

        from pymodbus.pdu import ExceptionResponse
        if item.event_type == EventTypes.MODBUS:
            mbus_value = item.event_payload['mbus_value']
            tab_item = item.event_payload['tab_item']
            if not isinstance(mbus_value.mbus_resp, Exception) and not isinstance(mbus_value.mbus_resp, ExceptionResponse):
                faulted['MODBUS'] = False
                (sens_val, read_value, _) = normalize_modbus_value(mbus_value, tab_item)
                mbus_events.append({
                    'timestamp': datetime.datetime.now(),
                    'tab_item': tab_item, 
                    'reg_str': mbus_value.reg_str,
                    'value': read_value })

        if item.event_type == EventTypes.MQTT:
            faulted['MQTT'] = False
            gateway_logger.info('Executing command {command_type}'.format(command_type=item.event_payload))
            payload = item.event_payload['payload']
            if not 'idsender' in payload or ('idsender' in payload and payload['idsender'] != 'internal'):
                mqtt_commmands_logtable.append({**item.event_payload, "timestamp": datetime.datetime.now()})

        
        events_handlers[item.event_type](item.event_payload)              

    gateway_logger.info("Staring main threads. ModBus polling every {0} secs, refesh registers every {1}".format(mbus_poll_sec, mbus_poll_sec))
    # Poll Modbus, according to configuration in CSV table, every {mbus_poll_sec} seconds
    
    modbus_poll = ProducerThread(name='modbus_poll', q=q, loop_sleep=mbus_poll_sec, loop_fn=get_modbus_producer(topics_table, mbus))
    main_consumer = ConsumerThread(name='main_consumer', q=q, loop_fn=main_consumer)
    out_state_refresh = SimpleThread(name='out_state_refresh', loop_fn=get_state_refresher(q, out_state_table), delayed_start=mbus_refresh_delay, loop_sleep=mbus_poll_sec, logger=logger)

    if enable_modbus == True:
        modbus_poll.start()
    main_consumer.start()
    out_state_refresh.start()

    if enable_webadmin == True:
        setup_webadmin(ver=__version__, web_port=web_port, debug=debug,
            sets=sets, mqtt=mqtt, slave_status_map=slave_status_map, 
            mqtt_commmands_logtable=mqtt_commmands_logtable, 
            mbus_events=mbus_events, logger=gateway_logger, 
            mqtt_logger=mqtt_logger, modbus_logger=modbus_logger, 
            topics_table=topics_table, faulted=faulted)

if __name__ == '__main__':
    main()
