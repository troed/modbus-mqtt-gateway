
import os
import logging
from consts import logs_base_dir, GATEWAY_LOG_PATH, MQTT_LOG_PATH, MODBUS_LOG_PATH, CELLULAR_LOG_PATH
from logging.config import fileConfig

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True
}

if not os.path.isdir(logs_base_dir):
    os.mkdir(logs_base_dir)

logging.config.dictConfig(LOGGING)

verbose_formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
simple_formatter = logging.Formatter('%(levelname)s %(message)s')

logger = logging.getLogger('root')
handler = logging.StreamHandler()
handler.setFormatter(simple_formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)


mqtt_logger = logging.getLogger('app.mqtt')
mqtt_handler = logging.handlers.RotatingFileHandler(filename=MQTT_LOG_PATH, backupCount=5, maxBytes=3000000)
mqtt_handler.setFormatter(verbose_formatter)
mqtt_logger.addHandler(mqtt_handler)
mqtt_logger.setLevel(logging.DEBUG)


modbus_logger = logging.getLogger('app.modbus')
modbus_handler = logging.handlers.RotatingFileHandler(filename=MODBUS_LOG_PATH, backupCount=5, maxBytes=3000000)
modbus_handler.setFormatter(verbose_formatter)
modbus_logger.addHandler(modbus_handler)
modbus_logger.setLevel(logging.DEBUG)

cellular_logger = logging.getLogger('app.cellular')
cellular_handler = logging.handlers.RotatingFileHandler(filename=CELLULAR_LOG_PATH, backupCount=5, maxBytes=3000000)
cellular_handler.setFormatter(verbose_formatter)
cellular_logger.addHandler(cellular_handler)
cellular_logger.setLevel(logging.DEBUG)

gateway_logger = logging.getLogger('app.gateway')
gateway_handler = logging.handlers.RotatingFileHandler(filename=GATEWAY_LOG_PATH, backupCount=5, maxBytes=3000000)
gateway_handler.setFormatter(verbose_formatter)
# Adding previous handler to have a single logger for everything
gateway_logger.addHandler(handler)
gateway_logger.addHandler(gateway_handler)
gateway_logger.setLevel(logging.DEBUG)


def handlerOperation(handler, funcname):
   if hasattr(handler, funcname):
      getattr(handler, funcname)()
      return True
   return False

def rotateLog(log):
   """Force log rotation, for logger using RotatingFileHandler

   Parameters
   ----------
   log : obj
         Logger object

   Returns
   -------
   bool
         Operation result
   """
   for handler in log.handlers:
      handlerOperation(handler, 'doRollover')

def clearLog(log):
   """
   clear the file attached to the logger passed as parameter, works for logger using FileHandler or realted classes

   Parameters
   ----------
   log : obj
         Logger object
   
   """
   for handler in log.handlers:
      if hasattr(handler, 'baseFilename'):
         with open(handler.baseFilename, 'w'):
            pass

def readLogFile(log):
   logs = ""
   for handler in log.handlers:
      if hasattr(handler, 'baseFilename'):  
         with open(handler.baseFilename, 'r') as logfilehandler:
            logs = ''.join(logfilehandler.readlines())

   return logs.strip()
