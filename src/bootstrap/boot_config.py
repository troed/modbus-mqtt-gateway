from settings import SettingsFacade
from consts import (GENERAL_INI_FILE_PATH,
                    SERIAL_INI_FILE_PATH,
                    CELLULAR_INI_FILE_PATH,
                    MQTT_INI_FILE_PATH)

def setup_config():
    """
    Initialize configurations reading INI files from default path
    """
  
    sets = SettingsFacade(general_ini_file=GENERAL_INI_FILE_PATH, 
                            serial_ini_file=SERIAL_INI_FILE_PATH, 
                            cellular_ini_file=CELLULAR_INI_FILE_PATH, 
                            mqtt_ini_file=MQTT_INI_FILE_PATH)
                    
    sets.read_settings()
    return sets
