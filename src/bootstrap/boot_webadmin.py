import os
import datetime
from flask import Blueprint, render_template, flash, redirect, url_for
from flask_basicauth import BasicAuth
from webadmin.routes import get_blueprint
from webadmin import create_app
import werkzeug
from werkzeug.exceptions import HTTPException
from gevent.pywsgi import WSGIServer


def setup_webadmin(ver, web_port, debug, sets, mqtt, slave_status_map, mqtt_commmands_logtable, mbus_events, 
                    logger, mqtt_logger, modbus_logger, topics_table, faulted):
    app = create_app()
 
    # Setting custom variables in App Context
    app.mqtt_obj = mqtt
    app.sets = sets

    app.gw_logger = logger
    app.mqtt_logger = mqtt_logger
    app.modbus_logger = modbus_logger

    app.slave_status_map = slave_status_map
    app.mqtt_commmands_logtable = mqtt_commmands_logtable
    app.mbus_events = mbus_events
    app.topics_table = topics_table
    app.faulted = faulted
    
    app.config['BASIC_AUTH_USERNAME'] = sets.general.adminusr
    app.config['BASIC_AUTH_PASSWORD'] = sets.general.adminpwd
    basic_auth = BasicAuth(app)

    @app.context_processor
    def inject_stage_and_region():
        return dict(version=ver, gateway_name=sets.general.name, gateway_id=sets.general.gtwid, sysdate=datetime.datetime.now().strftime("%d/%m/%Y, %H:%M:%S"))

    @app.errorhandler(werkzeug.exceptions.Unauthorized)
    def handle_unauthorized(e):
        return 'UNAUTHORIZED', 401


    app.register_error_handler(401, handle_unauthorized)

    bp = get_blueprint(basic_auth)
    app.register_blueprint(bp)

  
    threaded=False
    print(f"Running web admin on port {web_port} (debug {debug})")
    if debug:
        app.run(debug=debug, threaded=threaded, host="0.0.0.0", port=web_port)
    else:
        http_server = WSGIServer(('', 5000), app)
        http_server.serve_forever()
        #app.run(threaded=threaded, host="0.0.0.0", port=web_port)
