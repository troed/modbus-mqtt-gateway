from mqtt.broker import MQTTBroker
from consts import MQTT_CMD_TOPIC


def setup_mqtt(gatewayId, mqtt_setts, mqtt_logger):
    mqtt = None
    if mqtt is not None and mqtt.is_connected:
        mqtt.disconnect()
        mqtt.stop()


def create_mqtt(qos, username, password, ca_cert_file, cert_file, key_file, gatewayId, mqtt_logger):
    return MQTTBroker(qos=int(qos), username=username, password=password, ca_cert_file=ca_cert_file, cert_file=cert_file, key_file=key_file, clientid=gatewayId, logger=mqtt_logger)
