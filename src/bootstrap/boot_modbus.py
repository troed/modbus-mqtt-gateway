from modbus.modbusinterface import ModbusRTU

def setup_modbus(serial_setts, modbus_logger):
    mbus = ModbusRTU(port=serial_setts.port, 
                     speed=int(serial_setts.speed),
                     stopbits=int(serial_setts.stopbit),
                     bitlen=int(serial_setts.bitlen),
                     parity=serial_setts.parity,
                     logger=modbus_logger)

    return mbus
