from consts import TABLE_FILE_PATH
from core.functions import interpolate_topic
from table.register_topic_handler import readRegisterTopicTable

def _table_mqtt_subscriptions(gtwid, table, mqtt, mqtt_logger):
    """
    Create a MQTT subscription, following the topic name convention defined in the specification manual, for each
    item having "W(rite)"
    """
    to_be_subscribed = [i for i in table if 'w' in i.access.lower()]
    try:
        for sub_item in to_be_subscribed:
            sub_topic = "{topic_name}_W".format(topic_name=sub_item.topic)
            mqtt.subscribe(sub_topic)
            mqtt_logger.info("subscribing '{0}'".format(sub_topic))
    except Exception as ex:
        mqtt_logger.error(ex)

def _interpolate_topics(topics_table, gatewayId):
    for tab_item in topics_table:
        tab_item.topic = interpolate_topic(tab_item.topic, gatewayId=gatewayId)

def setup_regs_table(sets, mqtt, mqtt_logger):
    topics_table = readRegisterTopicTable(TABLE_FILE_PATH)
    _interpolate_topics(topics_table, sets.general.gtwid)
    _table_mqtt_subscriptions(sets.general.gtwid, topics_table, mqtt, mqtt_logger)
    return topics_table