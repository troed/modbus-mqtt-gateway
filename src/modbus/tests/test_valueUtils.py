import pytest
from modbus.valueUtils import unscale_modbus_value, scale_modbus_value

def test_handle_unscaling_and_offset():
    converted1 = unscale_modbus_value(10, 1, 9)
    assert converted1 == 1

    converted2 = unscale_modbus_value(123, 1.5, 3)
    assert converted2 == 80

    converted3 = unscale_modbus_value(123, 0, 1)
    assert converted3 == 0

    converted4 = unscale_modbus_value(123, '1.5', '1')
    assert converted4 == 81

    converted5 = unscale_modbus_value(2000, 0.01, 1)
    assert converted5 == 199900

    with pytest.raises(ValueError):
        unscale_modbus_value(123, 'a', '1')
    
def test_handle_scaling_and_offset():
    converted0 = scale_modbus_value(80, 1.5, 3)
    assert converted0 == 123

    converted1 = scale_modbus_value(10, 1, 10)
    assert converted1 == 20

    converted2 = scale_modbus_value(123, 1.3, 1)
    assert converted2 == 160.9

    converted3 = scale_modbus_value(123, 0, 1)
    assert converted3 == 1

    converted4 = scale_modbus_value(123, '1.3', '1')
    assert converted4 == 160.9
    

    with pytest.raises(ValueError):
        scale_modbus_value(123, 'bb', '1')