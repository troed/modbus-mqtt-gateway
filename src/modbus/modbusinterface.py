import serial
from pymodbus.pdu import ModbusRequest
from pymodbus.client.sync import ModbusSerialClient as ModbusClient #initialize a serial RTU client instance
from pymodbus.transaction import ModbusRtuFramer
from functional.maybe import wrap_call

class ModbusRTU(object):
    """
    Modbus interface
    """
    def __init__(self, port, stopbits = 1, bitlen = 8, parity = 'N', speed = 19200, logger=None):
        self._client = ModbusClient(method = "rtu", port=port, stopbits = stopbits, bytesize = bitlen, parity = parity, baudrate = speed)
        if logger is not None:
            self._logger = logger

    def read_holding_registers(self, address, count, unit):
        wrap_call(self._logger, 'info')("reading holding reg from address {address} on slave_id {slave_id}".format(address=address, slave_id=unit))
        return self._client.read_holding_registers(address=address, count=count, unit=unit)

    def write_registers(self, address, values, unit):
        wrap_call(self._logger, 'info')("writing regiters from {address} on slave_id {slave_id} values {values}".format(address=address, slave_id=unit))
        return self._client.write_registers(address=address, values=values, unit=unit)

    def read_coils(self, address, count, unit):
        wrap_call(self._logger, 'info')("reading coil from address {address} on slave_id {slave_id}".format(address=address, slave_id=unit))
        return self._client.read_coils(address=address, count=count, unit=unit)

    def write_coil(self, address, value, unit):
        wrap_call(self._logger, 'info')("writing coil address {address} on slave_id {slave_id} values {value}".format(address=address, slave_id=unit, value=value))
        return self._client.write_coil(address=address, value=value, unit=unit)

    def read_discrete_inputs(self, address, count, unit):
        wrap_call(self._logger, 'info')("reading discrete input from address {address} on slave_id {slave_id}".format(address=address, slave_id=unit))
        return self._client.read_discrete_inputs(address=address, count=count, unit=unit)

    def read_input_registers(self, address, count, unit):
        wrap_call(self._logger, 'info')("reading input register from address {address} on slave_id {slave_id}".format(address=address, slave_id=unit))
        return self._client.read_input_registers(address=address, count=count, unit=unit)

    def write_registers(self, address, values, unit):
        wrap_call(self._logger, 'info')("writing {values} to address {address} on slave_id {slave_id}".format(values=values, address=address, slave_id=unit))
        return self._client.write_registers(address=address, values=values, unit=unit)

    def connect(self):
        wrap_call(self._logger, 'info')("Connecting to ModBus")
        self._client.connect()
        
    def close(self):
        wrap_call(self._logger, 'info')("closing ModBus connection")
        self._client.close()