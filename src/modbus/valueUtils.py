
def unscale_modbus_value(mbusvalue, scaling: float, offset: float):
    """
    Manage offset and unscaling of value read from mbus

    Note: scaling should be > 0 otherwise this method return 0
    """
    scaling = float(scaling)
    offset = float(offset)
    return int((mbusvalue - offset) / scaling if scaling > 0.0 else 0)

def scale_modbus_value(mbusvalue, scaling: float, offset: float):
    """
    Manage offset and scaling of value read from mbus

    Note: scaling should be > 0 otherwise this method return 0
    """
    scaling = float(scaling)
    offset = float(offset)
    return mbusvalue * scaling + offset if scaling >= 0.0 else 0