from enum import Enum

class ModBusRespType(Enum):
        NONE = 0
        HOLDING_REG = 1
        COILS = 2
        DISCRETE_INPUT = 3,
        INPUT_REG = 4

def modbustype_from_register(reg_str):
    """
    Deduces ModBus registrer type from address, address can 
    be a single value (ie. '40001' or a range '40001-40003')
    
    Arguments:
        reg_str {string} -- registry address as single value or range
    
    Returns:
        ModBusRespType -- Type or register
    """
    types = { 
            '0': ModBusRespType.COILS,
            '1': ModBusRespType.DISCRETE_INPUT,
            '3': ModBusRespType.INPUT_REG,
            '4': ModBusRespType.HOLDING_REG

        }
    register_address = reg_str
    regs = reg_str.split('-')
    if len(regs) > 1:
        register_address = regs[0]

    if len(register_address) == 5:
       return types[register_address[0]]
    
    # Default if type unrecognized
    return ModBusRespType.COILS