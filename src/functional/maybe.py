def wrap_call(obj, method):
    """
    Manage call to a method on a object that could be None
    """
    return (getattr(obj, method, lambda: None) if obj else lambda: None)