import pytest
from unittest.mock import MagicMock
from functional.fallible import fallible



def test_fallible(mocker):
    logger = MagicMock()
    mocker.patch.object(logger, 'exception', autospec=False)

    @fallible(ZeroDivisionError, logger=logger)
    def div(a, b):
        return a / b

    div(2, 2)
    assert not logger.exception.called

    div(2, 0)
    assert logger.exception.called