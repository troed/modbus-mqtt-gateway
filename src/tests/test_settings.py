import os
from io import StringIO
from settings import SettingsFacade
from settings import GeneralSettings, CellularModuleSettings, SerialMDBSettings, MQTTSettings
import configparser

def test_write_general_settings():
    config = configparser.ConfigParser()
    generalSettings = GeneralSettings(config, 
                                        gtwid='12334', 
                                        name='test', 
                                        adminusr='admin', 
                                        adminpwd='admin', 
                                        sysusr='root', 
                                        syspwd='root')
    output = StringIO() 
    generalSettings.write(output)
    lines = [s for s in output.getvalue().splitlines() if s.strip("\r\n")]
    assert len(lines) == 7
    assert lines[0] == "[GENERAL]"
    assert lines[1] == "id = 12334"
    assert lines[2] == "name = test"
    assert lines[3] == "admin_user = admin"
    assert lines[4] == "admin_password = admin"
    assert lines[5] == "sys_user = root"
    assert lines[6] == "sys_password = root"

def test_write_single_general_settings():
    config = configparser.ConfigParser()
    generalSettings = GeneralSettings(config, 
                                        gtwid='12334', 
                                        name='test', 
                                        adminusr='admin', 
                                        adminpwd='admin', 
                                        sysusr='root', 
                                        syspwd='root')


    generalSettings.gtwid = 'abcd' 
    generalSettings.name='name_test'
    generalSettings.adminusr='admin_test' 
    generalSettings.adminpwd='admin_pwd'
    generalSettings.sysusr='root_test'
    generalSettings.syspwd='root_pwd_test'
    
    output = StringIO() 
    generalSettings.write(output)
    lines = [s for s in output.getvalue().splitlines() if s.strip("\r\n")]
    assert len(lines) == 7
    assert lines[0] == "[GENERAL]"
    assert lines[1] == "id = abcd"
    assert lines[2] == "name = name_test"
    assert lines[3] == "admin_user = admin_test"
    assert lines[4] == "admin_password = admin_pwd"
    assert lines[5] == "sys_user = root_test"
    assert lines[6] == "sys_password = root_pwd_test"

def test_read_general_settings():
    config = configparser.ConfigParser()
    generalSettings = GeneralSettings(config)

    generalSettings.read_string("""
[GENERAL]
id = 12334
name = test
admin_user = admin
admin_password = adminpwd
sys_user = root
sys_password = rootpwd
""")
    assert generalSettings.gtwid == '12334'
    assert generalSettings.name == 'test'
    assert generalSettings.adminusr == 'admin'
    assert generalSettings.adminpwd == 'adminpwd'
    assert generalSettings.sysusr == 'root'
    assert generalSettings.syspwd == 'rootpwd'

def test_write_cellular_settings():
    config = configparser.ConfigParser()
    cellularModuleSettings = CellularModuleSettings(config, 
                                        simpin='0000', 
                                        reboot_timer='10', 
                                        protocol='3G', 
                                        apn='apn.tim.it')
    output = StringIO()
    cellularModuleSettings.write(output)
    lines = [s for s in output.getvalue().splitlines() if s.strip("\r\n")]
    assert len(lines) == 5
    assert lines[0] == "[4G]"
    assert lines[1] == "sim_pin = 0000"
    assert lines[2] == "reboot_timer = 10"
    assert lines[3] == "protocol = 3G"
    assert lines[4] == "apn = apn.tim.it"

def test_write_single_cellular_settings():
    config = configparser.ConfigParser()
    cellularModuleSettings = CellularModuleSettings(config, 
                                        simpin='0000', 
                                        reboot_timer='10', 
                                        protocol='3G', 
                                        apn='apn.tim.it')

    cellularModuleSettings.simpin='3333' 
    cellularModuleSettings.reboot_timer='11'
    cellularModuleSettings.protocol='4G'
    cellularModuleSettings.apn='apn.wind.it'

    output = StringIO()
    cellularModuleSettings.write(output)
    lines = [s for s in output.getvalue().splitlines() if s.strip("\r\n")]
    assert len(lines) == 5
    assert lines[0] == "[4G]"
    assert lines[1] == "sim_pin = 3333"
    assert lines[2] == "reboot_timer = 11"
    assert lines[3] == "protocol = 4G"
    assert lines[4] == "apn = apn.wind.it"

def test_read_cellular_settings():
    config = configparser.ConfigParser()
    cellularModuleSettings = CellularModuleSettings(config)

    cellularModuleSettings.read_string("""
[4G]
sim_pin = 0000
reboot_timer = 10
protocol = 3G
apn = apn.tim.it
""")
    assert cellularModuleSettings.simpin == '0000'
    assert cellularModuleSettings.reboot_timer == '10'
    assert cellularModuleSettings.protocol == '3G'
    assert cellularModuleSettings.apn == 'apn.tim.it'


def test_write_serial_settings():
    config = configparser.ConfigParser()
    serialMDBSettings = SerialMDBSettings(config,
                                        port='/dev/serial0',
                                        speed='9600', 
                                        parity='N', 
                                        stopbit='1', 
                                        bitlen='8',
                                        master_id='123',
                                        polling_time='12',
                                        connect_timeout='20',
                                        retry_count='3')

    output = StringIO()
    serialMDBSettings.write(output)
    lines = [s for s in output.getvalue().splitlines() if s.strip("\r\n")]
    assert len(lines) == 10
    assert lines[0] == "[SERIAL MODBUS]"
    assert lines[1] == "port = /dev/serial0"
    assert lines[2] == "speed = 9600"
    assert lines[3] == "parity = N"
    assert lines[4] == "stopbit = 1"
    assert lines[5] == "bitlen = 8"
    assert lines[6] == "master_id = 123"
    assert lines[7] == "polling_time = 12"
    assert lines[8] == "connect_timeout = 20"
    assert lines[9] == "retry_count = 3"

def test_write_single_serial_settings():
    config = configparser.ConfigParser()
    serialMDBSettings = SerialMDBSettings(config,
                                        port='/dev/serial0',
                                        speed='9600', 
                                        parity='N', 
                                        stopbit='1', 
                                        bitlen='8',
                                        master_id='123',
                                        polling_time='12',
                                        connect_timeout='20',
                                        retry_count='3')

    serialMDBSettings.port='/dev/serial1'
    serialMDBSettings.speed='19200'
    serialMDBSettings.parity='O'
    serialMDBSettings.stopbit='2'
    serialMDBSettings.bitlen='7'
    serialMDBSettings.master_id='456'
    serialMDBSettings.polling_time='15'
    serialMDBSettings.connect_timeout='50'
    serialMDBSettings.retry_count='7'

    output = StringIO()
    serialMDBSettings.write(output)
    lines = [s for s in output.getvalue().splitlines() if s.strip("\r\n")]
    assert len(lines) == 10
    assert lines[0] == "[SERIAL MODBUS]"
    assert lines[1] == "port = /dev/serial1"
    assert lines[2] == "speed = 19200"
    assert lines[3] == "parity = O"
    assert lines[4] == "stopbit = 2"
    assert lines[5] == "bitlen = 7"
    assert lines[6] == "master_id = 456"
    assert lines[7] == "polling_time = 15"
    assert lines[8] == "connect_timeout = 50"
    assert lines[9] == "retry_count = 7"

def test_read_serial_settings():
    config = configparser.ConfigParser()
    serialMDBSettings = SerialMDBSettings(config)

    serialMDBSettings.read_string("""
[SERIAL MODBUS]
port = /dev/serial0
speed = 9600
parity = N
stopbit = 1
bitlen = 8
master_id = 123
polling_time = 12
connect_timeout = 20
retry_count = 3
""")

    assert serialMDBSettings.port == '/dev/serial0'
    assert serialMDBSettings.speed == '9600'
    assert serialMDBSettings.parity == 'N'
    assert serialMDBSettings.stopbit == '1'
    assert serialMDBSettings.bitlen == '8'
    assert serialMDBSettings.master_id == '123'
    assert serialMDBSettings.polling_time == 12
    assert serialMDBSettings.connect_timeout == '20'
    assert serialMDBSettings.retry_count == '3'

def test_write_mqtt_settings():
    config = configparser.ConfigParser()
    mqttSettings = MQTTSettings(config, 
                                        broker_pri='mqtt1.nowhere.org', 
                                        broker_sec='mqtt2.nowhere.org', 
                                        qos='0', 
                                        user_auth='S',
                                        cert_auth='N')

    output = StringIO()
    mqttSettings.write(output)
    lines = [s for s in output.getvalue().splitlines() if s.strip("\r\n")]
    assert len(lines) == 6
    assert lines[0] == "[MQTT]"
    assert lines[1] == "broker_pri = mqtt1.nowhere.org"
    assert lines[2] == "broker_sec = mqtt2.nowhere.org"
    assert lines[3] == "qos = 0"
    assert lines[4] == "user_auth = S"
    assert lines[5] == "cert_auth = N"

def test_write_single_mqtt_settings():
    config = configparser.ConfigParser()
    mqttSettings = MQTTSettings(config, 
                                        broker_pri='mqtt1.nowhere.org', 
                                        broker_sec='mqtt2.nowhere.org', 
                                        qos='0', 
                                        user_auth='S',
                                        cert_auth='N')

    mqttSettings.broker_pri = 'newvalue.nowhere.org'
    mqttSettings.broker_sec = 'newvalue2.nowhere.org'
    mqttSettings.qos = '1'
    mqttSettings.user_auth = 'test'
    mqttSettings.cert_auth = 'test'
    
    output = StringIO()
    mqttSettings.write(output)
    lines = [s for s in output.getvalue().splitlines() if s.strip("\r\n")]
    assert len(lines) == 6
    assert lines[0] == "[MQTT]"
    assert lines[1] == "broker_pri = newvalue.nowhere.org"
    assert lines[2] == "broker_sec = newvalue2.nowhere.org"
    assert lines[3] == "qos = 1"
    assert lines[4] == "user_auth = test"
    assert lines[5] == "cert_auth = test"

def test_write_single_mqtt_settings_with_optionals():
    config = configparser.ConfigParser()
    mqttSettings = MQTTSettings(config, 
                                        broker_pri='mqtt1.nowhere.org', 
                                        broker_sec='mqtt2.nowhere.org', 
                                        qos='0', 
                                        user_auth='S',
                                        cert_auth='N',
                                        username='user',
                                        password='pass',
                                        cert_file='/path/cert',
                                        key_file='/pat/key')

    mqttSettings.broker_pri = 'newvalue.nowhere.org'
    mqttSettings.broker_sec = 'newvalue2.nowhere.org'
    mqttSettings.qos = '1'
    mqttSettings.user_auth = 'test'
    mqttSettings.cert_auth = 'test'
    mqttSettings.username = 'user1'
    mqttSettings.password = 'pass1'
    mqttSettings.cert_file = '/path/cert1'
    mqttSettings.key_file = '/pat/key1'
    mqttSettings.ca_file = '/path/ca1'
    
    output = StringIO()
    mqttSettings.write(output)
    lines = [s for s in output.getvalue().splitlines() if s.strip("\r\n")]
    assert len(lines) == 11
    assert lines[0] == "[MQTT]"
    assert lines[1] == "broker_pri = newvalue.nowhere.org"
    assert lines[2] == "broker_sec = newvalue2.nowhere.org"
    assert lines[3] == "qos = 1"
    assert lines[4] == "user_auth = test"
    assert lines[5] == "cert_auth = test"
    assert lines[6] == "username = user1"
    assert lines[7] == "password = pass1"
    assert lines[8] == "cert_file = /path/cert1"
    assert lines[9] == "key_file = /pat/key1"
    assert lines[10] == "ca_file = /path/ca1"

def test_read_mqtt_settings():
    config = configparser.ConfigParser()
    mqttSettings = MQTTSettings(config)

    mqttSettings.read_string("""
[MQTT]
broker_pri = mqtt1.nowhere.org
broker_sec = mqtt2.nowhere.org
qos = 0
user_auth = S
cert_auth = N
    """)
    
    assert mqttSettings.broker_pri == 'mqtt1.nowhere.org'
    assert mqttSettings.broker_sec == 'mqtt2.nowhere.org'
    assert mqttSettings.qos == '0'
    assert mqttSettings.user_auth == 'S'
    assert mqttSettings.cert_auth == 'N'
    assert mqttSettings.username == None
    assert mqttSettings.password == None
    assert mqttSettings.cert_file == None
    assert mqttSettings.key_file == None

def test_read_mqtt_settings_with_optionals():
    config = configparser.ConfigParser()
    mqttSettings = MQTTSettings(config)

    mqttSettings.read_string("""
[MQTT]
broker_pri = mqtt1.nowhere.org
broker_sec = mqtt2.nowhere.org
qos = 0
user_auth = S
cert_auth = N
username = user1
password = pass1
cert_file = /path/cert1
key_file = /path/key1
ca_file = /path/ca1
    """)
    
    assert mqttSettings.broker_pri == 'mqtt1.nowhere.org'
    assert mqttSettings.broker_sec == 'mqtt2.nowhere.org'
    assert mqttSettings.qos == '0'
    assert mqttSettings.user_auth == 'S'
    assert mqttSettings.cert_auth == 'N'
    assert mqttSettings.username == 'user1'
    assert mqttSettings.password == 'pass1'
    assert mqttSettings.cert_file == '/path/cert1'
    assert mqttSettings.key_file == '/path/key1'
    assert mqttSettings.ca_file == '/path/ca1'

def test_settings_facade():
    dirname = os.path.dirname(__file__)
    sets = SettingsFacade(general_ini_file=os.path.join(dirname,'./fixtures/general_test.ini'), 
                            serial_ini_file=os.path.join(dirname,'./fixtures/mdb_test.ini'), 
                            cellular_ini_file=os.path.join(dirname,'./fixtures/cellular_test.ini'), 
                            mqtt_ini_file=os.path.join(dirname,'./fixtures/mqtt_test.ini'))
                    
    sets.read_settings()

    assert sets.cellular.simpin == '0000'
    assert sets.general.name == 'test'
    assert sets.serial.port == '/dev/serial0'
    assert sets.mqtt.broker_pri == 'mqtt1.nowhere.org'
    

