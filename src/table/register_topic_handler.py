from table.csvtable import CsvTable
from table.csvtable import TOPIC,DESCR,CSCALING_COEFF,OFFSET,UI,REG_RANGE,SLAVE_ID,ACCESS,SEND_ON_VARIATION,VARIATION,VARIATION_TYPE,RECURRING_SEND,RECURRING_SPAN,ADD_UI,ADD_DESC,OPT_1,OPT_2,OPT_3
from table.table_item import TableItem


def dictToTableItem(row):
    item=TableItem(
        topic=row[TOPIC],
        descr=row[DESCR],
        cscaling_coeff=row[CSCALING_COEFF],
        offset=row[OFFSET],
        ui=row[UI],
        registry_range=row[REG_RANGE],
        slave_id=row[SLAVE_ID],
        access=row[ACCESS],
        send_on_variation=row[SEND_ON_VARIATION],
        variation=row[VARIATION],
        variation_type=row[VARIATION_TYPE],
        recurring_send=row[RECURRING_SEND],
        recurring_span=row[RECURRING_SPAN],
        add_ui=row[ADD_UI],
        add_descr=row[ADD_DESC],
        opt1=row[OPT_1],
        opt2=row[OPT_2],
        opt3=row[OPT_3])
    return item

# TODO: Handle table reading errors
def readRegisterTopicTable(tableFile):
    """
    Reads the CVS file containing configuration for register to topics (MQTT) mapping
        :param tableFile: Full file path with the table

    Returns
    -------
    list
        a list of TableItem objects
    """
    ret = []
    with open(tableFile, 'r') as csvfile:
        c = CsvTable(csvfile) 
        (errors, table) = c.tableListValidated()
        ret = list(map(lambda row: dictToTableItem(row), table))
 
    return ret

def writeRegisterTopicTable(tableFile, tableArray):
    """
    Write the TableItem array on the file
        :param tableFile: full file path where to write
        :param tableArray: TableItem array to write
    """
    with open(tableFile, 'w') as csvfile:
        c = CsvTable(csvfile)
        c.tableWriter(tableArray)