from table.csvtable import TOPIC,DESCR,CSCALING_COEFF,OFFSET,UI,REG_RANGE,SLAVE_ID,ACCESS,SEND_ON_VARIATION,VARIATION,VARIATION_TYPE,RECURRING_SEND,RECURRING_SPAN,ADD_UI,ADD_DESC,OPT_1,OPT_2,OPT_3

class TableItem(object):
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    @property
    def slave_id(self):
        return int(self.__dict__[SLAVE_ID])
        
    @property
    def add_ui(self):
        return self.__dict__[ADD_UI] == 'S'

    @property
    def send_on_variation(self):
        return self.__dict__[SEND_ON_VARIATION] == 'S'

    @property
    def add_descr(self):
        return self.__dict__[ADD_DESC] == 'S'

    @property
    def recurring_send(self):
        return self.__dict__[RECURRING_SEND] == 'S'

  
    @property
    def registries(self):
        a, b = (self.__dict__[REG_RANGE].split('-', 2) + [None])[:2]
        (start, stop) = (int(a), int(b) if b is not None else int(a))
        return list(map(lambda v: {'address': str(v), 'index': reg_to_index(v)}, range(start, stop+1)))

    def to_dict(self):
        return self.__dict__

def reg_to_index(reg_addr):
    """
    Convert registry address to value following the ModBus specs

    1-9999       - Disctrete output coils => 0-9998 (base 1)
    10001-19999  - Disctrete input contracts => 0-9998 (base - 10001)
    30001-39999  - Analog input register => 0-9998 (base - 30001)
    40001-49999  - Analog output holding register => 0-9998 (base - 40001)

    Arguments:
        reg_addr {int} -- Register address 
    """

    if reg_addr <= 19999 and reg_addr > 9999:
        return reg_addr - 10001
    elif reg_addr <= 39999 and reg_addr > 19999:
        return reg_addr - 30001
    elif reg_addr <= 49999 and reg_addr > 39999:
        return reg_addr - 40001
    else:
        return reg_addr - 1
    