from __future__ import print_function
import csv
import re


# Const 
DELIMITER=';'

# Key constants
TOPIC = "topic"
DESCR = "descr"
CSCALING_COEFF = "cscaling_coeff"
OFFSET = "offset"
UI = "ui"
REG_RANGE = "registry_range"
SLAVE_ID = "slave_id"
ACCESS = "access"
SEND_ON_VARIATION = "send_on_variation"
VARIATION = "variation"
VARIATION_TYPE = "variation_type"
RECURRING_SEND = "recurring_send"
RECURRING_SPAN  = "recurring_span"
ADD_UI = "add_ui"
ADD_DESC = "add_descr"
OPT_1 = "opt1"
OPT_2 = "opt2"
OPT_3 = "opt3"


def max_length(max_length):
    """
    Return a value check function which raises a ValueError if the len of
    the value is greater than max_length
    """

    def checker(v):
        return len(v) > max_length
    return checker

def convert_to_float():
    """
    Check if the value can be converted in a float
    """

    def checker(v):
        try:
            float(v)
            return False
        except ValueError:
            return True

    return checker

def convert_to_int():
    """
    Check if the value can be converted in a float
    """

    def checker(v):
        try:
            int(v)
            return False
        except ValueError:
            return True

    return checker

def one_of(*values):
    """
    Check if value is one of values
    """
    def checker(v):
        return not v in values
    return checker

class CSVTableValidator:
    def __init__(self):
        self._validators = []

    def add_field_validator(self, field_name, validator, message):
        self._validators.append((field_name, validator, message))
    
    def validate(self, row):
        errors = []
        for field_name, validator, message in self._validators:
            if validator(row[field_name]) == True:
                errors.append(message.format(value=row[field_name]))

        return errors


class CsvTable:
    def __init__(self, csvStream):
        """
        Abstraction to manage the interface to a CSV file.

        Extended description of function.

        Parameters
        ----------
        csvStream : Stream
            Stream where to read from or write to
        Returns
        -------
        int
            Description of return value

        """ 
        self._headers = [TOPIC,
            DESCR, CSCALING_COEFF, OFFSET, UI, REG_RANGE, SLAVE_ID, 
            ACCESS, SEND_ON_VARIATION, VARIATION, VARIATION_TYPE, RECURRING_SEND, 
            RECURRING_SPAN, ADD_UI, ADD_DESC, OPT_1, OPT_2, OPT_3]
        self._csvStream = csvStream


    def tableListValidated(self):
        tableList = list(csv.DictReader(self._csvStream, delimiter=DELIMITER))

        validator = CSVTableValidator()
        validator.add_field_validator(TOPIC, max_length(32), "unexpected 'topic' length ({value})")
        validator.add_field_validator(DESCR, max_length(180),"unexpected 'descr' length ({value})")
        validator.add_field_validator(CSCALING_COEFF, convert_to_float(), "'cscaling_coeff' must be a float ({value})")
        validator.add_field_validator(OFFSET, convert_to_float(), "'offset' must be a float ({value})")
        validator.add_field_validator(UI, max_length(20),  "unexpected 'ui' length ({value})")
        validator.add_field_validator(REG_RANGE, max_length(50),  "unexpected 'registry_range' length ({value})")
        validator.add_field_validator(SLAVE_ID, convert_to_int(), "'slave_id' must be an integer ({value})")
        validator.add_field_validator(ACCESS, one_of('R', 'R/W', 'RW'), "invalid 'access' value ({value})")
        validator.add_field_validator(SEND_ON_VARIATION, one_of('S', 'N'), "invalid 'send_on_variation' value ({value})")
        validator.add_field_validator(VARIATION_TYPE, one_of('%', 'A'), "invalid 'variation_type' value ({value})")
        validator.add_field_validator(RECURRING_SEND, one_of('S', 'N'), "invalid 'recurring_send' value ({value})")
        validator.add_field_validator(ADD_UI, one_of('S', 'N'), "invalid 'add_ui' value ({value})")
        validator.add_field_validator(ADD_DESC, one_of('S', 'N'), "invalid 'add_descr' value ({value})")

        errors = list(filter(lambda varray: len(varray) > 0, map(lambda item: validator.validate(item), tableList)))
        return (errors, tableList)

    def tableWriter(self, tableArray):
        writer = csv.DictWriter(self._csvStream, fieldnames=self._headers, delimiter=DELIMITER)
        writer.writeheader()
        for tableDictItem in tableArray:
            writer.writerow(tableDictItem.__dict__)

            


    
