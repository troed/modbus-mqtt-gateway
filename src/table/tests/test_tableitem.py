from table.table_item import TableItem, reg_to_index


def test_tableitem_init():
    tableArray = [
                TableItem(topic='test/topic',
                        descr='test descr',
                        cscaling_coeff=0,
                        offset=0,
                        ui='A',
                        registry_range='10001',
                        slave_id=1,
                        access='R',
                        send_on_variation='S',
                        variation=5,
                        variation_type='%',
                        recurring_send='S',
                        recurring_span=1,
                        add_ui='S',
                        add_descr='S',
                        opt1='o1',
                        opt2='o2',
                        opt3='o3'),
                TableItem(topic='prova/topic2',
                        descr='decription',
                        cscaling_coeff=2,
                        offset=10,
                        ui='V',
                        registry_range='2',
                        slave_id=123,
                        access='W',
                        send_on_variation='S',
                        variation=12,
                        variation_type='%',
                        recurring_send='N',
                        recurring_span=0,
                        add_ui='S',
                        add_descr='S',
                        opt1='o1-1',
                        opt2='o2-1',
                        opt3='o3-1'),
                TableItem(topic='prova/topic3',
                        descr='decription',
                        cscaling_coeff=2,
                        offset=10,
                        ui='V',
                        registry_range='40002-40004',
                        slave_id=123,
                        access='W',
                        send_on_variation='S',
                        variation=12,
                        variation_type='%',
                        recurring_send='N',
                        recurring_span=0,
                        add_ui='N',
                        add_descr='N',
                        opt1='o1-1',
                        opt2='o2-1',
                        opt3='o3-1')
                ]

    assert tableArray[0].topic == 'test/topic'
    assert tableArray[0].descr == 'test descr'
    assert tableArray[0].cscaling_coeff == 0
    assert tableArray[0].offset == 0
    assert tableArray[0].ui == 'A'
    assert tableArray[0].registry_range == '10001'
    assert len(tableArray[0].registries) == 1
    assert tableArray[0].registries[0]['address'] == '10001'
    assert tableArray[0].registries[0]['index'] == 0
    assert tableArray[0].slave_id == 1
    assert tableArray[0].access == 'R'
    assert tableArray[0].send_on_variation == True
    assert tableArray[0].variation == 5
    assert tableArray[0].variation_type == '%'
    assert tableArray[0].recurring_send == True
    assert tableArray[0].recurring_span == 1
    assert tableArray[0].add_ui == True
    assert tableArray[0].add_descr == True
    assert tableArray[0].opt1 == 'o1'
    assert tableArray[0].opt2 == 'o2'
    assert tableArray[0].opt3 == 'o3'

    assert tableArray[1].topic == 'prova/topic2'
    assert tableArray[1].descr == 'decription'
    assert tableArray[1].cscaling_coeff == 2
    assert tableArray[1].offset == 10
    assert tableArray[1].ui == 'V'
    assert tableArray[1].registry_range == '2'
    assert tableArray[1].registries[0]['address'] == '2'
    assert tableArray[1].registries[0]['index'] == 1
    assert tableArray[1].slave_id == 123
    assert tableArray[1].access == 'W'
    assert tableArray[1].send_on_variation == True
    assert tableArray[1].variation == 12
    assert tableArray[1].variation_type == '%'
    assert tableArray[1].recurring_send == False
    assert tableArray[1].recurring_span == 0
    assert tableArray[1].add_ui == True
    assert tableArray[1].add_descr == True
    assert tableArray[1].opt1 == 'o1-1'
    assert tableArray[1].opt2 == 'o2-1'
    assert tableArray[1].opt3 == 'o3-1'

    assert tableArray[2].topic == 'prova/topic3'
    assert tableArray[2].descr == 'decription'
    assert tableArray[2].cscaling_coeff == 2
    assert tableArray[2].offset == 10
    assert tableArray[2].ui == 'V'
    assert tableArray[2].registry_range == '40002-40004'
    assert len(tableArray[2].registries) == 3
    assert tableArray[2].registries[0]['address'] == '40002'
    assert tableArray[2].registries[0]['index'] == 1
    assert tableArray[2].registries[1]['address'] == '40003'
    assert tableArray[2].registries[1]['index'] == 2
    assert tableArray[2].registries[2]['address'] == '40004'
    assert tableArray[2].registries[2]['index'] == 3
    assert tableArray[2].slave_id == 123
    assert tableArray[2].access == 'W'
    assert tableArray[2].send_on_variation == True
    assert tableArray[2].variation == 12
    assert tableArray[2].variation_type == '%'
    assert tableArray[2].recurring_send == False
    assert tableArray[2].recurring_span == 0
    assert tableArray[2].add_ui == False
    assert tableArray[2].add_descr == False
    assert tableArray[2].opt1 == 'o1-1'
    assert tableArray[2].opt2 == 'o2-1'
    assert tableArray[2].opt3 == 'o3-1'

def test_reg_to_index():       
    assert reg_to_index(1) == 0
    assert reg_to_index(10001) == 0
    assert reg_to_index(10003) == 2
    assert reg_to_index(30003) == 2
    assert reg_to_index(40003) == 2
    assert reg_to_index(49999) == 9998