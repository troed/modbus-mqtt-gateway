import os
from table.table_item import TableItem
from table.register_topic_handler import readRegisterTopicTable, writeRegisterTopicTable

def test_read_fields():
    table = readRegisterTopicTable('./table/tests/fixtures/regs-topics.csv')
    assert len(table) == 3
    index = 0
    
    topics = ('/sample1', '/sample2', '/sample3')
    descrs = ('descr1', 'descr2', 'descr3')
    cscalings = ('1', '2', '3')
    offsets = ('0', '5', '8')
    uis = ('V', 'A', 'V')
    regs = ('40001-40010', '40000', '40001-40010')
    slaveids = (1, 2, 3)
    accesses = ('R', 'W', 'R')
    sendonvars = (True, True, False)  ## CHECK WITH DOCS
    variations = ('10', '11', '12')
    vartypes = ('%', '%', '%')
    recsends = (False,  True, True)
    recurspans = ('1', '1', '1')
    adduis = (False, True, True)
    adddescrs = (True, False, True)
    opts1s = ('opt1_1', 'opt1_2', 'opt1_3')
    opts2s = ('opt2_1', 'opt2_2', 'opt2_3')
    opts3s = ('opt3_1', 'opt3_2', 'opt3_3')
    for line in table:
        assert line.topic == topics[index]
        assert line.descr == descrs[index]
        assert line.cscaling_coeff == cscalings[index]
        assert line.offset == offsets[index]
        assert line.ui == uis[index]
        assert line.registry_range == regs[index]
        assert line.slave_id == slaveids[index]
        assert line.access == accesses[index]
        assert line.send_on_variation == sendonvars[index]
        assert line.variation == variations[index]
        assert line.variation_type == vartypes[index]
        assert line.recurring_send == recsends[index]
        assert line.recurring_span == recurspans[index]
        assert line.add_ui == adduis[index]
        assert line.add_descr == adddescrs[index] 
        assert line.opt1 == opts1s[index]
        assert line.opt2 == opts2s[index]
        assert line.opt3 == opts3s[index]  

        index = index + 1

def test_write_fields():
        tableArray = [
                TableItem(topic='test/topic',
                        descr='test descr',
                        cscaling_coeff=0,
                        offset=0,
                        ui='A',
                        registry_range='40000',
                        slave_id=1,
                        access='R',
                        send_on_variation='S',
                        variation=5,
                        variation_type='%',
                        recurring_send='S',
                        recurring_span=1,
                        add_ui='S',
                        add_descr='S',
                        opt1='o1',
                        opt2='o2',
                        opt3='o3'),
                TableItem(topic='prova/topic',
                        descr='decription',
                        cscaling_coeff=1,
                        offset=10,
                        ui='V',
                        registry_range='41000',
                        slave_id=123,
                        access='W',
                        send_on_variation='S',
                        variation=12,
                        variation_type='%',
                        recurring_send='N',
                        recurring_span=0,
                        add_ui='S',
                        add_descr='S',
                        opt1='o1-1',
                        opt2='o2-1',
                        opt3='o3-1')
                ]
        file_name = './test.csv.log'
        writeRegisterTopicTable(file_name, tableArray)
        with open(file_name, 'r') as written_file:
                lines = written_file.read().splitlines()
                assert lines[0] == "topic;descr;cscaling_coeff;offset;ui;registry_range;slave_id;access;send_on_variation;variation;variation_type;recurring_send;recurring_span;add_ui;add_descr;opt1;opt2;opt3"
                assert lines[1] == "test/topic;test descr;0;0;A;40000;1;R;S;5;%;S;1;S;S;o1;o2;o3"
                assert lines[2] == "prova/topic;decription;1;10;V;41000;123;W;S;12;%;N;0;S;S;o1-1;o2-1;o3-1"

        os.remove(file_name)
