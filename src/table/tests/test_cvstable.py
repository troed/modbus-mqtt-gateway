import re
from table.csvtable import CsvTable
from table.table_item import TableItem
from io import StringIO
from table.csvtable import TOPIC,DESCR,CSCALING_COEFF,OFFSET,UI,REG_RANGE,SLAVE_ID,ACCESS,SEND_ON_VARIATION,VARIATION,VARIATION_TYPE,RECURRING_SEND,RECURRING_SPAN,ADD_UI,ADD_DESC,OPT_1,OPT_2,OPT_3


def test_fields():
    f = StringIO("""topic;descr;cscaling_coeff;offset;ui;registry_range;slave_id;access;send_on_variation;variation;variation_type;recurring_send;recurring_span;add_ui;add_descr;opt1;opt2;opt3\n
/sample1;descr1;1;0;1 V;40001-40010;1;R;S;10;%;S;1;S;S;opt1_1;opt2_1;opt3_1\n
/sample2;descr2;2;5;1 A;40000;2;RW;S;11;%;S;1;S;N;opt1_2;opt2_2;opt3_2\n
/sample3;descr3;3;8;3 V;40001-40010;3;R;N;12;%;S;1;S;S;opt1_3;opt2_3;opt3_3""" )
    c = CsvTable(f)   
    (errors, table) = c.tableListValidated()

    assert len(errors) == 0
    index = 0
   
    topics = ('/sample1', '/sample2', '/sample3')
    descrs = ('descr1', 'descr2', 'descr3')
    cscalings = ('1', '2', '3')
    offsets = ('0', '5', '8')
    uis = ('1 V', '1 A', '3 V')
    regs = ('40001-40010', '40000', '40001-40010')
    slaveids = ('1', '2', '3')
    accesses = ('R', 'RW', 'R')
    sendonvars = ('S', 'S', 'N')  ## CHECK WITH DOCS
    variations = ('10', '11', '12')
    vartypes = ('%', '%', '%')
    recsends = ('S', 'S', 'S')
    recurspans = ('1', '1', '1')
    adduis = ('S', 'S', 'S')
    adddescrs = ('S', 'N', 'S')
    opts1s = ('opt1_1', 'opt1_2', 'opt1_3')
    opts2s = ('opt2_1', 'opt2_2', 'opt2_3')
    opts3s = ('opt3_1', 'opt3_2', 'opt3_3')
    for line in table:
        assert line[TOPIC] == topics[index]
        assert line[DESCR] == descrs[index]
        assert line[CSCALING_COEFF] == cscalings[index]
        assert line[OFFSET] == offsets[index]
        assert line[UI] == uis[index]
        assert line[REG_RANGE] == regs[index]
        assert line[SLAVE_ID] == slaveids[index]
        assert line[ACCESS] == accesses[index]
        assert line[SEND_ON_VARIATION] == sendonvars[index]
        assert line[VARIATION] == variations[index]
        assert line[VARIATION_TYPE] == vartypes[index]
        assert line[RECURRING_SEND] == recsends[index]
        assert line[RECURRING_SPAN] == recurspans[index]
        assert line[ADD_UI] == adduis[index]
        assert line[ADD_DESC] == adddescrs[index] 
        assert line[OPT_1] == opts1s[index]
        assert line[OPT_2] == opts2s[index]
        assert line[OPT_3] == opts3s[index] 

        index = index + 1

def test_writer():
    output = StringIO() 
    c = CsvTable(output)
    tableArray = [
        TableItem(topic='test/topic',
                    descr='test descr',
                    cscaling_coeff=0,
                    offset=0,
                    ui='A',
                    registry_range='40000',
                    slave_id=1,
                    access='R',
                    send_on_variation='S',
                    variation=5,
                    variation_type='%',
                    recurring_send='S',
                    recurring_span=1,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1',
                    opt2='o2',
                    opt3='o3'),
        TableItem(topic='prova/topic',
                    descr='decription',
                    cscaling_coeff=1,
                    offset=10,
                    ui='V',
                    registry_range='41000',
                    slave_id=123,
                    access='W',
                    send_on_variation='S',
                    variation=12,
                    variation_type='%',
                    recurring_send='N',
                    recurring_span=0,
                    add_ui='S',
                    add_descr='S',
                    opt1='o1-1',
                    opt2='o2-1',
                    opt3='o3-1')
    ]
    c.tableWriter(tableArray)
    buffer = output.getvalue()
    lines = buffer.splitlines()
    assert lines[0] == "topic;descr;cscaling_coeff;offset;ui;registry_range;slave_id;access;send_on_variation;variation;variation_type;recurring_send;recurring_span;add_ui;add_descr;opt1;opt2;opt3"
    assert lines[1] == "test/topic;test descr;0;0;A;40000;1;R;S;5;%;S;1;S;S;o1;o2;o3"
    assert lines[2] == "prova/topic;decription;1;10;V;41000;123;W;S;12;%;N;0;S;S;o1-1;o2-1;o3-1"

def test_validated():
    f = StringIO("""topic;descr;cscaling_coeff;offset;ui;registry_range;slave_id;access;send_on_variation;variation;variation_type;recurring_send;recurring_span;add_ui;add_descr;opt1;opt2;opt3\n
/sample1dddddeekjfkejfkejfkejkfjefjekesample1dddddeekjfkejfkejfkejkfjefjekesample1dddddeekjfkejfkejfkejkfjefjekesample1dddddeekjfkejfkejfkejkfjefjeke;descr1;1;0;1 V;40001-40010;1;R;S;10;%;S;1;S;S;opt1_1;opt2_1;opt3_1\n
/sample2;descr2;A;5;1 A;40000;2;W;S;11;%;S;1;S;N;opt1_2;opt2_2;opt3_2\n
/samplele3;descr1;1;0;1 V;40001-40010;1;R;P;10;%;U;1;R;T;opt_A;opt_B;opt_C\n
sample4;description 4;1;0;1 V;10001;1;R;N;10;E;N;1;S;S;opt_O;opt_M;opt_U""" )
    c = CsvTable(f) 
    (errors, _) = c.tableListValidated()

    assert len(errors) > 0
    assert len(errors[0]) == 1
    assert re.match("unexpected 'topic' length *.", errors[0][0])
    assert len(errors[1]) == 2
    assert re.match("'cscaling_coeff' must be a float *.", errors[1][0])
    assert re.match("invalid 'access' value *.", errors[1][1])
    assert len(errors[2]) == 4
    assert re.match("invalid 'send_on_variation' value *.", errors[2][0])
    assert re.match("invalid 'recurring_send' value *.", errors[2][1])
    assert re.match("invalid 'add_ui' value *.", errors[2][2])
    assert re.match("invalid 'add_descr' value *.", errors[2][3])
    assert len(errors[3]) == 1
    assert re.match("invalid 'variation_type' value *.", errors[3][0])