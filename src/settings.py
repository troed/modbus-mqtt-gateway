import os
from dotenv import load_dotenv
load_dotenv()

import configparser


class SettingsBase(object):
    def __init__(self, config, section_name):
        self._config = config
        self._section_name = section_name

    def write(self, stream):
        self._config.write(stream)

    def read_string(self, ini_buffer):
        self._config.read_string(ini_buffer)

    def read(self, inifile):
        self._config.read(inifile)
    
GENERAL='GENERAL'
GENERAL_MAP = {
    'id': 'gtwid',
    'name':'name',
    'admin_user': 'adminusr',
    'admin_password': 'adminpwd',
    'sys_user': 'sysusr',
    'sys_password': 'syspwd'
}

class GeneralSettings(SettingsBase):

    def __init__(self, config, **kwargs):
        super(GeneralSettings, self).__init__(config, GENERAL)

        config[self._section_name] = {}
        for cfg_key in GENERAL_MAP.keys():
            if GENERAL_MAP[cfg_key] in kwargs:
                config[self._section_name][cfg_key] = kwargs[GENERAL_MAP[cfg_key]]

    @property
    def gtwid(self):
        return self._config[self._section_name]['id']

    @gtwid.setter
    def gtwid(self, value):
        self._config[self._section_name]['id'] = value

    @property
    def name(self):
        return self._config[self._section_name]['name']

    @name.setter
    def name(self, value):
        self._config[self._section_name]['name'] = value

    @property
    def adminusr(self):
        return self._config[self._section_name]['admin_user']

    @adminusr.setter
    def adminusr(self, value):
        self._config[self._section_name]['admin_user'] = value

    @property
    def adminpwd(self):
        return self._config[self._section_name]['admin_password']

    @adminpwd.setter
    def adminpwd(self, value):
        self._config[self._section_name]['admin_password'] = value

    @property
    def sysusr(self):
        return self._config[self._section_name]['sys_user']   

    @sysusr.setter
    def sysusr(self, value):
        self._config[self._section_name]['sys_user'] = value

    @property
    def syspwd(self):
        return self._config[self._section_name]['sys_password']
    
    @syspwd.setter
    def syspwd(self, value):
        self._config[self._section_name]['sys_password'] = value


CELLULAR='4G' 
CELLULAR_MAP = {
    'sim_pin': 'simpin',
    'reboot_timer': 'reboot_timer',
    'protocol': 'protocol',
    'apn': 'apn'
}

class CellularModuleSettings(SettingsBase):
    def __init__(self, config, **kwargs):
        super(CellularModuleSettings, self).__init__(config, CELLULAR)

        config[self._section_name] = {}
        for cfg_key in CELLULAR_MAP.keys():
            if CELLULAR_MAP[cfg_key] in kwargs:
                config[self._section_name][cfg_key] = kwargs[CELLULAR_MAP[cfg_key]]

    @property
    def simpin(self):
        return self._config[self._section_name]['sim_pin']

    @simpin.setter
    def simpin(self, value):
        self._config[self._section_name]['sim_pin'] = value

    @property
    def reboot_timer(self):
        return self._config[self._section_name]['reboot_timer']

    @reboot_timer.setter
    def reboot_timer(self, value):
        self._config[self._section_name]['reboot_timer'] = value

    @property
    def protocol(self):
        return self._config[self._section_name]['protocol']

    @protocol.setter
    def protocol(self, value):
        self._config[self._section_name]['protocol'] = value

    @property
    def apn(self):
        return self._config[self._section_name]['apn']    
    
    @apn.setter
    def apn(self, value):
        self._config[self._section_name]['apn'] = value

       
SERIALMDB='SERIAL MODBUS'
SERIALMDB_MAP = {
    'port': 'port',
    'speed': 'speed',
    'parity': 'parity',
    'stopbit': 'stopbit',
    'bitlen': 'bitlen',
    'master_id': 'master_id',
    'polling_time': 'polling_time',
    'connect_timeout': 'connect_timeout',
    'retry_count': 'retry_count'
}

class SerialMDBSettings(SettingsBase):
    def __init__(self, config, **kwargs):
        super(SerialMDBSettings, self).__init__(config, SERIALMDB)
         
        config[self._section_name] = {}
        for cfg_key in SERIALMDB_MAP.keys():
            if SERIALMDB_MAP[cfg_key] in kwargs:
                config[self._section_name][cfg_key] = kwargs[SERIALMDB_MAP[cfg_key]]

    @property
    def port(self):
        return self._config[self._section_name]['port'] 

    @port.setter
    def port(self, value):
        self._config[self._section_name]['port'] = value

    @property
    def speed(self):
        return self._config[self._section_name]['speed']    

    @speed.setter
    def speed(self, value):
        self._config[self._section_name]['speed'] = value

    @property
    def parity(self):
        return self._config[self._section_name]['parity']    

    @parity.setter
    def parity(self, value):
        self._config[self._section_name]['parity'] = value

    @property
    def stopbit(self):
        return self._config[self._section_name]['stopbit'] 

    @stopbit.setter
    def stopbit(self, value):
        self._config[self._section_name]['stopbit'] = value

    @property
    def bitlen(self):
        return self._config[self._section_name]['bitlen']

    @bitlen.setter
    def bitlen(self, value):
        self._config[self._section_name]['bitlen'] = value

    @property
    def master_id(self):
        return self._config[self._section_name]['master_id']
    
    @master_id.setter
    def master_id(self, value):
        self._config[self._section_name]['master_id'] = value

    @property
    def polling_time(self):
        return int(self._config[self._section_name]['polling_time'])

    @polling_time.setter
    def polling_time(self, value):
        self._config[self._section_name]['polling_time'] = value

    @property
    def connect_timeout(self):
        return self._config[self._section_name]['connect_timeout']
    
    @connect_timeout.setter
    def connect_timeout(self, value):
        self._config[self._section_name]['connect_timeout'] = value

    @property
    def retry_count(self):
        return self._config[self._section_name]['retry_count']

    @retry_count.setter
    def retry_count(self, value):
        self._config[self._section_name]['retry_count'] = value

MQTT='MQTT'
MQTT_MAP = {
    'broker_pri':'broker_pri',
    'broker_sec': 'broker_sec',
    'qos':'qos',
    'user_auth': 'user_auth',
    'cert_auth':'cert_auth',
    'username': 'username',
    'password': 'password',
    'cert_file': 'cert_file',
    'key_file': 'key_file',
    'ca_file': 'ca_file',
}

class MQTTSettings(SettingsBase):
    def __init__(self, config, **kwargs):
        super(MQTTSettings, self).__init__(config, MQTT)

        config[self._section_name] = {}
        for cfg_key in MQTT_MAP.keys():
            if MQTT_MAP[cfg_key] in kwargs:
                config[self._section_name][cfg_key] = kwargs[MQTT_MAP[cfg_key]]
    
    @property
    def broker_pri(self):
        return self._config[self._section_name]['broker_pri']

    @broker_pri.setter
    def broker_pri(self, value):
        self._config[self._section_name]['broker_pri'] = value
    
    @property
    def broker_sec(self):
        return self._config[self._section_name]['broker_sec']

    @broker_sec.setter
    def broker_sec(self, value):
        self._config[self._section_name]['broker_sec'] = value

    @property
    def qos(self):
        return self._config[self._section_name]['qos']
    
    @qos.setter
    def qos(self, value):
        self._config[self._section_name]['qos'] = value

    @property
    def user_auth(self):
        return self._config[self._section_name]['user_auth']
    
    @user_auth.setter
    def user_auth(self, value):
        self._config[self._section_name]['user_auth'] = value

    @property
    def cert_auth(self):
        return self._config[self._section_name]['cert_auth']

    @cert_auth.setter
    def cert_auth(self, value):
        self._config[self._section_name]['cert_auth'] = value

    # Note: it might be worthwhile to introduce a decorator to manage 'optional' values
    @property
    def username(self):
        return self._config[self._section_name]['username'] if 'username' in self._config[self._section_name] else None

    @username.setter
    def username(self, value):
        self._config[self._section_name]['username'] = value

    @property
    def password(self):
        return self._config[self._section_name]['password'] if 'password' in self._config[self._section_name] else None

    @password.setter
    def password(self, value):
        self._config[self._section_name]['password'] = value

    @property
    def cert_file(self):
        return self._config[self._section_name]['cert_file'] if 'cert_file' in self._config[self._section_name] else None

    @cert_file.setter
    def cert_file(self, value):
        self._config[self._section_name]['cert_file'] = value

    @property
    def key_file(self):
        return self._config[self._section_name]['key_file'] if 'key_file' in self._config[self._section_name] else None

    @key_file.setter
    def key_file(self, value):
        self._config[self._section_name]['key_file'] = value

    @property
    def ca_file(self):
        return self._config[self._section_name]['ca_file'] if 'ca_file' in self._config[self._section_name] else None

    @ca_file.setter
    def ca_file(self, value):
        self._config[self._section_name]['ca_file'] = value


class SettingsFacade(object):
    """
    Facade object to expose settings to consumer aggregated
    """

    def __init__(self, general_ini_file, serial_ini_file, cellular_ini_file, mqtt_ini_file):
        # General
        self._general_ini_file = general_ini_file
        config_general = configparser.ConfigParser()
        self._generalSettings = GeneralSettings(config_general)
        
        # MDB (serial)
        self._serial_ini_file = serial_ini_file
        config_serial = configparser.ConfigParser()
        self._serialSettings = SerialMDBSettings(config_serial)

        # Cellular
        self._cellular_ini_file = cellular_ini_file
        config_cellular = configparser.ConfigParser()
        self._cellularSettings = CellularModuleSettings(config_cellular)

        # MQTT
        self._mqtt_ini_file = mqtt_ini_file
        config_mqtt = configparser.ConfigParser()
        self._mqttSettings = MQTTSettings(config_mqtt)

    @property 
    def general(self):
        return self._generalSettings

    @property 
    def serial(self):
        return self._serialSettings

    @property 
    def cellular(self):
        return self._cellularSettings

    @property 
    def mqtt(self):
        return self._mqttSettings

    def read_settings(self):
        self._generalSettings.read(self._general_ini_file)
        self._serialSettings.read(self._serial_ini_file)
        self._cellularSettings.read(self._cellular_ini_file)
        self._mqttSettings.read(self._mqtt_ini_file)
        

    