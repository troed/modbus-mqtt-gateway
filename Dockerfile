#FROM python:3.6.8-stretch
ARG distro=stretch
FROM resin/rpi-raspbian:$distro

RUN apt-get update && apt-get install -y perl python3 gcc libc-dev python3-pip python3-dev --no-install-recommends  && \ 
    rm -rf /var/lib/apt/lists/*

RUN mkdir -p /usr/src/app
COPY src /usr/src/app
COPY requirements.txt /usr/src/app
WORKDIR /usr/src/app
RUN pip3 install -r requirements.txt

CMD ["python3", "main.py"]