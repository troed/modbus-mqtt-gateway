# Modbus to MQTT Gateway

## Provisioning Raspberry Pi

Below all the information to provision a Raspberry Pi starting from installing the operating system to the application.

### Installing the OS

- Download Rasbian image "Raspbian Stretch Lite" from [here](https://www.raspberrypi.org/downloads/raspbian/)
- Install the image following the [instruction](https://www.raspberrypi.org/documentation/installation/installing-images/README.md)
- After installing Raspbian boot the device and apply following steps:
  - sudo raspi-config
  - goto '5 Interfacing Options'
  - goto 'P6 Serial'
  - 'Would you like a login shell to be accessible over serial?' --> NO
  - 'Would you like the serial port hardware to be enabled?' --> YES
  - Finish raspi-config
- reboot the Raspberry Pi

### Method A - provisioning with Docker

- Install Docker with `curl -fsSL get.docker.com -o get-docker.sh && sh get-docker.sh` (run as root)
- Add user for docker `sudo groupadd docker`
- Add user to docker group `sudo gpasswd -a $USER docker`
- ...

### Method B - provisioning on the "bare system"

- Install Python 3.6
- Run `sudo apt-get update`
- Run `sudo apt-get install build-essential tk-dev libncurses5-dev libncursesw5-dev libreadline6-dev libdb5.3-dev libgdbm-dev libsqlite3-dev libssl-dev libbz2-dev libexpat1-dev liblzma-dev zlib1g-dev`
- Donwload Python sources with `wget https://www.python.org/ftp/python/3.6.6/Python-3.6.6.tar.xz`
- Extract sources `tar xf Python-3.6.6.tar.xz`
- `cd Python-3.6.6`
- `./configure`
- `make`
- `sudo make altinstall`

Having the system configured the application can be installed as described in "Installation" paragraph.

<div style="page-break-after: always;"></div>

## Sources

### Naming convention used

https://visualgit.readthedocs.io/en/latest/pages/naming_convention.html

### Requirements

To run the project you need Python version 3.6 or above, application was developed and tested using Python 3.6.6.

## Installation

Application is installed starting from sources. Sources can be obtained in many ways, thre simplest one is to get them as a compressed zip file and put them in `/opt/modbus-mqtt-gateway/`. 
The official source where to get zip file is from https://github.com/guglielmino/modbus-mqtt-gateway/archive/v{VERSION}.zip, where VERSION must be replaced with the required version number in semver format (ie. 0.0.3, https://github.com/guglielmino/modbus-mqtt-gateway/archive/v0.0.3.zip).

To install project dependencies change the working directory in the one where sources are stored (tipically `/opt/modbus-mqtt-gateway/`), thens run `pip3.6 install -r requirements.txt`

Application default build create a Docker image pushed [here](https://cloud.docker.com/u/guglielmino/repository/docker/guglielmino/modbus-mqtt-gw), you can install it without installing Python or any other dependencies in case of docker deploy.

### Running tests

Application is developed with TDD approach, then all main features are covered by unit tests.
Before running the application is a best practice to execute unit tests, to do this run `make test` from the root folder.

<div style="page-break-after: always;"></div>

## Running the application

Application configurations are deployed in `data/` folder in the project root directory. Default files are:

- `general.ini` : General configurations

Sample content

```
[GENERAL]
id = 12334
name = test
admin_user = admin
admin_password = adminpwd
sys_user = root
sys_password = rootpwd
```

- `serial.ini` : Serial and Modbus configurations

Sample content

```
[SERIAL MODBUS]
port = /dev/serial0
speed = 19200
parity = N
stopbit = 1
bitlen = 8
master_id = 123
polling_time = 12
connect_timeout = 20
retry_count = 3
```

- `cellular.ini` : Cellular module settings

Sample content

```
[4G]
sim_pin = 0000
reboot_timer = 10
protocol = 3G
apn = apn.tim.it
```

<div style="page-break-after: always;"></div>

- `mqtt.ini` : MQTT connection settings

Sample content

```
[MQTT]
broker_pri = 212.237.3.187
broker_sec = mqtt2.nowhere.org
qos = 0
user_auth = S
cert_auth = N
username = user1
password = pass1
cert_file = /path/cert1
key_file = /pat/key1
```

- `regs-topics.csv` : main table for mapping register to topics

Sample content

```
topic;descr;cscaling_coeff;offset;ui;registry_range;slave_id;access;send_on_variation;variation;variation_type;recurring_send;recurring_span;add_ui;add_descr;opt1;opt2;opt3
sample1/{slave_id}/{reg_address};descr1;1;0;V;40001-40003;1;R;S;10;%;S;3;S;S;opt1_1;opt2_1;opt3_1
sample2/{slave_id}/{reg_address};descr1;1;0;V;40005;1;R;S;10;%;S;8;S;S;opt1_1;opt2_1;opt3_1
samplesub;sample conf for subscription;1;0;V;40002;1;R/W;S;10;%;S;8;S;S;opt1_1;opt2_1;opt3_1
```

<div style="page-break-after: always;"></div>

## Running application from commandline

Applications supports some commandline parameters, parameters are shown with the common Unix tools syntax,
running the application with the `--help` flag.

```
Usage: main.py [OPTIONS]

Options:
  --enable-modbus TEXT        Enable ModBus subsystem
  --enable-webadmin TEXT      Enable WebAdmin interface
  --web-port INTEGER          Web admin listening port
  --debug TEXT                Enable debug
  --mbus-refresh-sec INTEGER  ModBus register refresh interval (default 5 sec)
  --help                      Show this message and exit.
```


## Application service (auto start at boot time)

ModBus gateway is setup to start automatically at boot time using `systemd`, to configure autostart in Raspian distribution installed from scratch is necessary to create a `/lib/systemd/system/modbusgateway.service` file with the following content.

```
[Unit]
Description=ModBus to MQTT Gateway
After=multi-user.target

[Service]
Type=idle
ExecStart=/usr/local/bin/python3.6 /opt/modbus-mqtt-gateway/src/main.py
Restart=always

[Install]
WantedBy=multi-user.target
```

In `ExecStart` supports command line parameters, as described in the previous paragraph. For a full description of `Systemd` configuration
refer to [this](https://www.shellhacks.com/systemd-service-file-example/) post.

**Note**: you must set `python` name and path accorting to the system installation. The same applies to the `main.py` script.
          The same apply to `pip` when dependencies are installed, ie. if `python3.6` is the one installed dependecies must be
          installed using `pip3.6 ....`
        

To complete the service installation execute the following instructions.

```
# chmod 664 /lib/systemd/system/modbusgateway.service
# systemctl daemon-reload
# systemctl enable modbusgateway.service

```

### Controlling the service

Even if the application is automatically started at boot time it's possibile to control it's lifecycle with standard `systemd` command, specifically.

- check application status `systemctl status modbusgateway.service`
- stop the application `systemctl stop modbusgateway.service`
- start the application `systemctl start modbusgateway.service`

<div style="page-break-after: always;"></div>

## Web admin

To monitor and perform administration tasks a web administration is available at http://{deviceip}:5000.
Some pages are public while other are protected by user and password. For protected pages, first access to them, pop ups a dialog asking for user and password.

### Dashboard

Main screen is Dashboard, from this page is avaiable MQTT and ModBus statuses, a green flag means that subsystems
are working fine, a red X means there is an issue.

![Dashboard](./docs/dashboard.png)

### Commands

In this page there are commands to control the application. Commands are split in two categories, log commands are in the first row, while in the row below there are commands related to reboot subsystems.

![Comandi](./docs/comandi.png)

### Config - General

General config page holds generic informations for the gateway application. It's very important to be careful managing "Admin user" and "Admin password" because those are the credentials to get access to web admin's protected pages. Configuration changes are applied after reboot.

![Config General](./docs/config-general.png)

### Config - Serial

Serial configuration related to ModBus communication trough RS485. Parameters are self esplicatory but `polling time` worth a little note, this is the internal frequency that drives all the logic to manage recurring and on variation reading. It should be changed according to the setting in ModBus registries table, 1 is a safe value.
Configuration changes are applied after reboot.

![Config Serial](./docs/config-serial.png)

### Config - MQTT

MQTT settings for the communication with broker. The two borker addresses are intended to manage fault tollerance, in case one isn't available application try with the second one. Note: this could create side effects in case of connection temporarily unavailable to one broker, the application will switch on the orther one, potentially losing sync with other consumers if the first broker was actually working.
Configuration changes are applied after reboot.

![Config MQTT](./docs/config-mqtt.png)

### Config - MQTT Table

This is the main table keeping mapping information between ModBus registries and MQTT topics.
Columns are:
- topic: MQTT Topic
- descr: generic description
- cscaling_coeff: scaling coefficient for register's reading
- offset: offset for register's reading
- ui: engineering unit 
- registry_range: register address(es) to read/write
- slave_id: ModBus device's slave id
- access: R,W or RW for read, write, read/write
- send_on_variation: data will be send when it changes 
- variation: variation edge
- variation_type: type of variation, percent or absolute
- recurring_send: send data periodically
- recurring_span: send frequency
- add_ui: add ui string to message
- add_descr: add description to message
- opt1: optional field 1
- opt2: optional field 2
- opt3: optional field 3

![Config MQTT Table](./docs/config-mqtt-table.png)

## MQTT Commands

Below MQTT request and response payloads for defined commands expected on `{gatewayId}/System/GatewayCmd` and `{gatewayId}/System/GatewayInfo` topics respectively:

### GtwInfo

Get info about the gateway application

```
REQUEST:
{
  "type": "gtwinfo",
  "payload": {}
}

RESPONSE:
{
  "type": "gtw",
  "payload": {
    "version": "1.0.2"
  }
}
```

### Gtw

```
REQUEST:
{
  "type": "gtw",
  "payload": {}
}

RESPONSE:
{
  "type": "gtw",
  "payload": {
    "csv": "topic;descr;cscaling_coeff;offset;ui;registry_range;slave_id;access;send_on_variation;variation;variation_type;recurring_send;recurring_span;add_ui;add_descr;opt1;opt2;opt3\\r\\ntest\/topic;test descr;0;0;A;40000;1;R;S;5;%;S;1;S;S;o1;o2;o3\\r\\nprova\/topic;decription;1;10;V;41000;123;W;S;12;%;N;0;S;S;o1-1;o2-1;o3-1\\r\\n"
  }
}
```

### GtwId

```
REQUEST:
{
  "type": "gtwid",
  "payload": {}
}

RESPONSE
{
  "type": "gtwid",
  "payload": {
    "id": "1234"
  }
}
```

<div style="page-break-after: always;"></div>

### GtwIdWrite

Write one gateway id property in general setting

```
REQUEST:
{
   "type":"gtwidwrite",
   "payload":{
      "settings":[
         { "gtwid":"mygtwid" }
      ]
   }
}

RESPONSE
{
  "type": "gtwidwrite",
  "payload": { }
}
```

### GtwReboot

```
REQUEST:
{
  "type": "gtwreboot",
  "payload": {}
}

RESPONSE
{
  "type": "gtwreboot",
  "payload": {}
}
```

<div style="page-break-after: always;"></div>

### GtwWrite

```

REQUEST:
{
  "type": "gtwwrite",
  "payload": {
    "csv": "topic;descr;cscaling_coeff;offset;ui;registry_range;slave_id;access;send_on_variation;variation;variation_type;recurring_send;recurring_span;add_ui;add_descr;opt1;opt2;opt3\\r\\ntest\/topic;test descr;0;0;A;40000;1;R;S;5;%;S;1;S;S;o1;o2;o3\\r\\nprova\/topic;decription;1;10;V;41000;123;W;S;12;%;N;0;S;S;o1-1;o2-1;o3-1\\r\\n"
  }
}

RESPONSE
{
  "type": "gtwwrite",
  "payload": {}
}

```

### GtwLog

```
REQUEST:
{
  "type": "gtwlog",
  "payload": {}
}

RESPONSE
{
  "type": "gtwlog",
  "payload": { "log": "LOG DATA" }
}
```

### GwtLogErase

Delete Gateway activity log

```

REQUEST:
{
   "type":"gtwlogerase",
   "payload":{ }
}

RESPONSE
{
  "type": "gtwlogerase",
  "payload": { }
}
```

<div style="page-break-after: always;"></div>

### Mqtt

```
REQUEST:
{
  "type": "mqtt",
  "payload": {}
}

RESPONSE
{
  "type": "mqtt",
  "payload": { "config": "CONFIG FILE CONTENT" }
}
```
Note about certs: requesting the config some fields not physically present in the MQTT.ini will be added to give information
on the state of the certificate. In particular those fields are:

```
cert_file_lasttime = 30/01/2020 18:21:42
key_file_lasttime = [MISSING]
ca_file_lasttime = 17/12/2019 09:41:14
```

Those fields can have a date, indicating that file exists and last time it was updated, or [MISSING] to indicate file isn't uploaded. At the same time fields related to physical location of certificates in filesystem are removed.

### MqttLog

```
REQUEST:
{
  "type": "mqttlog",
  "payload": {}
}

RESPONSE
{
  "type": "mqttlog",
  "payload": { "log": "LOG DATA" }
}
```

### MqttLogErase

Delete Mqtt module activity log

```

REQUEST:
{
   "type":"mqttlogerase",
   "payload":{ }
}

RESPONSE
{
  "type": "mqttlogerase",
  "payload": { }
}
```

<div style="page-break-after: always;"></div>

### MqttWrite

Write one or more keys related to MQTT configuration

```
REQUEST:
{
   "type":"mqttwrite",
   "payload":{
      "settings":[
         { "broker_pri":"mqtt.test.com" },
         { "broker_sec":"mqtt2.test.org" },
         { "qos":"0" },
         { "user_auth":"N" },
         { "cert_auth":"S" },
         { "cert_file": "-----BEGIN CERTIFICATE-----\nMIIEZDCCA0ygAwIBAgIUHvbWkHKpY7rjDVBcsIP0C8kXqj0wDQYJKoZIhvcNAQEN\nBQAwajEXMBUGA1UEAwwOQW4gTVFUVCBicm9rZXIxFjAUBgNVBAoMDU93blRyYWNr..."},
         { "ca_file": "-----BEGIN CERTIFICATE-----\nMIIDtTCCAp2gAwIBAgIUbI5kESOcu1nuZsd0DIKtBYVPhTEwDQYJKoZIhvcNAQEN\nBQAwajEXMBUGA1UEAwwOQW4gTVFUVCBicm9rZXIxFjAUBgNVBAoMDU93blRyYWNr..."},
         { "key_file": "-----BEGIN RSA PRIVATE KEY-----\nMIIEowIBAAKCAQEAnhqq8O9PExWGmG4UtXYF3FHJluTGFdY5HkYTXf6vF5Pbvxfh\n3leKqSXcmV1p30SabB1x1561R6UxKbvlR88yVMsUs6falR1gClSYpeZZnMb5fIQl..."}
      ]
   }
}

RESPONSE
{
  "type": "mqttwrite",
  "payload": { }
}
```

### MqttReboot

Reboot MQTT module

```
REQUEST:
{
   "type":"mqttreboot",
   "payload":{}
}

RESPONSE
{
  "type": "mqttreboot",
  "payload": { }
}
```

<div style="page-break-after: always;"></div>

### CnxMdb

Reqest Serial/ModBus config

```
REQUEST:
{
  "type": "cnxmdb",
  "payload": {}
}

RESPONSE
{
  "type": "cnxmdb",
  "payload": { "config": "CONFIG FILE CONTENT" }
}
```

### CnxMdbLog

Request Serial/Modbus log

```
REQUEST:
{
  "type": "cnxmdblog",
  "payload": {}
}

RESPONSE
{
  "type": "cnxmdblog",
  "payload": { "log": "LOG DATA" }
}
```

<div style="page-break-after: always;"></div>

### CnxMdbLogErase

Delete Serial/Modbus module activity log

```

REQUEST:
{
   "type":"cnxmdberase",
   "payload":{ }
}

RESPONSE
{
  "type": "cnxmdberase",
  "payload": { }
}
```

### CnxMdbWrite

Write one or more keys related to Serial/ModBus configuration

```

REQUEST:
{
   "type":"cnxmdbwrite",
   "payload":{
      "settings":[
         { "port":"/dev/serial0" },
         { "speed":"19200" },
         { "parity":"N" },
         { "stopbit":"1" },
         { "bitlen":"8" },
         { "master_id":"123" },
         { "polling_time":"8" },
         { "connect_timeout":20" },
         { "retry_count":"3" }
      ]
   }
}

RESPONSE
{
  "type": "mqttwrite",
  "payload": { }
}
```

### CnxMdbStatus

Get status related to connected slaves, it returns "true" for devices that sends at least a successful response in the last read cycle 

```

REQUEST:
{
   "type":"cnxmdbstatus",
   "payload":{}
}

RESPONSE
{
  "type": "mqttwrite",
  "payload": { 
    "status": {
      "1": "true",
      "2": "false
    }
  }
}
```



<div style="page-break-after: always;"></div>

### 4G

```
REQUEST:
{
  "type": "4G",
  "payload": {}
}

RESPONSE
{
  "type": "4G",
  "payload": { "config": "CONFIG FILE CONTENT" }
}
```

### 4GWrite

Write one or more keys related to Cellular configuration

```

REQUEST:
{
   "type":"4gwrite",
   "payload":{
      "settings":[
         { "sim_pin":"1234" },
         { "reboot_timer":"10" },
         { "protocol":"3G" },
         { "apn":"apn.tim.it" }
      ]
   }
}

RESPONSE
{
  "type": "4gwrite",
  "payload": { }
}
```

<div style="page-break-after: always;"></div>

### 4GReboot

Reboot 4G module

```

REQUEST:
{
   "type":"4greboot",
   "payload":{ }
}

RESPONSE
{
  "type": "4greboot",
  "payload": { }
}
```

### 4GLogErase

Delete Cellular module activity log

```

REQUEST:
{
   "type":"4glogerase",
   "payload":{ }
}

RESPONSE
{
  "type": "4glogerase",
  "payload": { }
}
```

<div style="page-break-after: always;"></div>

#### Command error responses

If a command encounter an error during the execution a specific error payload is sent on `{gatewayId}/System/GatewayInfo`

```
{
  "type" : "regwrite",
  "payload" : {
    "error" : "ERROR MESSAGE",
    "details": "ERROR DETAILS"
  }
}
```

type brings the origina command type but it could be `UNKNOWN` in case the error is related to command type.
In some cases further details are added to payload, like in the example of failed CSV table validation below.

```
{
   "type":"gtwwrite",
   "payload":{
      "error":"write failed",
      "write_errors":[
         [
            "invalid 'variation_type' value (U)"
         ]
      ]
   }
}
```

### Register subscription on custom topic

Each item in configuration table marked as `R/W` create an MQTT topic subscription where the application listen for specific payload.
Payload expected on these topic is like the following example.

```
{
  "type": "regwrite",
  "payload": {
       "topic": "samplesub",
       "value": "233",
       "CDR": "",
       "timestamp": "2019-03-17T07:39:28",
       "idsender": "123"
   }
}
```

<div style="page-break-after: always;"></div>

Topic subscribed by the application follow the convention `{gateway_id}/{table_row_topic}_W`, then given gateway id like `gateway1`
and a table row as

`loadtopic;register connected to a load;1;0;V;40002;1;R/W;S;10;%;S;8;S;S;opt1_1;opt2_1;opt3_1`

The MQTT topic subscribed will be `gateway1/loadtopic_W` and payload expected somethig like

```
{
  "type": "regwrite",
  "payload": {
       "topic": "gateway1/loadtopic",
       "value": "4",
       "CDR": "",
       "timestamp": "2019-03-17T10:12:21",
       "idsender": "123"
   }
}
```

**Note**: Subscribed topic on MQTT is not the same of the value of `topic` field in the payload. The former follow the convention
described above, while the latter is the topic as specified in the configuration table, must be specified with the full gatewayId if specified as interpolation variable.

### Topic interpolation

Table for mapping registers to topics supports string interpolation. The variable supported for interpolation is {gatewayId}, then defining a topic like `{gatewayId}/sample` is resolved by the application to `12334/sample`, supposing `12334` as the configured gatewayId

## Technical details

Application is strongly based on a Producer/Consumer pattern. The function `main_consumer` is intended as a central point to process messages coming from many sources (mainly ModBus and MQTT at time of writing) and dispatching them to the right processor.

Messages comes to `main_consumer` in a message wrapper with the structure as in the pseudocode below:

```
class BusEvent:
  event_type
  event_payload
```

The property `event_type` is defined in a enumerator, values are `MODBUS = 1` or `MQTT = 2`, instead `event_payload` is a generic object and structure
depends on the `event_type`. 

When `event_type` is `MODBUS` the `event_payload` is as follow:

```
class ModBusPayload:
  tab_item:
    slave_id
    add_ui
    send_on_variation
    add_descr
    recurring_send
    registries
  mbus_value: 
    reg_str
    mbus_resp:
      resp_type
      mbus_resp
```

